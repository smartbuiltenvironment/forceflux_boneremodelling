#pragma once

#include "Common.h" 
#include "StructuralComponent.h"

#include "ErrorCheck.h"
#include<GL/glew.h>
#include<glfw3.h>
#include "glm/glm.hpp" 
#include "glm/gtc/matrix_transform.hpp"
#include "Shader.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw_gl3.h"

using namespace std;
int ScreenWidth = 2400;
int ScreenHeight = 1350;

keyAcion m_KeyAction = keyAcion::non;
float m_xPos = ScreenWidth / 2;
float m_yPos = ScreenHeight / 2;
float m_Fov = 45.0f;
bool m_LeftButtonPressed = false;
bool m_RightButtonPressed = false;

bool m_LeftButtonReleased = false;


void processInput(GLFWwindow* window, StructuralComponent* currentTest, float scroll)
{
	bool reset = false;

	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		m_KeyAction = keyAcion::leftArrow;
	}
	else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		m_KeyAction = keyAcion::rightArrow;
	}
	else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		m_KeyAction = keyAcion::upArrow;
	}
	else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		m_KeyAction = keyAcion::downArrow;
	}
	
	if(m_KeyAction != keyAcion::non && m_KeyAction != keyAcion::mouse)
		currentTest->OnUpdateCameraKey(m_KeyAction);

	if (m_KeyAction == keyAcion::mouse && m_LeftButtonPressed)
	{
		currentTest->OnUpdateCameraMouseRotate(m_xPos, m_yPos);
		reset = true;
	}
	
	if (m_KeyAction == keyAcion::mouse && m_LeftButtonReleased)
	{
		currentTest->OnUpdateCameraReset();
		m_LeftButtonReleased = false;
		reset = true;
	}


	if(reset)
		m_KeyAction = keyAcion::non;

	if (scroll != 0)
	{	
		m_Fov -= scroll;
		currentTest->OnUpdateCameraScroll(m_Fov);
	}
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	m_KeyAction = keyAcion::mouse;
	m_xPos = xpos;
	m_yPos = ypos;
}

void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

void ButtonFeedback(GLFWwindow* window)
{
	int stateLeft = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
	if (stateLeft == GLFW_PRESS)
		m_LeftButtonPressed = true;
	else if (stateLeft == GLFW_RELEASE) 
	{
		m_LeftButtonReleased = true;
		m_LeftButtonPressed = false;
	}
	
	int stateRight = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
	if (stateRight == GLFW_PRESS)
		m_RightButtonPressed = true;
	else if (stateRight == GLFW_RELEASE)
		m_RightButtonPressed = false;
}


SolverSettings SolverSetup(SimulationType simType)
{
	SolverSettings ss;

	if (simType == SimulationType::Bone) 
	{
		ss.dt = 0.03f; //0.03f;  
		ss.co = 0.98f;
		ss.t = 0.0f;
		ss.t2 = ss.t + ss.dt;
		ss.t1 = 0.5 * (ss.t + ss.t2);
		ss.sScale = 0.50;
	}
	else
	{
		ss.dt = 0.0000001f; 
		ss.co = 0.99f; //0.98f;
		ss.t = 0.0f;
		ss.t2 = ss.t + ss.dt;
		ss.t1 = 0.5 * (ss.t + ss.t2);
		ss.sScale = 1.0;
	}

	
	return ss;
}

MaterialSettings SetupMaterial(SimulationType simType)
{
	//Data from: https://www.engineeringtoolbox.com/concrete-properties-d_1223.html
	//Compressive strength : 20 - 40 MPa(3000 - 6000 psi)
	//Flexural strength : 3 - 5 MPa(400 - 700 psi)
	//Tensile strength - ? : 2 - 5 MPa(300 - 700 psi)

	MaterialSettings ms;

	if (simType == SimulationType::Bone)
	{
		ms.rho = 7800.0f;
		ms.E = 10e6f;
		ms.v = 0.28f;
		ms.G = ms.E / (2.0f * (1.0f + ms.v));
		ms.K = ms.E / (3.0f * (1.0f - 2.0f * ms.v));
		ms.yieldStrain = 1.0f; //0.0005;			//1 / 1500 ~ 0.00066
		ms.elongationLimit = 0.0001;	// 1.0f / 6000 ~ 0.00016 - > 13 % of average arm length. 0.0012 -> 100 % of average arm length
		ms.basicFibreStiffnessConstant = 0.05f;
	}
	else
	{
		ms.rho = 7800.0f;
		ms.E = 210e9f;
		ms.v = 0.28f;
		ms.G = ms.E / (2.0f * (1.0f + ms.v));
		ms.K = ms.E / (3.0f * (1.0f - 2.0f * ms.v));
		ms.yieldStrain = 0.0010; //0.0030; //1.0f / 1500.0f;		// 1/800
		ms.elongationLimit = 0.0001; //0.0002; //0.0008;  //1.0f / 6000;		//10000.0f; //100000.0f;
	}	

	return ms;
}

KernelSettings SetupKernel(ImportType importType) 
{
	KernelSettings ks;

	float alpha = 4.5f;
	if (importType == ImportType::Femur2D || importType == ImportType::Frame2D)
		alpha = 5.5f;

	int k = 2;
	float pi = 3.1415f;
	float C = (float)Factorial(2 * k + 3) / (4.0f * pi * pow(alpha, 3) * pow(2.0f, (2.0f * k)) * pow((float)Factorial(k), 2.0f));
	float C2 = (8.0f / 105.0f) * pow(alpha, 3);
	C = 2.0f;

	ks.alpha = alpha;
	ks.alphaSquared = alpha * alpha;
	ks.alphaPowThree = pow(alpha, 3);
	ks.alphaPowSeven = pow(alpha, 7);
	ks.k = k;
	ks.C = C;

	ks.rndSeed = 1;

	return ks;
}

DisplacementSettings SetupDisp(ImportType importType, SimulationType simType)
{
	DisplacementSettings ds;
	
	float loadStep = 0.0f;
	
	if (simType == SimulationType::Bone) 
	{	
		loadStep = 1e-8; // 3D
		if (importType == ImportType::Femur2D || importType == ImportType::Frame2D) // 2D
			loadStep = 2e-10;
	
		ds.stepSize = loadStep; //2.5e-8; // ds.magnitude / ds.nDispSteps;
		ds.nDispSteps = 200;
		ds.stepCounter = 1;
	}
	else //Steel component
	{
		loadStep = 2.0e-8; //1.0e-7;
		ds.stepSize = loadStep; //2.5e-8; // ds.magnitude / ds.nDispSteps;
		ds.nDispSteps = 10000;
		ds.stepCounter = 1;
	}
	
	return ds;
}

void Execute(SimulationType simType, ImportType impType, SolverSettings solver, MaterialSettings material, KernelSettings kernel, DisplacementSettings disp, float strainLimit, string fileNameSuffix)
{
	/*いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい*/
	//Initialise OpenGL following the The Cherno - OpenGL Series on youtube
	/*いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい*/

	m_LeftButtonPressed = true;
	m_RightButtonPressed = false;

	GLFWwindow* window;
	glfwSetErrorCallback(error_callback);

	/* Initialize the library */
	if (!glfwInit())
		return; //-1;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(ScreenWidth, ScreenHeight, "Force flux peridynamics", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return;// -1;
	}

	const char* glfwVersion = glfwGetVersionString();
	glfwSetCursorPosCallback(window, mouse_callback);

	std::cout << "GLFW Version: " << glfwGetVersionString() << std::endl;

	/* Make the window's context current */
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1); //

	//Needs to be initialised after we have a valid context
	if (glewInit() != GLEW_OK)
		std::cout << "Error at GLEW initialisation!" << std::endl;

	std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << std::endl;

	int counter = 0;

	//Initialise the model
	StructuralComponent* currentTest = new StructuralComponent(simType, impType, solver, material, kernel, disp, strainLimit, fileNameSuffix);
	currentTest->InitialiseOpenGL();

	//Scope definition for variable life etc.
	{
		GLCall(glEnable(GL_BLEND));
		GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

		//Initialisation of ImGUI
		ImGui::CreateContext();
		ImGui_ImplGlfwGL3_Init(window, true);
		ImGui::StyleColorsDark();

		/* Loop until the user closes the window */
		while (!glfwWindowShouldClose(window))
		{
			GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
			GLCall(glClear(GL_COLOR_BUFFER_BIT));

			ImGui_ImplGlfwGL3_NewFrame();
			if (currentTest) //Testing that the object is not null
			{
				ImVec2 wPos = ImGui::GetWindowPos();
				processInput(window, currentTest, ImGui::GetIO().MouseWheel);

				currentTest->OnUpdateAnalysis();
				currentTest->OnUpdateTime();
				currentTest->OnUpdateOpenGL();
				currentTest->OnRender();

				//Save results every 10th iteration
				currentTest->SaveResults();
				
				//Write the results to file every 100th iteration
				if (counter % 100 == 0)
					currentTest->Output();

				counter++;
				
				ImGui::Begin("Test");
				currentTest->OnImGuiRenderer();		//Registering a new test based on the button clicked.
				ImGui::End();
			}

			ImGui::Render();
			ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
			GLCall(glfwSwapBuffers(window));
			GLCall(glfwPollEvents());

			ButtonFeedback(window);
		}

		delete currentTest;

	}//Scope to avoid some OpenGL error (Video Abstracting OpenGL into Classes)

	ImGui_ImplGlfwGL3_Shutdown();
	ImGui::DestroyContext();
	glfwTerminate();

	return;
}

int main()
{	
	std::cout << "Code version: Free from steel components" << std::endl;

	ImportType importType = ImportType::NodeRotSymHole3D; //ImportType::NodeRotSymHole3D;

	float alpha = 4.5;
	SimulationType simType = SimulationType::SteelComponent;

	if (importType == ImportType::Femur2D || importType == ImportType::Frame2D) 
		alpha = 5.5;

	float strainLimit = 0.1; //0.025;

	SolverSettings solver = SolverSetup(simType);
	MaterialSettings material = SetupMaterial(simType);
	KernelSettings kernel = SetupKernel(importType);
	DisplacementSettings disp = SetupDisp(importType, simType);

	string fileNameSuffix = "";

	Execute(simType, importType, solver, material, kernel, disp, strainLimit, fileNameSuffix);
	
	return 0;
}







