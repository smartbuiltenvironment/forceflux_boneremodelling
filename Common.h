#pragma once

#include <cmath>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <array>


#define RANDMAX = 1;

#define VISUAL = 1;

#include <chrono>


enum class drawType
{
	drawStatic, 
    drawDynamic
};


enum class crackDirection
{
    radial,
    tangential,
    vertical,
};


enum class keyAcion
{
    non, 
    leftArrow, 
    rightArrow, 
    upArrow, 
    downArrow, 
    mouse, 
    scroll
};

enum class colorParticleBy 
{
    displacement,
    velocity, 
    acceleration, 
    meanStrain, 
    forceArm,
    forceExt,
    forceTot,
    damage,
    isBC,
    life,
    strainSquared,
    strain,
};

enum class gaugePlane 
{
    top,
    bot,
    non
};

enum class analysisType 
{
    linear,
    nonlinear,
};

enum class colorArmBy
{
    elongationToFracture,
    strain,
    plasticStrain,
    force,
    forceComTen,
    broken,
    stiffnessSF,
    anisotropy,
    tension,
    compression,
};

enum class pArrangement
{
    regular,
    irregular,
    random
};

enum class SimulationType
{
    //Import2D,
    //Import3D,
    Bone,
    SteelComponent,
    SteelTest,
};

enum class view 
{
    undefined,
    top,
    shortSide,
    longSide,
};

enum class ImportType
{
    Femur2D,
    Frame2D,
    Femur3D,
    Gerbretter3D,
    TwaColumn3D,
    Tamedia3D,
    TetraNode3D,
    SteelTest3D,
    NodeRotSym3D,
    NodeRotSymHole3D,
    Pipe3D,
};

enum class Anisotropy
{
    Cylindrical,
    BuildDirX,
    BuildDirY,
    BuildDirZ,
};


enum class BC
{
    prescribeAll,
    prescribeNon,
    prescribeZ,
    springLoad,
    withFriction,
    loadCylinders
};

enum class BCtype
{
    support,
    load
};



enum class viewType
{
    perspective,
    orthographic
};

enum class limitType
{
    strain,
    stress,
    combined
};

enum class pLife
{
    alive,
    dead,
    immortal
};




struct Vec2
{
	float x, y;
};

struct Vec3
{
	float x, y, z;
};

struct Vec4
{
	float x, y, z, w;
};

struct Arr3
{
    int x, y, z;
};

struct BB
{
    Vec3 min;
    Vec3 max;
};


struct SimulationSettings 
{
    float alpha;
    float elongationLimit;
    float yieldStrain;
    int rndSeed;
};

struct Vertex
{
	Vec3 Position;
	Vec4 Color;
};

struct VertexD
{
    Vec3 cPosition;
    Vec4 Color;
    Vec3 iPosition;
};

struct VertexN
{
    Vec3 Position;
    Vec3 Normal;
    Vec4 Color;
};

struct LoadingCylinder 
{
    Vec3 cPosition;
    Vec3 Normal;
    float Radius;
    BCtype type;
    Vec3 iPosition;
};

struct Particle 
{
    Vec3 cPosition;         //Current position
    Vec3 iPosition;         //Initial position
    Vec3 ArmForce;
    Vec3 ExtForce;
    Vec3 TotForce;
    Vec3 Veloc;
    Vec3 Accel;
    Vec4 Color;
    float h;                //Radius of the particle
    float rMass;            //Real mass 
    float fMass;            //Fictious mass
    Vec3 fMasses;
    float meanStrain;
    float displacement;
    float rSize;
    float damage;           //Number of broken bonds over number of total bonds.
    bool isBC;
    pLife life;
    float strainSquared;
    float strain;
};

struct Arm
{
    int p1;
    int p2;
    float iLength;
    float cLength;
    float iLengthSquared;
    float elongation;
    float strain;
    float strainRate;
    float plasticStrain;
    float fMag;
    Vec4 color;
    bool broken;
    bool selected;
    float stiffness;
    float sf;               //Stiffness scale factor
    float dir_sf;           //Direction based stiffness scale factor
};

struct Plane
{
    Vec3 Normal;
    Vec3 Origin;
    Vec3 xAxis;
    Vec3 yAxis;
    float a;                //Components of the plane equation: ax + by + cz = d
    float b;
    float c;
    float d;
};


struct Crack 
{
    float radius;
    Plane plane;
};


struct ArmIndices 
{
    int p1;
    int p2;
    int flatIndex;
};

struct Point
{
    Vec3 Position;
};

struct Color
{
    Vec4 Color;
};

struct Line
{
    Vec3 StartPosition;
    Vec3 EndPosition;
    Vec4 Color;
};

struct Stats 
{
    int nBondsPerParticle;
    int nParticlesPerZone;
    int nBonds;
    int nParticles;
    int nZones;
    int nUsedZones;
    float zoneSizeParticleSizeRatio;
    float averageArmLength;
};

struct GeometrySettings 
{
    Arr3 zoneCount;
    Vec3 zoneStep;
    Vec3 bbSize;
    Vec3 zonePadding;
    float height;
    float radius;
    float AreaSmall;
    float AreaLarge;
    float bcAngle;
    float bcThickness;
    float edgePadding;
    float waistSF;
    float rndSeed;
};


struct TetraZoneSettings
{
    Arr3 zoneCount;
    Vec3 zoneStep;
    Vec3 domain;
    Vec3 add;
};


struct PoissonsRatioSettings
{
    float hLimit;
    float waistHeight;
    float rLimit;
};

struct SolverSettings 
{
    float dt;       //Timestep
    float co;       //Carry over factor
    float t;        //Time
    float t1;       //Time at t + 0.5* dt
    float t2;       //Time at t + dt
    float sScale;   //Stiffness scale
};

struct LoadSettings
{
    float magnitude;      //Total load in [N]
    int nLoadSteps;
    int stepCounter;
};

struct DisplacementSettings
{
    float magnitude;      //Total load in [N]
    float stepSize;
    int nDispSteps;
    int stepCounter;
};

struct SpringSettings
{
    float k;      //Total load in [N]
    float iLength;
};

struct MaterialSettings 
{
    float rho;                              //Density    
    float E;                                //Youngs modulus
    float K;                                //Bulk modulus    
    float G;                                //Shear modulus
    float v;                                //Poissions ratio
    float yieldStrain;                      //Yield strain in compression
    float elongationLimit;                  //Plastic elongation limit to failure
    float basicFibreStiffnessConstant;
};

struct KernelSettings 
{
    float alpha;
    float alphaSquared;
    float alphaPowThree;
    float alphaPowSeven;
    float C;
    float k;
    int rndSeed;  //Not strickly a kernel parameter

};

struct LimitCriteria 
{
    float strainLimit;
    float stressLimit;
    float stressFractionOfMaxLimit;
    limitType type;
};

static Vec4 GetBlendedColor(float percentage)
{
    float frac;

    if (percentage >= 0.0f && percentage <= 25.0f)
    {
        //Blue fading to Cyan [0,x,255], where x is increasing from 0 to 255
        frac = percentage / 25.0f;
        return { 0.0f, (frac * 1.0f), 1.0f , 1.0f};
    }
    else if (percentage <= 50.0f)
    {
        //Cyan fading to Green [0,255,x], where x is decreasing from 255 to 0
        frac = 1.0f - std::abs(percentage - 25.0f) / 25.0f;
        return { 0.0f, 1.0f, (frac * 1.0f), 1.0f};
    }
    else if (percentage <= 75.0f)
    {
        //Green fading to Yellow [x,255,0], where x is increasing from 0 to 255
        frac = std::abs(percentage - 50.0f) / 25.0f;
        return {(frac * 1.0f), 1.0f, 0.0f, 1.0f };
    }
    else if (percentage <= 100.0f)
    {
        //Yellow fading to red [255,x,0], where x is decreasing from 255 to 0
        frac = 1.0f - std::abs(percentage - 75.0f) / 25.0f;
        return { 1.0f, (frac * 1.0f), 0.0f, 1.0f };
    }
    else if (percentage > 100.0f)
    {
        return { 1.0f, 0.0f, 0.0f, 1.0f};
    }

    return { 0.5, 0.5, 0.5, 1.0};
}

static Vec4 GetBlendedColor(float min, float max, float value)
{
    float diff = max - min;

    //float newMin = 0;
    float newMax = diff;
    float newValue = value - min;
    float percentage = 100.0f * (newValue / newMax);

    float frac;

    if (percentage >= 0.0f && percentage <= 25.0f)
    {
        //Blue fading to Cyan [0,x,255], where x is increasing from 0 to 255
        frac = percentage / 25.0f;
        return { 0.0f, (frac * 1.0f), 1.0f , 1.0f };
    }
    else if (percentage <= 50.0f)
    {
        //Cyan fading to Green [0,255,x], where x is decreasing from 255 to 0
        frac = 1.0f - std::abs(percentage - 25.0f) / 25.0f;
        return { 0.0f, 1.0f, (frac * 1.0f), 1.0f };
    }
    else if (percentage <= 75.0f)
    {
        //Green fading to Yellow [x,255,0], where x is increasing from 0 to 255
        frac = std::abs(percentage - 50.0f) / 25.0f;
        return { (frac * 1.0f), 1.0f, 0.0f, 1.0f };
    }
    else if (percentage <= 100.0f)
    {
        //Yellow fading to red [255,x,0], where x is decreasing from 255 to 0
        frac = 1.0f - std::abs(percentage - 75.0f) / 25.0f;
        return { 1.0f, (frac * 1.0f), 0.0f, 1.0f };
    }
    else if (percentage > 100.0f)
    {   
        //Returning red if the value overshoot the limit.
        return { 1.0f, 0.0f, 0.0f, 1.0f };
    }

    return { 0.5, 0.5, 0.5, 1.0 };
}

static Vec4 GetBlendedColorCap(float min, float max, float cap, float value)
{
    float diff = max - min;

    //float newMin = 0;
    float newMax = diff;
    float capedMax = newMax * cap;          //Cap is less or equal to 1.0
    float newValue = value - min;
    float percentage = 100.0f * (newValue / capedMax);

    float frac;

    if (percentage >= 0.0f && percentage <= 25.0f)
    {
        //Blue fading to Cyan [0,x,255], where x is increasing from 0 to 255
        frac = percentage / 25.0f;
        return { 0.0f, (frac * 1.0f), 1.0f , 1.0f };
    }
    else if (percentage <= 50.0f)
    {
        //Cyan fading to Green [0,255,x], where x is decreasing from 255 to 0
        frac = 1.0f - std::abs(percentage - 25.0f) / 25.0f;
        return { 0.0f, 1.0f, (frac * 1.0f), 1.0f };
    }
    else if (percentage <= 75.0f)
    {
        //Green fading to Yellow [x,255,0], where x is increasing from 0 to 255
        frac = std::abs(percentage - 50.0f) / 25.0f;
        return { (frac * 1.0f), 1.0f, 0.0f, 1.0f };
    }
    else if (percentage <= 100.0f)
    {
        //Yellow fading to red [255,x,0], where x is decreasing from 255 to 0
        frac = 1.0f - std::abs(percentage - 75.0f) / 25.0f;
        return { 1.0f, (frac * 1.0f), 0.0f, 1.0f };
    }
    else if (percentage > 100.0f)
    {
        //Returning red if the value overshoot the limit.
        return { 1.0f, 0.0f, 0.0f, 1.0f };
    }

    return { 0.5, 0.5, 0.5, 1.0 };
}


static Vec4 GetBlendedColorBlueRed(float min, float max, float value)
{
    float negRange, posRange, percentage, frac, sf;

    //Compression, thus blue, we assume that min is also a negative value
    if (value < 0) 
    {
        negRange = min;
        frac = value / negRange;   

        //Blue fading to white [x,x,255], where x is increasing from 0 to 255
        sf = frac;
        return { (sf * 1.0f), (sf * 1.0f), 1.0f , 1.0f };
    }
    else //We got tension, this red
    {
        posRange = max;
        frac = value / posRange;

        //White fading to Red [255,x,x], where x is decreasing from 255 to 0
        sf = 1.0f - frac;
        return { 1.0f, (sf * 1.0f), (sf * 1.0f), 1.0f };
    }
    
    return { 0.5, 0.5, 0.5, 1.0 };
}


static Vec4 GetBlendedColorGreenZero(float min, float max, float value)
{
    float negRange, posRange, percentage, frac, sf;

    //Negative strain colored from green to blue.
    if (value < 0)
    {
        negRange = std::abs(min);
        frac = std::abs(value / negRange);
        percentage = 100 * frac;

        if (percentage >= 0.0f && percentage <= 50.0f)
        {
            //Green fading to Cyan [0,x,255], where x is increasing from 0 to 255
            sf = percentage / 50.0f;
            return { 0.0f, 1.0f, (sf * 1.0f) , 1.0f };
        }
        else if (percentage <= 100.0f)
        {
            //Cyan fading to blue [0,x,255], where x is decreasing from 255 to 0
            sf = 1.0f - std::abs(percentage - 50.0f) / 50.0f;
            return { 0.0f, (sf * 1.0f), 1.0f, 1.0f };
        }
        else if (percentage > 100.0f)
        {
            //Blue
            return { 0.0f, 0.0f, 1.0f, 1.0f };
        }
    }
    else if (value == 0)
    {
        return { 0.0f, 1.0f, 0.0f, 1.0f };
    }
    else //Positive strain, colored from greed to red.
    {
        posRange = std::abs(max);
        frac = std::abs(value / posRange);
        percentage = 100 * frac;

        if (percentage >= 0.0f && percentage <= 50.0f)
        {
            sf = percentage / 50.0f;
            
            //Green fading to Yellow [x,255,0], where x is increasing from 0 to 255
            frac = std::abs(percentage - 50.0f) / 50.0f;
            return { (frac * 1.0f), 1.0f, 0.0f, 1.0f };
        }
        else if (percentage <= 100.0f)
        {
            //Yellow fading to red [255,x,0], where x is decreasing from 255 to 0
            frac = 1.0f - std::abs(percentage - 50.0f) / 50.0f;
            return { 1.0f, (frac * 1.0f), 0.0f, 1.0f };
        }
        else if (percentage > 100)
        {   
            //Red
            return { 1.0f, 0.0f, 0.0f, 1.0f };
        }
    }
    return { 0.5, 0.5, 0.5, 1.0 };
}

static Vec4 GetBlendedColorBlueBlackRed(float min, float max, float value) 
{
    float negRange, posRange, percentage, frac, sf;
    float movedVal;

    //Negative strain colored from black to red.
    if (value < 0)
    {
        negRange = std::abs(min);// -std::abs(zero);
        movedVal = std::abs(value);// - std::abs(zero);

        frac = movedVal / negRange;
        
        //Black to red
        return { 0.0f, 0.0f, 1.0f * frac, 1.0f }; //(1.0f - frac) * 1.0f };
    }
    else if(value > 0) //Positive strain, colored from black to red.
    {
        posRange = std::abs(max); // - std::abs(zero);
        movedVal = std::abs(value); // - std::abs(zero);

        frac = movedVal / posRange;
        
        return { 1.0f * frac, 0.0f, 0.0f, 1.0f}; //(1.0f - frac) * 1.0f
    }
    else if (value == 0)
    {
        return { 0.0f, 0.0f, 0.0f, 1.0f };
    }

    return { 0.5, 0.5, 0.5, 1.0 };
}

static Vec4 GetBlendedColorCapedBlueBlackRed(float min, float minCap, float max, float maxCap, float value)
{
    //minCap maxCap should be values between 0 and 1.

    float negRange, posRange, percentage, frac, sf;
    float movedVal;

    //Negative strain colored from black to red.
    if (value < 0)
    {
        float capedMin = min * minCap;
        negRange = std::abs(capedMin);// -std::abs(zero);
        movedVal = std::abs(value);// - std::abs(zero);

        frac = movedVal / negRange;

        if (frac < 1.0)
        {
            //Black to blue
            return { 0.0f, 0.0f, 1.0f * frac, 1.0f };
        }
        else //Only blue
            return { 0.0f, 0.0f, 1.0f, 1.0f };
    }
    else if (value > 0) //Positive strain, colored from black to red.
    {   
        float capedMax = max * maxCap;
        posRange = std::abs(capedMax); // - std::abs(zero);
        movedVal = std::abs(value); // - std::abs(zero);

        frac = movedVal / posRange;

        if (frac < 1.0) 
        {
            //Black to red
            return { 1.0f * frac, 0.0f, 0.0f, 1.0f };
        }
        else //Only red
            return { 1.0f, 0.0f, 0.0f, 1.0f };
    }
    else if (value == 0)
    {
        return { 0.0f, 0.0f, 0.0f, 1.0f };
    }

    return { 0.5, 0.5, 0.5, 1.0 };
}


static Vec4 GetBlendedColorCapedBlackWhite(float min, float minCap, float max, float maxCap, float value)
{
    float negRange, posRange, percentage, frac, sf;
    float movedVal;
    float capedMax = max * maxCap;

    movedVal = std::abs(value);// - std::abs(zero);

    frac = value / capedMax;

    if(value < capedMax) 
    {
        return { 1.0f * frac, 1.0f * frac, 1.0f * frac, 1.0f }; 
    }
    else if (value >= capedMax) 
    {
        return { 1.0f, 1.0f, 1.0f, 1.0f };
    }

    return { 0.5, 0.5, 0.5, 1.0 };
}



static float Distance(Vertex &v1, Vertex &v2) 
{
    float x1, y1, z1;
    float x2, y2, z2;

    x1 = v1.Position.x;
    y1 = v1.Position.y;
    z1 = v1.Position.z;

    x2 = v2.Position.x;
    y2 = v2.Position.y;
    z2 = v2.Position.z;

    return std::sqrt(std::pow(x2 - x1,2) + std::pow(y2 - y1, 2) + std::pow(z2 - z1, 2));
}

static float Distance(Particle& p1, Particle& p2)
{
    float x1, y1, z1;
    float x2, y2, z2;

    x1 = p1.cPosition.x;
    y1 = p1.cPosition.y;
    z1 = p1.cPosition.z;

    x2 = p2.cPosition.x;
    y2 = p2.cPosition.y;
    z2 = p2.cPosition.z;

    return std::sqrt(std::pow(x2 - x1, 2) + std::pow(y2 - y1, 2) + std::pow(z2 - z1, 2));
}

static float Distance(Vec3& v1, Vec3& v2)
{
    float x1, y1, z1;
    float x2, y2, z2;

    x1 = v1.x;
    y1 = v1.y;
    z1 = v1.z;

    x2 = v2.x;
    y2 = v2.y;
    z2 = v2.z;

    return std::sqrt(std::pow(x2 - x1, 2) + std::pow(y2 - y1, 2) + std::pow(z2 - z1, 2));
}

static Vec3 GetVector(Vec3& from, Vec3& to)
{
    float x, y, z;

    x = to.x - from.x;
    y = to.y - from.y;
    z = to.z - from.z;

    return {x, y, z};
}

static Vec3 AddVectors(Vec3& v1, Vec3& v2)
{
    float x, y, z;

    x = v1.x + v2.x;
    y = v1.y + v2.y;
    z = v1.z + v2.z;

    return { x, y, z };
}

static int Factorial(int f)
{
    if (f == 0)
        return 1;
    else
        return f * Factorial(f - 1);
}

static float CalcForceFlux110(float strain_ab, float strain_ab_pl, MaterialSettings bondMaterial, float meanStrain, float sf)
{
    float S = 0.0f;
    float N = 3.0f;                    
    float G = sf * bondMaterial.G;
    float v = bondMaterial.v;
    float K = sf * bondMaterial.K;

    S = G * (N + 2) * (strain_ab - strain_ab_pl) + (N * K - G * (N + 2)) * meanStrain;

    //------------------------
    //mu is the viscocity
    //S =  (N + 2) *( G * (strain_ab - strain_ab_pl) + mu * strainRate) + (N * K - G * (N + 2)) * meanStrain;
    //------------------------

    return S;
}

static float CalcForceFlux77(float strain_ab, MaterialSettings bondMaterial, float meanStrain, float sf)
{
    float S = 0.0f;
    float N = 3.0f;
    float G = sf * bondMaterial.G;
    float v = bondMaterial.v;
    float K = sf * bondMaterial.K;

    S = G * (N + 2) * strain_ab + (N * K - G * (N + 2)) * meanStrain;

    return S;
}

static float EvaluateDerivative(float r, float h, KernelSettings ks)
{
    float u = r / h;
    float Wprime = 0;
    float fprime = 0;
    float pi = 3.14159265358979323846f;

    if (u <= ks.alpha)
    {
        //Eq.(113) simplified and the derivative taken 
        fprime = (105.0f * u * (u * u - ks.alphaSquared)) / (8.0f * pi * ks.alphaPowSeven);

        //Eq.(57) 
        Wprime = (1.0f / (h * h * h * h)) * fprime;
    }
    else
        Wprime = 0;

    return Wprime;
}

static double Evaluate(double r, double h, KernelSettings ks)
{
    double u = r / h;
    double W = 0;
    double f = 0;
    float pi = 3.14159265358979323846f;

    if (u <= ks.alpha)
    {
        //Eq.(113) simplified
        f = (105.0f / (32.0f * pi * ks.alphaPowThree)) * pow((1 - (u*u / ks.alphaSquared)), 2);

        //Eq.(53)
        W = (1.0 / (h * h * h)) * f;
    }
    else
        W = 0;

    return W;
}

static Vec3 GetUnitVector(Particle a, Particle b, float length) 
{
    return 
    {
        (b.cPosition.x - a.cPosition.x) / length,
        (b.cPosition.y - a.cPosition.y) / length,
        (b.cPosition.z - a.cPosition.z) / length
    };
}

static Vec3 GetAbsolutVector(Vec3 v) 
{
    return
    {
        std::abs(v.x),
        std::abs(v.y),
        std::abs(v.z)
    };
}

static Vec3 UnitizeVector(Vec3 v)
{
    float length = std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z);

    return
    {
        v.x / length,
        v.y / length,
        v.z / length
    };
}

static float GetSmallest(float v1, float v2, float v3) 
{
    if (v1 < v2 && v1 < v3)
        return v1;
    else if (v2 < v1 && v2 < v3)
        return v2;

    return v3;
}

static float GetVectorLength(Vec3 v) 
{
    return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

static float Rad2Deg(float rad) 
{
    return rad * 180 / 3.14159265359f;
}

static float Deg2Rad(float deg)
{
    return deg * 3.14159265359 / 180.0f;
}

static bool IsSameVector(Vec3 v1, Vec3 v2) 
{
    float xDiff = v1.x - v2.x;
    float yDiff = v1.y - v2.y;
    float zDiff = v1.z - v2.z;
    float small = 1e-10;

    if ((xDiff < small) && (yDiff < small) && (zDiff < small))
        return true;

    return false;
}

static float GetAddedVectorLength(Vec3 v1, Vec3 v2)
{
    Vec3 v = { v1.x + v2.x, v1.y + v2.y, v1.z + v2.z };

    return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

static bool InsideSliceBC(float rLimit, float aLimit, Vec3 vBase, Vec3 vParticle) 
{  
    //Project to the yz plane
    vBase.x = 0;
    vParticle.x = 0;

    float r = GetVectorLength(vParticle);

    Vec3 vBaseU = UnitizeVector(vBase);
    Vec3 vParticleU = UnitizeVector(vParticle);

    float scalarProd = vBaseU.y * vParticleU.y + vBaseU.z * vParticleU.z; //vBaseU.x * vParticleU.x;

    float aRad = std::acos(scalarProd);
    float aDeg = Rad2Deg(aRad);


    if ((aDeg < aLimit || aDeg >(180 - aLimit)) && r > rLimit) 
    {
        return true;
    }
    return false;
}

static bool InsideHeightBC(float hLimit, Vec3 vParticle) 
{
    if (std::abs(vParticle.z) > hLimit)
        return true;

    return false;
}

static bool InsideLengthBC(float lLimit, Vec3 vParticle)
{
    if (std::abs(vParticle.x) < lLimit)
        return true;

    return false;
}

static bool IsParticleInsideCylinder(Vec3 pPos, LoadingCylinder LC) 
{
    Vec3 projPos = { pPos.x, 0.0f, pPos.z };

    Vec3 LCprojPos = { LC.cPosition.x, 0.0f, LC.cPosition.z };

    float d = Distance(projPos, LCprojPos);

    if (d <= LC.Radius)
        return true;

    return false;
}

static Vec3 GetAverageVector(Vec3 v1, Vec3 v2) 
{
    return { (v1.x + v2.x) / 2.0f, (v1.y + v2.y) / 2.0f, (v1.z + v2.z) / 2.0f };
}

static Vec3 CalcParticleProjectionOnCylinder(Vec3 pPos, LoadingCylinder LC)
{
    Vec3 vec = GetVector(LC.cPosition, pPos);
    vec.y = 0.0f;
    Vec3 vecScaled = GetVector(LC.cPosition, pPos);
    vecScaled.y = 0.0f;
    
    Vec3 vecDiff = { 0, 0, 0};

    float vL = GetVectorLength(vec);

    //Unitize vector
    vecScaled.x = vecScaled.x / vL;
    //vecScaled.y = vecScaled.y / vL;
    vecScaled.z = vecScaled.z / vL;

    //Scale vector
    vecScaled.x = vecScaled.x * LC.Radius;
    //vecScaled.y = vecScaled.y * LC.Radius;
    vecScaled.z = vecScaled.z * LC.Radius;

    vecDiff.x = vecScaled.x - vec.x;
    //vecDiff.y = vecScaled.y - vec.y;
    vecDiff.z = vecScaled.z - vec.z;

    return vecDiff;
}

static bool ListContains(std::vector<int> indices, int index)
{
    for (int i = 0; i < indices.size(); i++)
    {
        if (indices[i] == index)
            return true;
    }
    return false;
}

static int GetZoneIndex(Vec3 pt, TetraZoneSettings tzs)
{
    float x = pt.x;
    float y = pt.y;
    float z = pt.z;

    int zoneIndexX = (int)((x + tzs.add.x) / tzs.zoneStep.x);
    int zoneIndexY = (int)((y + tzs.add.y) / tzs.zoneStep.y);
    int zoneIndexZ = (int)((z + tzs.add.z) / tzs.zoneStep.z);

    //To avoid round off errors with the zone indexing
    if (zoneIndexX >= tzs.zoneCount.x) zoneIndexX = tzs.zoneCount.x - 1;
    if (zoneIndexY >= tzs.zoneCount.y) zoneIndexY = tzs.zoneCount.y - 1;
    if (zoneIndexZ >= tzs.zoneCount.z) zoneIndexZ = tzs.zoneCount.z - 1;

    int zoneIndex = zoneIndexX + zoneIndexY * tzs.zoneCount.x + zoneIndexZ * tzs.zoneCount.x * tzs.zoneCount.y;

    return zoneIndex;
}

static bool IsInsideTetra(Vec4 bcc)
{
    if (bcc.x < 0 || bcc.y < 0 || bcc.z < 0 || bcc.w < 0)
        return false;
    
    return true;
}

static Vec3 CrossProduct(Vec3 a, Vec3 b)
{
    Vec3 c;
    c.x = a.y * b.z - a.z * b.y;
    c.y = a.z * b.x - a.x * b.z;
    c.z = a.x * b.y - a.y * b.x;
    return c;
}

static float ScTP(Vec3 a, Vec3 b, Vec3 c)
{
    // computes scalar triple product
    Vec3 cross = CrossProduct(b, c);
    float dot = (a.x * cross.x) + (a.y * cross.y) + (a.z * cross.z);
    return dot;
}

static Vec4 BarycentricCoord(Vec3 a, Vec3 b, Vec3 c, Vec3 d, Vec3 p)
{
    Vec3 vap = {p.x - a.x, p.y - a.y, p.z - a.z};
    Vec3 vbp = {p.x - b.x, p.y - b.y, p.z - b.z}; 

    Vec3 vab = {b.x - a.x, b.y - a.y, b.z - a.z}; 
    Vec3 vac = {c.x - a.x, c.y - a.y, c.z - a.z};
    Vec3 vad = {d.x - a.x, d.y - a.y, d.z - a.z}; 

    Vec3 vbc = {c.x - b.x, c.y - b.y, c.z - b.z };
    Vec3 vbd = {d.x - b.x, d.y - b.y, d.z - b.z };  
    // ScTP computes the scalar triple product
    float va6 = ScTP(vbp, vbd, vbc);
    float vb6 = ScTP(vap, vac, vad);
    float vc6 = ScTP(vap, vad, vab);
    float vd6 = ScTP(vap, vab, vac);
    float v6 = 1.0f / ScTP(vab, vac, vad);

    return {va6 * v6, vb6 * v6, vc6 * v6, vd6 * v6};
}

static float TetraVolume(Vec3 a, Vec3 b, Vec3 c, Vec3 d) 
{
    Vec3 vab = { b.x - a.x, b.y - a.y, b.z - a.z };
    Vec3 vac = { c.x - a.x, c.y - a.y, c.z - a.z };
    Vec3 vad = { d.x - a.x, d.y - a.y, d.z - a.z };

    Vec3 cross = CrossProduct(vab, vac);
    float dot = (vad.x * cross.x) + (vad.y * cross.y) + (vad.z * cross.z);

    return (1.0f / 6.0f) * dot;
}

static float CalcTotTetraVolume(std::vector<std::vector<Vec3>> tetras) 
{
    float V = 0;
    float v = 0;

    for (int i = 0; i < tetras.size(); i++)
    {
        if (tetras[i].size() == 4)
            v = TetraVolume(tetras[i][0], tetras[i][1], tetras[i][2], tetras[i][3]);

        V += v;
    }

    return V;
}

//Calculate tetrahedron points as mid points on faces and edges to complement the corners.  
static std::vector<Vec3> GetTetraExtraVertices(Vec3 a, Vec3 b, Vec3 c, Vec3 d) 
{
    std::vector<Vec3> extras;

    Vec3 abc = { (1.0f / 3.0f) * (a.x + b.x + c.x), (1.0f / 3.0f) * (a.y + b.y + c.y), (1.0f / 3.0f) * (a.z + b.z + c.z) };
    Vec3 bcd = { (1.0f / 3.0f) * (b.x + c.x + d.x), (1.0f / 3.0f) * (b.y + c.y + d.y), (1.0f / 3.0f) * (b.z + c.z + d.z) };
    Vec3 cda = { (1.0f / 3.0f) * (c.x + d.x + a.x), (1.0f / 3.0f) * (c.y + d.y + a.y), (1.0f / 3.0f) * (c.z + d.z + a.z) };
    Vec3 dab = { (1.0f / 3.0f) * (d.x + a.x + b.x), (1.0f / 3.0f) * (d.y + a.y + b.y), (1.0f / 3.0f) * (d.z + a.z + b.z) };

    Vec3 ab = { (1.0f / 2.0f) * (a.x + b.x), (1.0f / 2.0f) * (a.y + b.y), (1.0f / 2.0f) * (a.z + b.z)};
    Vec3 ac = { (1.0f / 2.0f) * (a.x + c.x), (1.0f / 2.0f) * (a.y + c.y), (1.0f / 2.0f) * (a.z + c.z) };
    Vec3 ad = { (1.0f / 2.0f) * (a.x + d.x), (1.0f / 2.0f) * (a.y + d.y), (1.0f / 2.0f) * (a.z + d.z) };

    Vec3 bc = { (1.0f / 2.0f) * (b.x + c.x), (1.0f / 2.0f) * (b.y + c.y), (1.0f / 2.0f) * (b.z + c.z) };
    Vec3 bd = { (1.0f / 2.0f) * (b.x + d.x), (1.0f / 2.0f) * (b.y + d.y), (1.0f / 2.0f) * (b.z + d.z) };
    
    Vec3 cd = { (1.0f / 2.0f) * (c.x + d.x), (1.0f / 2.0f) * (c.y + d.y), (1.0f / 2.0f) * (c.z + d.z) };

    extras = std::vector<Vec3>{ abc, bcd, cda, dab, ab, ac, ad, bc, bd, cd };

    return extras;
}

static float VectorAngle(Vec3 v1, Vec3 v2) 
{
    //Assume that v1 and v2 are unitized.
    float dot = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    float angle = std::acos(dot);
    return angle;
}

static float Remap(float newDomainStart, float newDomainEnd, float currentDomainStart, float currentDomainEnd, float value) 
{
    float newDomDiff = newDomainEnd - newDomainStart;

    float curDomDiff = currentDomainEnd - currentDomainStart;

    float curPercent = (value - currentDomainStart) / curDomDiff;

    float newValue = newDomainStart + curPercent * newDomDiff;

    return newValue;
}

static std::vector<float> GetPlaneEquation(Plane plane) 
{
    Vec3 origin = plane.Origin;
    Vec3 normal = plane.Normal;

    float d = -1 * (normal.x * origin.x + normal.y * origin.y + normal.z * origin.z);
    std::vector<float> abcd = std::vector<float>(4);

    abcd[0] = normal.x;
    abcd[1] = normal.y;
    abcd[2] = normal.z;
    abcd[3] = d;

    return abcd;
}


static bool IsPointOnNormalSide(Plane p, Vec3 pt)
{
    float dot = (p.a * pt.x) + (p.b * pt.y) + (p.c * pt.z) + (p.d * 1.0);
    if (dot > 0.0f)
        return true;

    return false;

}

static bool IsArmCrossingCrack(Particle pa, Particle pb, Crack c) 
{
    bool bool_a = IsPointOnNormalSide(c.plane, pa.iPosition);
    bool bool_b = IsPointOnNormalSide(c.plane, pb.iPosition);

    if (bool_a != bool_b) 
    {
        float d_a = Distance(pa.iPosition, c.plane.Origin);
        float d_b = Distance(pb.iPosition, c.plane.Origin);
        
        if (d_a < c.radius && d_b < c.radius)
            return true;
    }   

    return false;
}



static int FindClosestVertex(std::vector<Particle> particles, int nParticles, Vec3 pt) 
{
    float minD = 1e10;
    int index = -1;

    for (int i = 0; i < nParticles; i++) 
    {
        float d = Distance(particles[i].iPosition, pt);

        if (d < minD) 
        {
            minD = d;
            index = i;
        }
    }

    return index;
}


static int GenRandomInt(int min, int max)
{
    srand(1);
    int rnd1 = rand();
    int rnd = rnd1 / (RAND_MAX);
    int diff = max - min;
    int val = min + rnd * diff;
    return val;
}