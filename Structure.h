#pragma once
#include "Common.h"

using namespace std;

class Structure
{

public:
	Structure();
	Structure(int nA,
				int nP,
				int nZ,
				int ppCount,
				int paCount,
				Vertex* vertices,
				ArmIndices* arms,
				float* massSize,
				Vec3* loadVecs,
				bool* isLoaded,
				bool* isLocked,
				bool* isSlider,
				int* ppTop, 
				int* ppStartIndex, 
				int* ppRowLength, 
				int* paTop, 
				int* paStartIndex, 
				int* paRowLength, 
				SimulationType simType,
				ImportType impType,
				GeometrySettings ms,
				SolverSettings solverSettings,
				MaterialSettings materialSettings,
				KernelSettings kernelSettings, 
				DisplacementSettings dispSettings,
				Stats stats,
				string suffix);

	~Structure();

	void SetupKernel(float alpha);
	void SetupLoad();
	void SetupSpring();
	void SetupPoissonsRatio();
	void SetupAnisotropy();
	void SetupCracks();
	Crack CreateCrack(Vec3 crackPosition, float crack_radius, crackDirection crackDir);
	void CreateRandomCracks();



	void CreateStructure(Vertex* vertices, ArmIndices* arms, float* massSize, Vec3* loadVecs, bool* isLoaded, bool* isLocked, bool* isSlider);
	void CopyTopology(int* ppTop, int* ppStartIndex, int* ppRowLength, int* pbTop, int* pbStartIndex, int* pbRowLength);

	void RunAnalysis_BoneRemodelling();
	void RunAnalysis_SteelComponent();
	void RunAnalysis_SteelTest();

	void ZeroParticleForce();
	void CalcArmForce();
	void CalcArmForceSimpleEnergy();
	void CalcParticleMeanStrain();
	void CalcMass();
	void CalcDisplacementEnds();
	void CalcStrain();
	void SaveResults();
	void CalcParticleFictiousMass();

	void SetAnalysisType(bool isNonLinear);

	void UpdateParticleLife();

	void ScaleStiffnessArms();

	void SumVelocity();

	void ImplementCracks();

	void CalcStressEnds();
	void CalcConvergenceForce();
	void CalcPoissonsRatio();
	void CalcPoissonsRatioVertical();
	void CalcSectionStress();
	void CountBrokenArms();
	void CreateGaugePlanes();

	void CalcFirstPartialVelocity();
	void CalcSecondPartialVelocity();
	
	void CalcAcceleration();
	void ApplyExternalSpringDisp();
	void ApplyExternalSpringDisp_New();
	void TestConsitencyCondition();

	void OutputResults();
	void CollectData();
	void ExportModel();

public:
	std::vector<Arm> m_arms;
	std::vector<Particle> m_particles;
	std::vector<Vec3> m_pInitPos;
	std::vector<bool> m_isLoaded;
	std::vector<bool> m_isLocked;
	std::vector<bool> m_isSlider;

	MaterialSettings m_material;
	DisplacementSettings m_disp;

	std::vector<LoadingCylinder> m_loadCylinders;
	std::vector<double> m_incDisp;
	std::vector<double> m_incStrain;
	std::vector<double> m_incGaugeStrain;

	std::vector<double> m_incVelocity;
	std::vector<double> m_incStress;

	std::vector<Crack> m_cracks;

	bool m_isCracked;
	crackDirection m_crackDirection;
	float m_sfMax;

	std::vector<gaugePlane> m_gaugePlane;

	bool m_isAnisotrop;
	Anisotropy m_anisotropy;

private:	
	
	KernelSettings m_kernel;
	SolverSettings m_solver;
	LoadSettings m_load;
	SpringSettings m_spring;
	PoissonsRatioSettings m_prs;
	GeometrySettings m_gs;
	
	Stats m_stats;

	int m_nParticles; 
	int m_nArms;
	int m_nZones;
	int m_nBrokenArms;
	int m_nCracks;

	int m_pClosestToOrigin;
	double m_gaugeSpacing;

	float m_avrgArmStrain;
	float m_avrgParticleMeanStrain;



	std::vector<Vec3> m_loadDir;


	std::vector<double> m_avrgDispTotVer;
	std::vector<double> m_avrgDispTop;
	std::vector<double> m_avrgDispBot;

	std::vector<double> m_avrgDispGaugeTot;
	std::vector<double> m_avrgDispGaugeTop;
	std::vector<double> m_avrgDispGaugeBot;

	std::vector<double> m_avrgDispTotHor;
	std::vector<double> m_avrgDispLeft;
	std::vector<double> m_avrgDispRight;
	
	std::vector<double> m_incLoad;
	std::vector<double> m_incSpringForceTop;
	std::vector<double> m_incSpringForceBot;
	std::vector<double> m_incSectionStress;
	std::vector<double> m_incSectionStressCom;
	std::vector<double> m_incSectionStressTen;
	std::vector<double> m_reactionForceTop;
	std::vector<double> m_reactionForceBot;
	std::vector<double> m_incObfAvrg;
	std::vector<double> m_incPossionsRatio;

	std::vector<double> m_kernelConcistency;
	
	//Particle-particle topology
	int* m_ppTop;
	int* m_ppStartIndex;
	int* m_ppRowLength;
	int m_ppCount;

	bool m_plasticityOn;
	bool m_CheckFracture;
	BC m_bc;
	SimulationType m_simType;
	ImportType m_importType;
	bool m_scaleStiffness;
	float m_maxStrainEnergy;

	std::vector<int> m_ppTopV;
	std::vector<int> m_ppStartIndexV;
	std::vector<int> m_ppRowLengthV;

	//Particle-arm topology
	int* m_paTop;
	int* m_paStartIndex;
	int* m_paRowLength;
	int m_paCount;

	std::vector<int> m_paTopV;
	std::vector<int> m_paStartIndexV;
	std::vector<int> m_paRowLengthV;

	string m_fileNameSuffix;

};


