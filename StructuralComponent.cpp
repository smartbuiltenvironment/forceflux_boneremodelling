
#include "StructuralComponent.h" 

StructuralComponent::StructuralComponent(SimulationType simType, ImportType importType, SolverSettings solverSettings, MaterialSettings materialSettings, KernelSettings kernelSettings, DisplacementSettings dispSettings, float strainLimit, string suffix)
{
	InitialiseParameters();

	m_strainLimit = strainLimit;
	m_nDiscSides = 20;

	m_armIndexCounter = 0;
	m_armCounter = 0;
	m_alpha = kernelSettings.alpha;
	m_gs.rndSeed = kernelSettings.rndSeed;

	pArrangement pArr = pArrangement::irregular;
	m_simType = simType;

	m_importType = importType;

	auto start = std::chrono::high_resolution_clock::now();

	std::cout << "--------------------------Setting up geometry------------------------" << std::endl;

	if(m_importType == ImportType::Femur2D || m_importType == ImportType::Frame2D)
	{
		string fileName = "";
		Arr3 zoneCountParticles = { 1,1,1 };
		m_colorCap = 0.2;

		if (m_importType == ImportType::Femur2D) 
		{
			fileName = "import\\finefemurCoord_Fixed.txt";
			zoneCountParticles = { 20,20,1 };
			m_nParticles = 10000;
			m_orthoSF = 0.06;
		}
		else if (m_importType == ImportType::Frame2D)
		{
			fileName = "import\\frame_Fixed.txt";
			zoneCountParticles = { 50,30,1 };
			m_nParticles = 50000; //50000;
			m_orthoSF = 0.1;
		}
		CreateContainer2D(fileName);
		SetupBB2D(zoneCountParticles);
	}
	else //3D simulation
	{
		string fileName = "";
		Arr3 zoneCountTetra = {1,1,1};
		Arr3 zoneCountParticles = {1,1,1};
		m_colorCap = 0.2;
		m_selectionCap = 0.0f;

		if (m_importType == ImportType::Gerbretter3D)
		{
			fileName = "import\\gerbCoord3_Fixed.txt";
			zoneCountTetra = { 3, 1, 1 };
			zoneCountParticles = { 30, 6, 6};
			m_nParticles = 300000;//500000;
			m_orthoSF = 0.12;
		}
		else if (m_importType == ImportType::Femur3D)
		{
			fileName = "import\\femurCoord3D_Fixed.txt";
			zoneCountTetra = { 1, 1, 3 };
			zoneCountParticles = { 8, 4, 12 };
			m_nParticles = 100000;
			m_orthoSF = 0.062;
		}
		else if (m_importType == ImportType::TwaColumn3D)
		{
			fileName = "import\\twaCoord_Fixed.txt";
			zoneCountTetra = { 1, 2, 4 };
			zoneCountParticles = { 8, 10, 10 };
			m_nParticles = 400000;
			m_orthoSF = 0.08;
		}
		else if (m_importType == ImportType::Tamedia3D)
		{
			fileName = "import\\tamediaCoord_Fixed2.txt";
			zoneCountTetra = { 2, 4, 4 };
			zoneCountParticles = { 6, 10, 10 };
			m_nParticles = 200000;
			m_orthoSF = 0.07;	//0.06
		}
		else if (m_importType == ImportType::TetraNode3D)
		{
			fileName = "import\\tetraNodeCoord_Fixed.txt";
			zoneCountTetra = { 4, 4, 4 };
			zoneCountParticles = { 10, 10, 10 };
			m_nParticles = 200000;
			m_orthoSF = 0.07;	//0.06
		}
		else if (m_importType == ImportType::NodeRotSym3D)
		{
			fileName = "import\\nodeRotationSymmetry_Fixed.txt";
			zoneCountTetra = { 6, 6, 2 };
			zoneCountParticles = { 10, 10, 2 };
			m_nParticles = 200000;
			m_orthoSF = 0.07;	//0.06
		}
		else if (m_importType == ImportType::NodeRotSymHole3D)
		{
			fileName = "import\\nodeRotationSymmetryHole_2_Fixed.txt";
			zoneCountTetra = { 10, 10, 2 };
			zoneCountParticles = { 18, 18, 4 };
			m_nParticles = 1000000;  //400000;
			m_orthoSF = 0.08;	//0.06
		}
		else if (m_importType == ImportType::Pipe3D)
		{
			fileName = "import\\pipe_Fixed.txt";
			zoneCountTetra = { 2, 10, 2 };
			zoneCountParticles = { 6, 30, 6 };
			m_nParticles = 300000;  //400000;
			m_orthoSF = 0.08;	//0.06
		}

		CreateContainer3D(fileName);
		SetupBB3D(zoneCountParticles);
		TetraZoning(zoneCountTetra);
	}

	InitialiseArrays();

	if (m_importType == ImportType::Femur2D || m_importType == ImportType::Frame2D)
		CreateParticlesRegularInPolygon();
	else //3D simulation
		CreateParticlesRegularTetras();

	CalcTargetSize();

	CreateZones();

	GenFlatTopology1();

	if (pArr == pArrangement::irregular)
	{
		CalcBaseSize(10, 6);

		CalcMassSize();

		ShakeParticlesIteration(100);
	}

	if (m_importType == ImportType::Femur2D || m_importType == ImportType::Frame2D)
	{	
		if(m_importType == ImportType::Femur2D)
			UpdateBcImportFemur2D();
		else if (m_importType == ImportType::Frame2D)
			UpdateBcImportFrame2D();
	}
	else //3D simulation
	{
		if (m_importType == ImportType::Femur3D)
			UpdateBcImportFemur3D();
		else if (m_importType == ImportType::Gerbretter3D)
			UpdateBcImportGerberette();
		else if (m_importType == ImportType::TwaColumn3D)
			UpdateBcImportTwaColumn();
		else if (m_importType == ImportType::TetraNode3D)
			UpdateBcImportTetraNode();
		else if (m_importType == ImportType::NodeRotSym3D)
			UpdateBcImportNodeRotSym(0.055);
		else if (m_importType == ImportType::NodeRotSymHole3D)
			UpdateBcImportNodeRotSym(0.071);
		else if (m_importType == ImportType::Pipe3D)
			UpdateBcImportPipe(0.071);
	}
	
	CalcBaseSize(10, 6);

	CalcMassSize();

	CalcHaSize(m_alpha);

	GenDiscs();

	GenIcosas();

	CreateArms();

	GenFlatTopology2();

	ComputeStats(materialSettings);

	//Only copied once since the topology never changes
	//m_ffCuda.CopyFromHostToDevice1(m_pzTopFlat, m_pHaSize, m_pCurrentFriendsCount);
	//m_ffCuda.CopyFromHostToDevice2(m_zzTopFlat, m_zzCounter, m_zzStartIndex, m_zzRowLength);
	//m_ffCuda.CopyFromHostToDevice3(m_zpTopFlat, m_zpCounter, m_zpStartIndex, m_zpRowLength);
	//m_ffCuda.CopyFromHostToDevice4(m_ppTopFlat, m_ppCounter, m_ppStartIndex, m_ppRowLength);

	std::cout << "-------------------------- Setting up structure ----------------------" << std::endl;


	std::cout << "PARTICLE COUNT: " << m_nParticles << std::endl;

	m_structure = Structure(
		m_nParticles,
		m_armCounter,
		m_nZones,
		m_ppCounter,
		m_paCounter,
		m_vertices,
		m_armi,
		m_pMassSize,
		m_loadDir,
		m_isLoaded,
		m_isLocked,
		m_isSlider,
		m_ppTopFlat,
		m_ppStartIndex,
		m_ppRowLength,
		m_paTopFlat,
		m_paStartIndex,
		m_paRowLength,
		m_simType,
		m_importType,
		m_gs, 
		solverSettings, 
		materialSettings, 
		kernelSettings,
		dispSettings,
		m_stats,
		suffix);

	std::cout << "-------------------------- Setting up OpenGL ----------------------" << std::endl;

	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<float> duration = end - start;
	std::cout << "Time elapsed: " << duration.count() << " seconds" << std::endl;
	m_startTimeAnalysis = std::chrono::high_resolution_clock::now();

}

StructuralComponent::~StructuralComponent()
{
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0));
	GLCall(glBindVertexArray(0));
	m_Shader->Unbind();

	delete[] m_Indices;
	delete[] m_arms;
	delete[] m_arms2;
	delete[] m_zoneLineIndices;
	delete[] m_pzTopFlat;
	delete[] m_vertices;
	delete[] m_vertices2;
	delete[] m_zoneVertices1D;
	delete[] m_discIndices;
	delete[] m_discVertices;
	delete[] m_icosaIndices;
	delete[] m_icosaVertices;
	delete[] m_icosaCenter;

	delete[] m_pBaseSize;
	delete[] m_pMassSize;
	delete[] m_pHaSize;
	delete[] m_pMass;

	delete[] m_loadDir;
	delete[] m_ppTopFlat;

	delete[] m_zzTopFlat;
	delete[] m_zzStartIndex;
	delete[] m_zzRowLength;

	delete[] m_zpTopFlat;
	delete[] m_zpStartIndex;
	delete[] m_zpRowLength;

}

//Initialise
void StructuralComponent::InitialiseParameters()
{
	m_Proj = glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f / 9.0f, 0.01f, 100.0f);
	m_View = glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up);
	m_Model = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));			//glm::mat4 m4(1.0f) => constructs an identity matrix
	m_Trans = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));
	m_Rot = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));
	m_TranslationA = glm::vec3(0, 0, 0);
	m_TranslationB = glm::vec3(0, 0, 0);
	m_clipDistance1 = 0.08f;
	m_clipDistance2 = 0.08f;
	m_clipDistance3 = 0.08f;
	m_clipDistance4 = 0.08f;
	m_btnRunAnalysis = false;
	m_btnRunShaking = false;
	m_btnRunOutput = false;
	m_btnRunExport = false;
	m_colorParticleBy = colorParticleBy::displacement;
	m_colorArmBy = colorArmBy::forceComTen;
	m_selectedItemArmColorBy = 4;
	m_scaleFactor = 1.0;
	m_enableClipping = true;
	m_XrayYZ = false;
	m_XrayDir = 1;
	m_XrayDir2 = -1;
	m_AniDefDir = 1;
	m_btnScaleStiffness = false;
	m_viewSpinning = false;
	m_btnViewSpinning = false;
	m_spinAngle = 0.0f;
	m_spinStep = 1.5f;
	m_btnViewTop = false;

	m_tol = 0.000001;
	m_runCounter = 0;
	m_tenCounter = 0;
	m_shakeCounter = 0;
	m_nParticles = 30000;		//Images made for nP = 30000.
	m_Horizon = 0.013f;
	m_runAnalysis = false; //true;
	m_runShaking = false;
	m_runOutput = false;
	m_min = 0.0f;
	m_sec = 0.0f;
}

void StructuralComponent::InitialiseArrays()
{
	int armsPerParticleEstimate = 150;

	m_Indices = new unsigned int[m_nParticles];
	m_zoneLineIndices = new unsigned int[m_nZones * 24]; //Allocating a little more space than nessecary.
	m_arms = new unsigned int[m_nParticles * armsPerParticleEstimate]; //Allocating more space than nessecary.
	m_arms2 = new unsigned int[m_nParticles * armsPerParticleEstimate]; //Allocating more space than nessecary.
		
	m_armi = new ArmIndices[m_nParticles * armsPerParticleEstimate]; //Allocating more space than nessecary.

	m_icosaIndices = new unsigned int[m_nParticles * 20 * 3];

	m_vertices = new Vertex[m_nParticles];
	m_vertices2 = new Vertex[m_nParticles * armsPerParticleEstimate * 2];
	m_vertices3 = new Vertex[m_nParticles * armsPerParticleEstimate * 2];

	m_icosaCenter = new Vertex[m_nParticles];
	m_pzTopFlat = new int[m_nParticles];
	m_zoneVertices1D = new Vertex[m_nZones * 8];
	m_icosaVertices = new Vertex[m_nParticles * 12];

	m_pBaseSize = new float[m_nParticles];
	m_pMassSize = new float[m_nParticles];
	m_pHaSize = new float[m_nParticles];
	m_pMass = new float[m_nParticles];

	m_discIndices = new unsigned int[m_nParticles * m_nDiscSides * 3];
	m_discVertices = new Vertex[m_nParticles * (m_nDiscSides + 1)];

	m_loadDir = new Vec3[m_nParticles];
	m_isLoaded = new bool[m_nParticles];
	m_isLocked = new bool[m_nParticles];
	m_isSlider = new bool[m_nParticles];
}

void StructuralComponent::InitialiseOpenGL()
{
	m_Shader = make_unique<Shader>("resources/shaders/FFShader.shader");

	m_drawParticles = true;
	m_drawBonds = false;
	m_drawBonds2 = true;
	m_drawBroken = false;
	m_drawZones = false;
	m_drawDisc = false;
	m_drawIcosa = false;

	m_viewType = viewType::perspective;

	Vec3 target = { 0.0f, 0.0f, 0.0f };

	m_Camera = Camera(0.3f, target);
	
	GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));

	GLCall(glGenVertexArrays(1, &m_pIndexVA));
	GLCall(glBindVertexArray(m_pIndexVA));

	GLCall(glGenBuffers(1, &m_pIndexVB));
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_pIndexVB));
	GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_nParticles * 10, nullptr, GL_DYNAMIC_DRAW)); //Allocating more then needed 

	GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
	GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

	GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
	GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

	glGenBuffers(1, &m_pIndexIB);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pIndexIB);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_nParticles, m_Indices, GL_STATIC_DRAW);


	//Zone lines
	GLCall(glGenVertexArrays(1, &m_zLineIndexVA));
	GLCall(glBindVertexArray(m_zLineIndexVA));

	GLCall(glGenBuffers(1, &m_zLineIndexVB));
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_zLineIndexVB));
	GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_zVertexCounter, nullptr, GL_STATIC_DRAW));

	GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
	GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

	GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
	GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

	GLCall(glGenBuffers(1, &m_zLineIndexIB));
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_zLineIndexIB));
	GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_zLineIndexCounter, m_zoneLineIndices, GL_STATIC_DRAW));


	//Bond lines
	GLCall(glGenVertexArrays(1, &m_bLineIndexVA));
	GLCall(glBindVertexArray(m_bLineIndexVA));

	GLCall(glGenBuffers(1, &m_bLineIndexVB));
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_bLineIndexVB));
	GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_nParticles, nullptr, GL_DYNAMIC_DRAW));

	GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
	GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

	GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
	GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

	GLCall(glGenBuffers(1, &m_bLineIndexIB));
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bLineIndexIB));
	GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_armIndexCounter, m_arms, GL_STATIC_DRAW));


	//Bond lines 2 with unique vertices for unique bond colors
	GLCall(glGenVertexArrays(1, &m_bLineIndexVA2));
	GLCall(glBindVertexArray(m_bLineIndexVA2));

	GLCall(glGenBuffers(1, &m_bLineIndexVB2));
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_bLineIndexVB2));
	GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_armIndexCounter, nullptr, GL_DYNAMIC_DRAW));

	GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
	GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

	GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
	GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

	GLCall(glGenBuffers(1, &m_bLineIndexIB2));
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bLineIndexIB2));
	GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_armIndexCounter, m_arms2, GL_STATIC_DRAW));

	//Disc triangles
	GLCall(glGenVertexArrays(1, &m_discIndexVA));
	GLCall(glBindVertexArray(m_discIndexVA));

	GLCall(glGenBuffers(1, &m_discIndexVB));
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_discIndexVB));
	GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_nParticles * (m_nDiscSides + 1), nullptr, GL_DYNAMIC_DRAW));

	GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
	GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

	GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
	GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

	GLCall(glGenBuffers(1, &m_discIndexIB));
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_discIndexIB));
	GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_nParticles * (m_nDiscSides * 3), m_discIndices, GL_STATIC_DRAW));


	//Icosahedron triangles
	GLCall(glGenVertexArrays(1, &m_icosaIndexVA));
	GLCall(glBindVertexArray(m_icosaIndexVA));

	GLCall(glGenBuffers(1, &m_icosaIndexVB));
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_icosaIndexVB));
	GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_nParticles * 12, nullptr, GL_DYNAMIC_DRAW));

	GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
	GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

	GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
	GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

	GLCall(glGenBuffers(1, &m_icosaIndexIB));
	GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_icosaIndexIB));
	GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_nParticles * 20 * 3, m_icosaIndices, GL_STATIC_DRAW));

	// depth test
	glEnable(GL_DEPTH_TEST);

	glPointSize(5); //1.5

	glLineWidth(0.1f);

}


//Import model
void StructuralComponent::CreateContainer2D(string fileName)
{
	string line;
	ifstream myfile(fileName);
	float x, y, z;
	int counter = 0;

	if (myfile.is_open())
	{
		while (std::getline(myfile, line, ','))
		{
			//std::cout << line << std::endl;
			if (counter == 0)
				x = std::stof(line);
			else if (counter == 1)
				y = std::stof(line);
			else if (counter == 2)
				z = std::stof(line);

			counter++;

			if (counter > 2)
			{
				m_outline.push_back({ x,y,z });
				counter = 0;
			}

		}
		myfile.close();

		std::cout << "Femur polygon create with " << m_outline.size() << " number of vertices." << std::endl;
	}
	else std::cout << "Unable to open file!!!";
}

void StructuralComponent::CreateContainer3D(string fileName) 
{
	string line;
	ifstream myfile(fileName);

	float x, y, z;
	int counter = 0;
	
	if (myfile.is_open())
	{
		while (std::getline(myfile, line, ','))
		{
			if (counter == 0)
				x = std::stof(line);
			else if (counter == 1)
				y = std::stof(line);
			else if (counter == 2)
				z = std::stof(line);

			counter++;

			if (counter > 2)
			{
				m_tetraVertices.push_back({ x,y,z });
				counter = 0;
			}
		}
		myfile.close();

		std::cout << "Tetra vertices total count: " << m_tetraVertices.size() << std::endl;
	}
	else cout << "Unable to open file!!!";

	int vertexCounter = 0;	
	vector<Vec3> tetraVertices;
	vector<Vec3> extraVertices;

	for (int i = 0; i < m_tetraVertices.size(); i++)
	{
		tetraVertices.push_back(m_tetraVertices[i]);
		vertexCounter++;
		
		//Assuming each tetrahedron has 4 vertices in consecutiv order
		if (vertexCounter == 4)
		{
			m_tetras.push_back(tetraVertices);
			extraVertices = GetTetraExtraVertices(tetraVertices[0], tetraVertices[1], tetraVertices[2], tetraVertices[3]);
			m_tetrasExtraVertices.push_back(extraVertices);

			extraVertices.clear();
			tetraVertices.clear();
			vertexCounter = 0;
		}
	}

	std::cout << "Tetra containers count: " << m_tetras.size() << std::endl;
}

//Setup boundingbox for imported geometry
void StructuralComponent::SetupBB2D(Arr3 zoneCount)
{
	m_gs.bcThickness = 0.005;

	Vec3 min = { 0.0f, 0.0f, 0.0f };
	Vec3 max = { 0.0f, 0.0f, 0.0f };

	for (int i = 0; i < m_outline.size(); i++)
	{
		if (m_outline[i].x < min.x) { min.x = m_outline[i].x; }
		if (m_outline[i].y < min.y) { min.y = m_outline[i].y; }
		if (m_outline[i].z < min.z) { min.z = m_outline[i].z; }

		if (m_outline[i].x > max.x) { max.x = m_outline[i].x; }
		if (m_outline[i].y > max.y) { max.y = m_outline[i].y; }
		if (m_outline[i].z > max.z) { max.z = m_outline[i].z; }
	}

	m_containerBB.min = min;
	m_containerBB.max = max;

	float xDom = max.x - min.x;
	float yDom = max.y - min.y;

	m_gs.zoneCount.x = zoneCount.x;
	m_gs.zoneCount.y = zoneCount.y;
	m_gs.zoneCount.z = zoneCount.z;

	m_gs.bbSize.x = xDom;
	m_gs.bbSize.y = yDom;
	m_gs.bbSize.z = xDom / 10.0f;

	m_nZones = m_gs.zoneCount.x * m_gs.zoneCount.y * m_gs.zoneCount.z;

	m_gs.zoneStep.x = m_gs.bbSize.x / m_gs.zoneCount.x;
	m_gs.zoneStep.y = m_gs.bbSize.y / m_gs.zoneCount.y;
	m_gs.zoneStep.z = m_gs.bbSize.z / m_gs.zoneCount.z;
}

void StructuralComponent::SetupBB3D(Arr3 zoneCount)
{
	m_gs.bcThickness = 0.005;

	Vec3 min = { 0.0f, 0.0f, 0.0f };
	Vec3 max = { 0.0f, 0.0f, 0.0f };

	for (int i = 0; i < m_tetraVertices.size(); i++)
	{
		if (m_tetraVertices[i].x < min.x) { min.x = m_tetraVertices[i].x; }
		if (m_tetraVertices[i].y < min.y) { min.y = m_tetraVertices[i].y; }
		if (m_tetraVertices[i].z < min.z) { min.z = m_tetraVertices[i].z; }

		if (m_tetraVertices[i].x > max.x) { max.x = m_tetraVertices[i].x; }
		if (m_tetraVertices[i].y > max.y) { max.y = m_tetraVertices[i].y; }
		if (m_tetraVertices[i].z > max.z) { max.z = m_tetraVertices[i].z; }
	}

	m_containerBB.min = min;
	m_containerBB.max = max;

	float xDom = max.x - min.x;
	float yDom = max.y - min.y;
	float zDom = max.z - min.z;

	int xCount = zoneCount.x;
	int yCount = zoneCount.y;
	int zCount = zoneCount.z;

	m_gs.zoneCount.x = xCount;
	m_gs.zoneCount.y = yCount;
	m_gs.zoneCount.z = zCount;

	m_gs.bbSize.x = xDom;
	m_gs.bbSize.y = yDom;
	m_gs.bbSize.z = zDom;

	m_nZones = m_gs.zoneCount.x * m_gs.zoneCount.y * m_gs.zoneCount.z;

	m_gs.zoneStep.x = m_gs.bbSize.x / m_gs.zoneCount.x;
	m_gs.zoneStep.y = m_gs.bbSize.y / m_gs.zoneCount.y;
	m_gs.zoneStep.z = m_gs.bbSize.z / m_gs.zoneCount.z;

	std::cout << "BB Size:" << xDom << ", " << yDom << ", " << zDom << std::endl;


}

//The tetrahedrons are sorted in zones to improve computational speed
void StructuralComponent::TetraZoning(Arr3 zoneCount) 
{
	m_tzs.domain.x = m_containerBB.max.x - m_containerBB.min.x;
	m_tzs.domain.y = m_containerBB.max.y - m_containerBB.min.y;
	m_tzs.domain.z = m_containerBB.max.z - m_containerBB.min.z;

	m_tzs.zoneCount.x = zoneCount.x;
	m_tzs.zoneCount.y = zoneCount.y;
	m_tzs.zoneCount.z = zoneCount.z;

	int zCount = m_tzs.zoneCount.x * m_tzs.zoneCount.y * m_tzs.zoneCount.z;

	m_tzs.zoneStep.x = m_tzs.domain.x / (float)m_tzs.zoneCount.x;
	m_tzs.zoneStep.y = m_tzs.domain.y / (float)m_tzs.zoneCount.y;
	m_tzs.zoneStep.z = m_tzs.domain.z / (float)m_tzs.zoneCount.z;

	m_tzs.add.x = m_tzs.domain.x / 2.0f;
	m_tzs.add.y = m_tzs.domain.y / 2.0f;
	m_tzs.add.z = m_tzs.domain.z / 2.0f;
	
	for (int i = 0; i < zCount; i++)
	{
		vector<int> zoneIndices;
		m_ztTop.push_back(zoneIndices);
	}

	//Sort the tetrahedrons into zones by checking vertex zone inclusion.
	for (int i = 0; i < m_tetras.size(); i++)
	{
		int tetraIndex = i;

		//Check if tetrahedron corner vertices are in the zone
		for (int j = 0; j < m_tetras[i].size(); j++)
		{
			Vec3 pt = m_tetras[i][j];
			int zoneIndex = GetZoneIndex(pt, m_tzs);

			if (zoneIndex >= zCount)
				std:cout<< "Zone Index is larger than expected at: " << zoneIndex << std::endl;

			if (!ListContains(m_ztTop[zoneIndex], tetraIndex))
				m_ztTop[zoneIndex].push_back(tetraIndex);
		}

		//Check if tetra face mid points and edge midpoints are in the zone
		for (int j = 0; j < m_tetrasExtraVertices[i].size(); j++)
		{
			Vec3 pt = m_tetrasExtraVertices[i][j];
			int zoneIndex = GetZoneIndex(pt, m_tzs);

			if (!ListContains(m_ztTop[zoneIndex], tetraIndex))
				m_ztTop[zoneIndex].push_back(tetraIndex);
		}
	}
}


//Particles
void StructuralComponent::CreateParticlesRegularInPolygon() 
{
	int nx, ny, nz, squarePCount, pCountPerSide;
	double ysf, xsf;

	ysf = m_gs.bbSize.y / m_gs.bbSize.x;
	squarePCount = (int)(m_nParticles / ysf);
	pCountPerSide = (int)std::pow(squarePCount, 1.0 / 2.0);

	nx = pCountPerSide;
	ny = (int)(pCountPerSide * ysf);
	nz = 1;

	float dx = m_gs.bbSize.x / nx;
	float dy = m_gs.bbSize.y / ny;
	float dz = 0;

	//Move geometry to the positive quadrand by adding this values.
	float addX = m_gs.bbSize.x / 2.0f;
	float addY = m_gs.bbSize.y / 2.0f;
	float addZ = 0.0f; //m_gs.bbSize.z / 2.0f;

	for (int i = 0; i < m_nZones; i++)
	{
		vector<int> zpi;
		m_zpTop.push_back(zpi);
	}

	float x, y, z, d, perc;
	//int attempts = 0;
	int pCounter = 0;
	m_zpCounter = 0;

	Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	//int zonesPerLevel = m_gs.zoneCount.x * m_gs.zoneCount.y;
	//int maxZoneIndex = m_gs.zoneCount.x * m_gs.zoneCount.y * m_gs.zoneCount.z - 1;
	float constraintsZlimit = 0; //m_zoneSideAverage * 0.45;

	float b = 0.0f;
	float corrFac = 0.0f;

	float radius = std::sqrt((m_gs.bbSize.x / 2.0f) * (m_gs.bbSize.x / 2.0f) + (m_gs.bbSize.y / 2.0f) * (m_gs.bbSize.y / 2.0f));

	m_zMin = -m_gs.bbSize.z / 2.0f + (dz / 2.0f);
	m_zMax = m_gs.bbSize.z / 2.0f - (dz / 2.0f);
	m_zPartST = m_zMax / 3.0f;

	z = 0.0f;
	for (int k = 0; k < nz; k++)
	{
		x = -m_gs.bbSize.x / 2.0f + (dx / 2.0f);
		for (int i = 0; i < nx; i++)
		{
			y = -m_gs.bbSize.y / 2.0f + (dy / 2.0f);
			for (int j = 0; j < ny; j++)
			{
				d = std::sqrt(x * x + y * y);

				bool inside = InsidePolygon(x, y); //InsideFemur(x, y, z);

				perc = 100 * (d / radius);

				if (inside)
				{
					int zoneIndexX = (int)((x + addX) / m_gs.zoneStep.x);
					int zoneIndexY = (int)((y + addY) / m_gs.zoneStep.y);
					int zoneIndexZ = (int)((z + addZ) / m_gs.zoneStep.z);

					//To avoid round off errors with the zone indexing
					if (zoneIndexX >= m_gs.zoneCount.x) zoneIndexX = m_gs.zoneCount.x - 1;
					if (zoneIndexY >= m_gs.zoneCount.y) zoneIndexY = m_gs.zoneCount.y - 1;
					if (zoneIndexZ >= m_gs.zoneCount.z)	zoneIndexZ = m_gs.zoneCount.z - 1;

					int zoneIndex = zoneIndexX + zoneIndexY * m_gs.zoneCount.x + zoneIndexZ * m_gs.zoneCount.x * m_gs.zoneCount.y;

					Vertex v;
					v.Position = { x, y, z };
					Vec4 color;

					color = GetBlendedColor(perc);

					v.Color = color;
					m_vertices[pCounter] = v;
					m_pzTopFlat[pCounter] = zoneIndex;

					m_zpTop[zoneIndex].push_back(pCounter);
					m_zpCounter++;

					pCounter++;
				}

				y += dy;
			}//y-loop

			x += dx;
		}//x-loop

		z += dz;
	}//z-loop

	if (pCounter > m_nParticles)
		std::cout << "!WARNING! particle count larger then allocation size" << std::endl;

	m_nParticles = pCounter;

	//Generate particle indices
	for (int i = 0; i < m_nParticles; i++)
		m_Indices[i] = i;

	std::cout << "Create regular femur particles completed" << std::endl;

	std::cout << "Created " << m_nParticles << std::endl;
}

void StructuralComponent::CreateParticlesRegularTetras()
{
	int nx, ny, nz, cubePCount, pCountPerSideOnMinCube;
	float zsf, ysf, xsf;

	float minSide = m_gs.bbSize.x;

	if (m_gs.bbSize.y < minSide)
		minSide = m_gs.bbSize.y;

	if (m_gs.bbSize.z < minSide)
		minSide = m_gs.bbSize.z;

	xsf = m_gs.bbSize.x / minSide;
	ysf = m_gs.bbSize.y / minSide;
	zsf = m_gs.bbSize.z / minSide;

	float totVol = m_gs.bbSize.x * m_gs.bbSize.y * m_gs.bbSize.z;

	float minVol = minSide * minSide * minSide;

	float volRatio = minVol / totVol;

	float minCubePCount = volRatio * m_nParticles;

	pCountPerSideOnMinCube = (int)std::pow(minCubePCount, 1.0 / 3.0);
 
	nx = (int)pCountPerSideOnMinCube * xsf;
	ny = (int)pCountPerSideOnMinCube * ysf;
	nz = (int)pCountPerSideOnMinCube * zsf;

	float dx = m_gs.bbSize.x / nx;
	float dy = m_gs.bbSize.y / ny;
	float dz = m_gs.bbSize.z / nz;

	//Move geometry to the positive quadrand by adding this values.
	float addX = m_gs.bbSize.x / 2.0f;
	float addY = m_gs.bbSize.y / 2.0f;
	float addZ = m_gs.bbSize.z / 2.0f;

	for (int i = 0; i < m_nZones; i++)
	{
		vector<int> zpi;
		m_zpTop.push_back(zpi);
	}

	float x, y, z, d, perc;
	int pCounter = 0;
	m_zpCounter = 0;

	Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	Vec4 coolor = { 1.0f, 0.0f, 0.0f, 1.0f };

	float constraintsZlimit = 0;

	float b = 0.0f;
	float corrFac = 0.0f;
	float radius = std::sqrt((m_gs.bbSize.x / 2.0f) * (m_gs.bbSize.x / 2.0f) + (m_gs.bbSize.y / 2.0f) * (m_gs.bbSize.y / 2.0f) + (m_gs.bbSize.z / 2.0f) * (m_gs.bbSize.z / 2.0f));

	m_zMin = -m_gs.bbSize.z / 2.0f + (dz / 2.0f);
	m_zMax = m_gs.bbSize.z / 2.0f - (dz / 2.0f);

	z = -m_gs.bbSize.z / 2.0f + (dz / 2.0f);
	for (int k = 0; k < nz; k++)
	{
		x = -m_gs.bbSize.x / 2.0f + (dx / 2.0f);
		for (int i = 0; i < nx; i++)
		{
			y = -m_gs.bbSize.y / 2.0f + (dy / 2.0f);
			for (int j = 0; j < ny; j++)
			{
				d = std::sqrt(x * x + y * y + z * z);
				Vec3 pt = { x, y, z };
				bool inside = InsideTetrahedron(pt);
				perc = 100 * (d / radius);

				if (inside)
				{
					int zoneIndexX = (int)((x + addX) / m_gs.zoneStep.x);
					int zoneIndexY = (int)((y + addY) / m_gs.zoneStep.y);
					int zoneIndexZ = (int)((z + addZ) / m_gs.zoneStep.z);

					//To avoid round off errors with the zone indexing
					if (zoneIndexX >= m_gs.zoneCount.x) zoneIndexX = m_gs.zoneCount.x - 1;
					if (zoneIndexY >= m_gs.zoneCount.y) zoneIndexY = m_gs.zoneCount.y - 1;
					if (zoneIndexZ >= m_gs.zoneCount.z)	zoneIndexZ = m_gs.zoneCount.z - 1;

					int zoneIndex = zoneIndexX + zoneIndexY * m_gs.zoneCount.x + zoneIndexZ * m_gs.zoneCount.x * m_gs.zoneCount.y;

					Vertex v;
					v.Position = { x, y, z };
					Vec4 color;

					color = GetBlendedColor(perc);

					v.Color = coolor; //color;
					m_vertices[pCounter] = v;
					m_pzTopFlat[pCounter] = zoneIndex;

					m_zpTop[zoneIndex].push_back(pCounter);
					m_zpCounter++;

					pCounter++;
				}

				y += dy;
			}//y-loop

			x += dx;
		}//x-loop

		z += dz;
	}//z-loop

	if (pCounter > m_nParticles)
		std::cout << "!WARNING! particle count larger then allocation size" << std::endl;

	m_nParticles = pCounter;

	//Generate particle indices
	for (int i = 0; i < m_nParticles; i++)
		m_Indices[i] = i;

	std::cout << "Create regular tetra contained particles completed" << std::endl;

	std::cout << "Number of particles created: " << m_nParticles << std::endl;
}

void StructuralComponent::CalcTargetSize()
{
	float pi = 3.1415;
	float rL, rS, volLargeSphere, volSmallSphere;
	float area = 0.0f;

	if (m_importType == ImportType::Femur2D || m_importType == ImportType::Frame2D)
	{
		if(m_importType == ImportType::Femur2D)
		{
			area = 0.00434792423;
		}
		else if (m_importType == ImportType::Frame2D)
		{
			area = 0.022;
		}
		
		float areaPerParticle = area / m_nParticles;
		float pRadius = std::sqrt(areaPerParticle / pi);
		float pVol = 4.0f / 3.0f * pi * pRadius * pRadius * pRadius;

		m_volTarget = pVol * m_nParticles;
	}
	else //3D Simulation
	{
		m_volTarget = CalcTotTetraVolume(m_tetras);
		std::cout << "Total volume from tetra calculation: " << m_volTarget << std::endl;
	}

	float volP = m_volTarget / m_nParticles;

	//Calc radius from particle volume
	m_massSizeEstimate = pow(((3.0f * volP) / (4.0f * pi)), (1.0f / 3.0f));

	//Set base size to twice that size. Used later for size calcs.
	m_baseSizeEstimate = 2.0f * m_massSizeEstimate;


	for (int i = 0; i < m_nParticles; i++)
	{
		m_pBaseSize[i] = m_baseSizeEstimate;

		m_pMassSize[i] = m_massSizeEstimate;
	}

	std::cout << "Base size estimate: " << m_baseSizeEstimate << std::endl;
}

void StructuralComponent::CalcBaseSize(int nIterations, int nNeighbours)
{
	int counter = 0;

	while (counter < nIterations)
	{
		IncrementParticleSize1D(nNeighbours);
		counter++;
	}

	UpdateDiscSize();
	UpdateIcosaSize();

	std::cout << nIterations << " size iterations completed" << std::endl;
}

void StructuralComponent::CalcMassSize()
{
	float volTot_base = 0;
	float pi = 3.1415;
	float* pVol = new float[m_nParticles];

	for (int i = 0; i < m_nParticles; i++)
	{
		pVol[i] = (4.0f / 3.0f) * pi * m_pBaseSize[i] * m_pBaseSize[i] * m_pBaseSize[i];
		volTot_base += pVol[i];
	}

	float volumeSf = m_volTarget / volTot_base;
	std::cout << "Target Volume: " << m_volTarget << std::endl;
	std::cout << "Volume from base size: " << volTot_base << std::endl;
	float volTot_mass = 0;

	for (int i = 0; i < m_nParticles; i++)
	{
		float radiusSf = pow(volumeSf, (1.0f / 3.0f));
		m_pMassSize[i] = radiusSf * m_pBaseSize[i];
		float volP = (4.0f / 3.0f) * pi * m_pMassSize[i] * m_pMassSize[i] * m_pMassSize[i];
		volTot_mass += volP;
	}

	std::cout << "Volume from mass size: " << volTot_mass << std::endl;

	std::cout << "Volume from mass size over target volume: " << volTot_mass / m_volTarget << std::endl;

	delete[] pVol;
}

void StructuralComponent::CalcHaSize(float alpha)
{
	for (int i = 0; i < m_nParticles; i++)
	{
		m_pHaSize[i] = alpha * m_pMassSize[i];
	}
}

void StructuralComponent::IncrementParticleSize()
{
	Vertex v, vNext;
	float d = 0;
	int vIndex, vNextIndex;
	m_pRequiredFriendsCount = 5;
	float averageSize = 0;

	//Loop over the particles
	for (int i = 0; i < m_nParticles; i++)
	{
		int currentZone = m_pzTopFlat[i];
		vIndex = i;
		v = m_vertices[vIndex];
		m_pCurrentFriendsCount[i] = 0;

		//Loop over neighbouring zones
		for (int j = 0; j < m_zzTop[currentZone].size(); j++)
		{
			int neighbZone = m_zzTop[currentZone][j];

			//Loop over particles in neighbouring zone
			for (int k = 0; k < m_zpTop[neighbZone].size(); k++)
			{
				vNextIndex = m_zpTop[neighbZone][k];
				vNext = m_vertices[vNextIndex];
				d = Distance(v, vNext);

				//Create a line
				if (d < m_pBaseSize[i] && vIndex != vNextIndex)
				{
					m_pCurrentFriendsCount[i]++;
				}
			}
		}
		m_pBaseSize[i] += 0.0001 * (m_pRequiredFriendsCount - m_pCurrentFriendsCount[i]);
		averageSize += m_pBaseSize[i] / m_nParticles;
	}
	std::cout << "Size increment done, with average particle size: " << averageSize << std::endl;
}

void StructuralComponent::IncrementParticleSize1D(int requiredFriendsCount)
{
	int currentFriendsCount = 0;
	float averageSize = 0;

	for (int i = 0; i < m_nParticles; i++)
	{
		int currentZone = m_pzTopFlat[i];
		int nNeighZones = m_zzRowLength[currentZone];
		int pIndexGlobal = i;

		Vertex p = m_vertices[i];
		currentFriendsCount = 0;

		float baseSize = m_pBaseSize[pIndexGlobal];

		float haSquared = baseSize * baseSize;

		for (int neighZoneRowIndex = 0; neighZoneRowIndex < nNeighZones; neighZoneRowIndex++)
		{
			int zzFlatIndex = m_zzStartIndex[currentZone] + neighZoneRowIndex;
			int zIndexGlobal = m_zzTopFlat[zzFlatIndex];

			int nParticlesInNeighZone = m_zpRowLength[zIndexGlobal];

			//Loop over the particles in the neighbouring zone
			for (int pRowIndexInNeighZone = 0; pRowIndexInNeighZone < nParticlesInNeighZone; pRowIndexInNeighZone++)
			{
				int zpFlatIndexNZ = m_zpStartIndex[zIndexGlobal] + pRowIndexInNeighZone;
				int pIndexGlobalNZ = m_zpTopFlat[zpFlatIndexNZ];

				//Avoid checking the particle against itself
				if (pIndexGlobal != pIndexGlobalNZ)
				{
					Vertex pNext = m_vertices[pIndexGlobalNZ];

					float xDiff = pNext.Position.x - p.Position.x;
					float yDiff = pNext.Position.y - p.Position.y;
					float zDiff = pNext.Position.z - p.Position.z;

					//Square the distances to avoid square root computation
					float dSquared = (xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);

					if (dSquared < haSquared)
					{
						currentFriendsCount++;
					}
				}
			}
		}
		//Loop around all neighbouring zones/particles finished, so size can be updated.
		m_pBaseSize[pIndexGlobal] += 0.000005 * (requiredFriendsCount - currentFriendsCount);
		
		averageSize += m_pBaseSize[pIndexGlobal] / m_nParticles;
	}
	std::cout << "Average base size: " << averageSize << std::endl;
}

void StructuralComponent::ShakeParticles(int rndSeed)
{
	float averageSize = 0;
	bool tooClose, outside;
	int attemptsCounter = 0;
	int shakeCounter = 0;
	int tooCloseCounter = 0;
	float rnd;
	float overlapSF = 0.75; //0.75; //For 2D in the article = 0.35;  // = 0.75;

	srand(rndSeed);

	for (int i = 0; i < m_nParticles; i++)
	{
		int currentZone = m_pzTopFlat[i];
		int nNeighZones = m_zzRowLength[currentZone];
		int pIndexGlobal = i;

		Vertex p = m_vertices[i];

		float massSize = m_pMassSize[pIndexGlobal];

		float sf = 0.2 * massSize;

		Vec3 rndVec = { GenRandom(-1, 1), GenRandom(-1,1), GenRandom(-1,1) };
		rndVec = UnitizeVector(rndVec);

		float newX = p.Position.x + sf * rndVec.x;
		float newY = p.Position.y + sf * rndVec.y;
		float newZ = p.Position.z + sf * rndVec.z;

		double r;

		 if (m_importType == ImportType::Femur2D || m_importType == ImportType::Frame2D)
		{
			r = sqrt(newX * newX + newY * newY);
			newZ = p.Position.z;
		}
		else
			r = sqrt(newX * newX + newY * newY + newZ * newZ);

		tooClose = false;
		outside = false;

		//Loop over neighbouring zones
		for (int neighZoneRowIndex = 0; neighZoneRowIndex < nNeighZones; neighZoneRowIndex++)
		{
			int zzFlatIndex = m_zzStartIndex[currentZone] + neighZoneRowIndex;
			int zIndexGlobal = m_zzTopFlat[zzFlatIndex];

			int nParticlesInNeighZone = m_zpRowLength[zIndexGlobal];

			//Loop over the particles in the neighbouring zone
			for (int pRowIndexInNeighZone = 0; pRowIndexInNeighZone < nParticlesInNeighZone; pRowIndexInNeighZone++)
			{
				int zpFlatIndexNZ = m_zpStartIndex[zIndexGlobal] + pRowIndexInNeighZone;
				int pIndexGlobalNZ = m_zpTopFlat[zpFlatIndexNZ];

				//Avoid checking the particle against itself
				if (pIndexGlobal != pIndexGlobalNZ)
				{
					Vertex pNext = m_vertices[pIndexGlobalNZ];
					float massSizeNext = m_pMassSize[pIndexGlobalNZ];

					float xDiff = newX - pNext.Position.x;
					float yDiff = newY - pNext.Position.y;
					float zDiff = newZ - pNext.Position.z;

					float d = std::sqrt(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);

					float overlap = (massSize + massSizeNext) - d;

					float overlapLimit = overlapSF * massSize;

					if (overlap > overlapLimit) //Allowing a little overlap.
					{
						tooClose = true;
						break;
					}
				}
			}//End neigh particle loop

			if (tooClose)
			{
				tooCloseCounter++;
				break;
			}

		}//End zone loop

		if (!tooClose)
		{
			if (m_importType == ImportType::Femur2D || m_importType == ImportType::Frame2D)
			{
				if (!InsidePolygon(newX, newY))
					outside = true;
			}
			else //3D Simulation
			{
				if (!InsideTetrahedron({newX, newY, newZ}))
					outside = true;
			}
		}
		if (!tooClose && !outside)
		{
			m_vertices[i].Position.x = newX;
			m_vertices[i].Position.y = newY;
			m_vertices[i].Position.z = newZ;
			shakeCounter++;
		}

		attemptsCounter++;

	}//End particle loop

	//std::cout<< "shake/attempts: " << ((float)shakeCounter)/((float)attemptsCounter) << std::endl;
}

void StructuralComponent::ShakeParticlesIteration(int n)
{
	std::cout << "Random seed: " << m_gs.rndSeed << std::endl;
	int counter = 0;
	int tenCounter = 0;
	for (int i = 0; i < n; i++)
	{
		ShakeParticles(i + m_gs.rndSeed);
		counter++;

		if (counter % 10 == 0)
		{
			tenCounter++;
			counter = 0;
			std::cout << "Completed " << tenCounter * 10 << " nr iterations" << endl;
		}
	}
}



//Setup boundary conditions for each 
void StructuralComponent::UpdateBcImportFemur2D()
{
	Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	float d = 0.0f;
	float perc = 0.0f;
	float hLimit = (m_gs.bbSize.y / 2.0f) - m_gs.bcThickness;
	Vec4 color;

	for (int i = 0; i < m_nParticles; i++)
	{
		m_isLoaded[i] = true;
		m_isLocked[i] = false;

		m_loadDir[i] = { 0, 0, 0 };

		Vertex v = m_vertices[i];

		if (v.Position.y < (-1.0 * hLimit))
		{
			m_vertices[i].Color = loadZoneColor;
			m_isLocked[i] = true;
		}
		else if (v.Position.y > hLimit && v.Position.x > 0)
		{
			m_vertices[i].Color = loadZoneColor;
			m_loadDir[i] = { 0, -1, 0};
		}
		else
			m_isLoaded[i] = false;

	}
}

void StructuralComponent::UpdateBcImportFrame2D()
{
	Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	float d = 0.0f;
	float perc = 0.0f;
	float hLimit = (m_gs.bbSize.y / 2.0f) - m_gs.bcThickness;
	Vec4 color;

	for (int i = 0; i < m_nParticles; i++)
	{
		m_isLoaded[i] = true;
		m_isLocked[i] = false;
		m_loadDir[i] = { 0, 0, 0 };

		Vertex v = m_vertices[i];

		if (v.Position.y < (-1.0 * hLimit))
		{
			m_vertices[i].Color = loadZoneColor;
			m_isLocked[i] = true;
		}
		else if (v.Position.y > hLimit)
		{
			m_vertices[i].Color = loadZoneColor;
			m_loadDir[i] = { 0, -1, 0 };
		}
		else
			m_isLoaded[i] = false;

	}
}

void StructuralComponent::UpdateBcImportFemur3D()
{
	Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	float d = 0.0f;
	float perc = 0.0f;
	float hLimit = 0.054; //0.055;
	Vec4 color;

	for (int i = 0; i < m_nParticles; i++)
	{
		m_isLoaded[i] = true;
		m_isLocked[i] = false;
		m_isSlider[i] = false;
		m_loadDir[i] = { 0, 0, 0 };

		Vertex v = m_vertices[i];

		if (v.Position.z < (-1.0f * hLimit))
		{
			m_vertices[i].Color = loadZoneColor;
			m_isLocked[i] = true;
		}
		else if (v.Position.z > hLimit)
		{
			m_vertices[i].Color = loadZoneColor;
			m_loadDir[i] = { 0.0f, 0.0f, -1.0f };
		}
		else
			m_isLoaded[i] = false;

	}
}

void StructuralComponent::UpdateBcImportTwaColumn()
{
	Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	float d = 0.0f;
	float perc = 0.0f;
	float hLimit = 0.055;
	Vec4 color;

	for (int i = 0; i < m_nParticles; i++)
	{
		m_isLoaded[i] = true;
		m_isLocked[i] = false;
		m_isSlider[i] = false;
		m_loadDir[i] = { 0, 0, 0 };

		Vertex v = m_vertices[i];

		if (v.Position.z < (-1.0f * 0.055))
		{
			m_vertices[i].Color = loadZoneColor;
			m_isLocked[i] = true;
		}
		else if (v.Position.x > 0.05)
		{
			m_vertices[i].Color = loadZoneColor;
			m_loadDir[i] = { -0.707f, 0.0f, -0.707f };
		}
		else
			m_isLoaded[i] = false;

	}
}

void StructuralComponent::UpdateBcImportConcreteFrame()
{
	Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	float d = 0.0f;
	float perc = 0.0f;
	float hLimit = 0.055;
	Vec4 color;

	for (int i = 0; i < m_nParticles; i++)
	{
		m_isLoaded[i] = true;
		m_isLocked[i] = false;
		m_loadDir[i] = { 0, 0, 0 };

		Vertex v = m_vertices[i];

		if (v.Position.z < (-1.0f * 0.05))
		{
			m_vertices[i].Color = loadZoneColor;
			m_isLocked[i] = true;
		}
		else if (v.Position.z > 0.05)
		{
			m_vertices[i].Color = loadZoneColor;
			m_loadDir[i] = { 0.0f, 0.0f, -1.0f };
		}
		else
			m_isLoaded[i] = false;

	}
}

void StructuralComponent::UpdateBcImportGerberette()
{
	Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	Vec4 color;

	Vec3 supportCp = { -0.101458f, 0.0f, 0.00133462f };
	float supportR = 0.015;

	for (int i = 0; i < m_nParticles; i++)
	{
		m_isLoaded[i] = true;
		m_isLocked[i] = false;
		m_isSlider[i] = false;
		m_loadDir[i] = { 0, 0, 0 };

		Vertex v = m_vertices[i];

		float dx = v.Position.x - supportCp.x;
		float dy = v.Position.y - supportCp.y;
		float dz = v.Position.z - supportCp.z;

		float distToSupport = std::sqrt(dx * dx + dz * dz);

		if (distToSupport < supportR)
		{
			m_vertices[i].Color = loadZoneColor;
			m_isLocked[i] = true;
		}
		else if (v.Position.x > 0.158215 && v.Position.z > 0.022)
		{
			m_vertices[i].Color = loadZoneColor;
			m_loadDir[i] = { 0.0f, 0.0f, -1.0f };
		}
		else if (v.Position.x < -0.16161 && v.Position.z > 0)
		{
			m_vertices[i].Color = loadZoneColor;
			m_loadDir[i] = { 0.0f, 0.0f, -1.0f };
		}
		else
			m_isLoaded[i] = false;

	}
}

void StructuralComponent::UpdateBcImportTetraNode()
{
	Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	Vec4 color;

	float supportR = 0.015;
	float dir = -1.0f;

	for (int i = 0; i < m_nParticles; i++)
	{
		m_isLoaded[i] = true;
		m_isLocked[i] = false;
		
		Vertex v = m_vertices[i];

		float dx = v.Position.x;
		float dy = v.Position.y;
		float dz = v.Position.z;

		Vec3 dirVec = { dir * dx, dir * dy, dir * dz };

		dirVec = UnitizeVector(dirVec);
		
		m_loadDir[i] = dirVec;

		float distToCentre = std::sqrt(dx * dx + dy * dy + dz * dz);

		if (distToCentre > 0.06)
		{
			m_vertices[i].Color = loadZoneColor;
			m_isLoaded[i] = true;
		}
		else
			m_isLoaded[i] = false;
		
	}
}

void StructuralComponent::UpdateBcImportNodeRotSym(float rLimit)
{
	Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	Vec4 color;

	float dir = 1.0f;

	for (int i = 0; i < m_nParticles; i++)
	{
		m_isLoaded[i] = false;
		m_isLocked[i] = false;
		m_isSlider[i] = false;

		Vertex v = m_vertices[i];

		float dx = v.Position.x;
		float dy = v.Position.y;
		float dz = v.Position.z;


		Vec3 dirVec = { 0.0f,  dir * 1.0f, 0.0f };

		if (v.Position.y < 0)
			dirVec = { 0.0f,  dir * -1.0f, 0.0f };

		//Vec3 dirVec = { dir * dx, dir * dy, dir * dz };
		//dirVec = UnitizeVector(dirVec);

		m_loadDir[i] = dirVec;

		float distToCentre = std::sqrt(dx * dx + dy * dy + dz * dz);

		if (distToCentre > rLimit )
		{
			if (v.Position.x < 0.012 && v.Position.x > -0.012) 
			{
				m_isLoaded[i] = true;
			}
			else
				m_isLocked[i] = true;
		}
		
	}

}

void StructuralComponent::UpdateBcImportPipe(float rLimit)
{
	Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	Vec4 color;

	float dir = 1.0f;

	for (int i = 0; i < m_nParticles; i++)
	{
		m_isLoaded[i] = false;
		m_isLocked[i] = false;
		m_isSlider[i] = false;

		Vertex v = m_vertices[i];

		float dx = v.Position.x;
		float dy = v.Position.y;
		float dz = v.Position.z;

		Vec3 dirVec = { 0.0f,  dir * 1.0f, 0.0f};

		if(v.Position.y < 0)
			dirVec = { 0.0f,  dir * -1.0f, 0.0f};

		
		m_loadDir[i] = dirVec;

		float distToCentre = std::abs(dy);

		if (distToCentre > rLimit)
		{
			m_isLoaded[i] = true;
		}

	}

}


//Check point inclusion
bool StructuralComponent::InsidePolygon(float x, float y)
{
	// If we never cross any lines we're inside.
	bool inside = false;

	// Loop through all the edges.
	for (int i = 0; i < m_outline.size(); ++i)
	{
		// i is the index of the first vertex, ii is the next one.
		// The original code uses a too-clever trick for this.
		int ii = (i + 1) % m_outline.size();

		// The vertices of the edge we are checking.
		double xp0 = m_outline[i].x;
		double yp0 = m_outline[i].y;
		double xp1 = m_outline[ii].x;
		double yp1 = m_outline[ii].y;

		// Check whether the edge intersects a line from (-inf,y) to (x,y).

		// First check if the line crosses the horizontal line at y in either direction.
		if ((yp0 <= y) && (yp1 > y) || (yp1 <= y) && (yp0 > y))
		{
			// If so, get the point where it crosses that line. This is a simple solution
			// to a linear equation. Note that we can't get a division by zero here -
			// if yp1 == yp0 then the above if be false.
			double cross = (xp1 - xp0) * (y - yp0) / (yp1 - yp0) + xp0;

			// Finally check if it crosses to the left of our test point. You could equally
			// do right and it should give the same result.
			if (cross < x)
				inside = !inside;
		}
	}
	return inside;

}

bool StructuralComponent::InsideTetrahedron(Vec3 pt)
{
	int zoneIndex = GetZoneIndex(pt, m_tzs);
	vector<int> tetraIndices = m_ztTop[zoneIndex];

	//Check containment with all tetrahedrons in the same zone
	for (int j = 0; j < tetraIndices.size(); j++)
	{
		int tetraIndex = tetraIndices[j];

		Vec3 a = m_tetras[tetraIndex][0];
		Vec3 b = m_tetras[tetraIndex][1];
		Vec3 c = m_tetras[tetraIndex][2];
		Vec3 d = m_tetras[tetraIndex][3];

		Vec4 barycentricCoords = BarycentricCoord(a, b, c, d, pt);

		bool pInside = IsInsideTetra(barycentricCoords);

		if (pInside)
		{
			return true;
		}
	}

	return false;
}

//Zones
void StructuralComponent::CreateZones()
{
	float xMp, yMp, zMp;
	//Zero zone related counters
	m_zVertexCounter = 0;
	m_zLineIndexCounter = 0;
	m_zzCounter = 0;

	//Creating zone vertices and zone line indices for the zones. Allowing dup vertices positions for simplicity.
	zMp = (-1.0f * m_gs.bbSize.z / 2.0f) + (m_gs.zoneStep.z / 2.0f);
	for (int i = 0; i < m_gs.zoneCount.z; i++)
	{
		yMp = (-1.0f * m_gs.bbSize.y / 2.0f) + (m_gs.zoneStep.y / 2.0f);;
		for (int j = 0; j < m_gs.zoneCount.y; j++)
		{
			xMp = (-1.0f * m_gs.bbSize.x / 2.0f) + (m_gs.zoneStep.x / 2.0f);;
			for (int k = 0; k < m_gs.zoneCount.x; k++)
			{
				CreateZone(xMp, yMp, zMp);
				CreateZoneTopology(xMp, yMp, zMp);
				xMp += m_gs.zoneStep.x;
			}
			yMp += m_gs.zoneStep.y;
		}
		zMp += m_gs.zoneStep.z;
	}

	std::cout << "Create zones completed" << std::endl;
}

void StructuralComponent::CreateZone(float x, float y, float z)
{
	float xHalfStep = m_gs.zoneStep.x / 2.0f;
	float yHalfStep = m_gs.zoneStep.y / 2.0f;
	float zHalfStep = m_gs.zoneStep.z / 2.0f;

	Vec4 color = { 0.9f, 0.9f, 0.9f, 0.5f };
	Vertex v1, v2, v3, v4, v5, v6, v7, v8;
	v1.Position = { x - xHalfStep, y - yHalfStep, z - zHalfStep };
	v2.Position = { x + xHalfStep, y - yHalfStep, z - zHalfStep };
	v3.Position = { x + xHalfStep, y + yHalfStep, z - zHalfStep };
	v4.Position = { x - xHalfStep, y + yHalfStep, z - zHalfStep };

	v5.Position = { x - xHalfStep, y + yHalfStep, z + zHalfStep };
	v6.Position = { x - xHalfStep, y - yHalfStep, z + zHalfStep };
	v7.Position = { x + xHalfStep, y - yHalfStep, z + zHalfStep };
	v8.Position = { x + xHalfStep, y + yHalfStep, z + zHalfStep };

	vector<Vertex> zv = { v1,v2,v3,v4,v5,v6,v7,v8 };

	for (int i = 0; i < 8; i++)
	{
		zv[i].Color = color;
		m_zoneVertices1D[m_zVertexCounter] = zv[i];
		m_zVertexCounter++;

		m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 1;
		m_zLineIndexCounter++;

		if (i == 7)
			m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 4;
		else
			m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter;

		m_zLineIndexCounter++;
	}

	m_zoneVertices2D.push_back(zv);

	m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 8;
	m_zLineIndexCounter++;
	m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 8 + 5;
	m_zLineIndexCounter++;

	m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 7;
	m_zLineIndexCounter++;
	m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 7 + 5;
	m_zLineIndexCounter++;

	m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 6;
	m_zLineIndexCounter++;
	m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 6 + 5;
	m_zLineIndexCounter++;
}

void StructuralComponent::CreateZoneTopology(float xMp, float yMp, float zMp)
{
	//Move geometry to the positive quadrand by adding this values.
	float addX = m_gs.bbSize.x / 2.0f;
	float addY = m_gs.bbSize.y / 2.0f;
	float addZ = m_gs.bbSize.z / 2.0f;

	xMp += addX;
	yMp += addY;
	zMp += addZ;

	int zoneIndexX = (int)(xMp / m_gs.zoneStep.x);
	int zoneIndexY = (int)(yMp / m_gs.zoneStep.y);
	int zoneIndexZ = (int)(zMp / m_gs.zoneStep.z);

	int zoneIndex = zoneIndexX + zoneIndexY * m_gs.zoneCount.x + zoneIndexZ * m_gs.zoneCount.x * m_gs.zoneCount.y;

	vector<int> neighIndex;
	neighIndex.push_back(zoneIndex);	//Add self first!
	m_zzCounter++;						//And increment the counter.

	int nZoneIndexX, nZoneIndexY, nZoneIndexZ;

	for (int i = -1; i < 2; i++)
	{
		nZoneIndexX = zoneIndexX + i;

		for (int j = -1; j < 2; j++)
		{
			nZoneIndexY = zoneIndexY + j;

			for (int k = -1; k < 2; k++)
			{
				nZoneIndexZ = zoneIndexZ + k;

				if (nZoneIndexX >= 0 && nZoneIndexX < m_gs.zoneCount.x &&
					nZoneIndexY >= 0 && nZoneIndexY < m_gs.zoneCount.y &&
					nZoneIndexZ >= 0 && nZoneIndexZ < m_gs.zoneCount.z)
				{
					int nZoneIndex = nZoneIndexX + nZoneIndexY * m_gs.zoneCount.x + nZoneIndexZ * m_gs.zoneCount.x * m_gs.zoneCount.y;

					if (nZoneIndex < m_nZones && nZoneIndex != zoneIndex)
					{
						neighIndex.push_back(nZoneIndex);
						m_zzCounter++;
					}
				}
			}
		}
	}

	m_zzTop.push_back(neighIndex);
}

//Create arms by connecting particels that are within the horizon distance of each other.
void StructuralComponent::CreateArms()
{
	Vertex v, vNext;
	float d = 0;
	int vIndex, vNextIndex;
	int pCount = 0;
	int addCount = 0;
	float xDiff, yDiff, zDiff;
	float bondExists;
	int existingNextParticleIndex;
	int existingArmFlatIndex;
	m_armIndexCounter = 0;
	m_armSubsetIndexCounter = 0;
	m_armCounter = 0;
	m_ppCounter = 0;
	m_paCounter = 0;
	float horizonSquared = 0;//m_Horizon * m_Horizon;
	float totArmLength = 0.0f;

	//Initialise vectors
	for (int i = 0; i < m_nParticles; i++)
	{
		vector<int> ppi;
		m_ppTop.push_back(ppi);
		vector<int> pai;
		m_paTop.push_back(pai);
	}

	//Loop over particles
	for (int i = 0; i < m_nParticles; i++)
	{
		int currentZone = m_pzTopFlat[i];
		vIndex = i;
		v = m_vertices[vIndex];

		float horizon = m_pMassSize[i] * m_alpha;

		//Loop over neighbouring zones
		for (int j = 0; j < m_zzTop[currentZone].size(); j++)
		{
			int neighbZone = m_zzTop[currentZone][j];

			//Loop over particles in neighbouring zone
			for (int k = 0; k < m_zpTop[neighbZone].size(); k++)
			{
				vNextIndex = m_zpTop[neighbZone][k];
				vNext = m_vertices[vNextIndex];

				xDiff = vNext.Position.x - v.Position.x;
				yDiff = vNext.Position.y - v.Position.y;
				zDiff = vNext.Position.z - v.Position.z;

				float d = sqrt(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);

				//Create a line
				if (d < horizon && vIndex != vNextIndex)
				{
					existingNextParticleIndex = FindNextParticleIndex(vIndex, vNextIndex);
					existingArmFlatIndex = FindExistingArmFlatIndex(vIndex, vNextIndex);

					if(existingArmFlatIndex == -1) //No existing arm found
					{
						Vertex v1Copy, v2Copy;
						v1Copy.Position = v.Position;
						v1Copy.Color = v.Color;
						v2Copy.Color = v.Color;
						v2Copy.Position = vNext.Position;

						ArmIndices ai = { vIndex, vNextIndex, m_armCounter};
						m_paTop[vIndex].push_back(m_armCounter);
						m_armi[m_armCounter] = ai;

						m_ppTop[vIndex].push_back(vNextIndex);

						m_arms[m_armIndexCounter] = vIndex;
						m_arms2[m_armIndexCounter] = m_armIndexCounter;
						m_vertices2[m_armIndexCounter] = v1Copy;
						m_vertices3[m_armSubsetIndexCounter] = v1Copy;

						m_armIndexCounter++;
						m_armSubsetIndexCounter++;

						m_arms[m_armIndexCounter] = vNextIndex;
						m_arms2[m_armIndexCounter] = m_armIndexCounter;
						m_vertices2[m_armIndexCounter] = v2Copy;
						m_vertices3[m_armSubsetIndexCounter] = v2Copy;

						m_armIndexCounter++;
						m_armSubsetIndexCounter++;

						m_armCounter++;
						m_ppCounter++;
						m_paCounter++;

						totArmLength += d;
					}
					else
					{
						m_paTop[vIndex].push_back(existingArmFlatIndex);
						m_ppTop[vIndex].push_back(existingNextParticleIndex);
						m_ppCounter++;
						m_paCounter++;
					}
				}
			}
		}

		m_avrgArmLength = totArmLength / (float)m_armCounter;
	}

	std::cout << "Create arms function completed. Average arm length = " << m_avrgArmLength << "." << std::endl;
}

bool StructuralComponent::DoesBondExist(int vIndex, int vNextIndex)
{
	for (int i = 0; i < m_ppTop[vNextIndex].size(); i++)
	{
		if (m_ppTop[vNextIndex][i] == vIndex)
			return true;
	}
	return false;
}

int StructuralComponent::FindNextParticleIndex(int vIndex, int vNextIndex)
{
	for (int i = 0; i < m_ppTop[vNextIndex].size(); i++)
	{
		if (m_ppTop[vNextIndex][i] == vIndex)
			return m_ppTop[vNextIndex][i];
	}
	return -1;
}

int StructuralComponent::FindExistingArmFlatIndex(int vIndex, int vNextIndex)
{
	//Loop through arms connected to the first index
	for (int i = 0; i < m_paTop[vIndex].size(); i++)
	{
		int armIndex = m_paTop[vIndex][i];

		if (m_armi[armIndex].p1 == vIndex && m_armi[armIndex].p2 == vNextIndex || m_armi[armIndex].p1 == vNextIndex && m_armi[armIndex].p2 == vIndex)
			return armIndex;
	}
	//Loop through arms connected to the second index
	for (int i = 0; i < m_paTop[vNextIndex].size(); i++)
	{
		int armIndex = m_paTop[vNextIndex][i];

		if (m_armi[armIndex].p1 == vIndex && m_armi[armIndex].p2 == vNextIndex || m_armi[armIndex].p1 == vNextIndex && m_armi[armIndex].p2 == vIndex)
			return armIndex;
	}

	//No existing arm found
	return -1;
}

void StructuralComponent::GenFlatTopology1()
{
	//Cuda initialisation functions 
	m_zzTopFlat = new int[m_zzCounter];
	m_zzStartIndex = new int[m_nZones];
	m_zzRowLength = new int[m_nZones];

	m_zpTopFlat = new int[m_nParticles];
	m_zpStartIndex = new int[m_nZones];
	m_zpRowLength = new int[m_nZones];

	int counter = 0;
	int rowLength;

	//Create 1D version of the zone-particle topology 2D array 
	for (int i = 0; i < m_zpTop.size(); i++)
	{
		rowLength = 0;
		for (int j = 0; j < m_zpTop[i].size(); j++)
		{
			if (j == 0)
			{
				m_zpStartIndex[i] = counter;
			}
			m_zpTopFlat[counter] = m_zpTop[i][j];
			m_zpTopFlatV.push_back(m_zpTop[i][j]);
			counter++;
			rowLength++;
		}
		m_zpRowLength[i] = rowLength;
	}

	int counter2 = 0;
	int rowLength2;

	//Create 1D version of the zone-zone topology 2D array 
	for (int i = 0; i < m_zzTop.size(); i++)
	{
		rowLength2 = 0;
		for (int j = 0; j < m_zzTop[i].size(); j++)
		{
			if (j == 0)
			{
				m_zzStartIndex[i] = counter2;
			}
			m_zzTopFlat[counter2] = m_zzTop[i][j];
			m_zzTopFlatV.push_back(m_zzTop[i][j]);
			counter2++;
			rowLength2++;
		}
		m_zzRowLength[i] = rowLength2;
	}

	std::cout << "Flat topology 1 completed" << std::endl;
}

void StructuralComponent::GenFlatTopology2()
{
	m_ppTopFlat = new int[m_ppCounter];
	m_ppStartIndex = new int[m_nParticles];
	m_ppRowLength = new int[m_nParticles];

	int counter = 0;
	int rowLength;

	//Create 1D version of the particle-particle topology 2D array.
	for (int i = 0; i < m_ppTop.size(); i++)
	{
		rowLength = 0;
		for (int j = 0; j < m_ppTop[i].size(); j++)
		{
			if (j == 0)
			{
				m_ppStartIndex[i] = counter;
			}
			m_ppTopFlat[counter] = m_ppTop[i][j];
			counter++;
			rowLength++;
		}
		m_ppRowLength[i] = rowLength;
	}


	m_paTopFlat = new int[m_paCounter];
	m_paStartIndex = new int[m_nParticles];
	m_paRowLength = new int[m_nParticles];

	counter = 0;
	rowLength = 0;

	//Create 1D version of the particle-arm topology 2D array
	for (int i = 0; i < m_paTop.size(); i++)
	{
		rowLength = 0;
		for (int j = 0; j < m_paTop[i].size(); j++)
		{
			if (j == 0)
			{
				m_paStartIndex[i] = counter;
			}
			m_paTopFlat[counter] = m_paTop[i][j];
			counter++;
			rowLength++;
		}
		m_paRowLength[i] = rowLength;
	}

	std::cout << "Flat topology 2 completed" << std::endl;
}

void StructuralComponent::SelectArms() 
{	
	float maxShortening = -1e30;
	float minShortening = 1e30;
	float shortening = 0;

	//Find max and min strain and deselect all from previous selection
	for (int i = 0; i < m_structure.m_arms.size(); i++)
	{
		m_structure.m_arms[i].selected = false;

		float iL = m_structure.m_arms[i].iLength;
		float cL = m_structure.m_arms[i].cLength;

		if (cL < iL)
			shortening = iL - cL;

		if (shortening > maxShortening)
			maxShortening = shortening;

		if (shortening < minShortening)
			minShortening = shortening;
	}

	float selectionPercentage = 0.5;

	for (int i = 0; i < m_structure.m_arms.size(); i++)
	{
		float iL = m_structure.m_arms[i].iLength;
		float cL = m_structure.m_arms[i].cLength;

		if (cL < iL)
			shortening = iL - cL;

		if (shortening > maxShortening * 0.1)
		{
			m_structure.m_arms[i].selected = true;
		}
	}
}

void StructuralComponent::SelectArms2()
{
	float limit = 0.0f;

	for (int i = 0; i < m_structure.m_arms.size(); i++)
	{
		
		if(m_colorArmBy == colorArmBy::tension)
		{
			if(m_structure.m_arms[i].fMag > 0 )
				m_structure.m_arms[i].selected = true;
			else
				m_structure.m_arms[i].selected = false;

		}
		else if (m_colorArmBy == colorArmBy::compression)
		{
			if (m_structure.m_arms[i].fMag < 0)
				m_structure.m_arms[i].selected = true;
			else
				m_structure.m_arms[i].selected = false;
		}
		else if (m_colorArmBy == colorArmBy::stiffnessSF)
		{
			limit = m_structure.m_sfMax * m_selectionCap;

			if (m_structure.m_arms[i].sf >  limit) //1.0f)
				m_structure.m_arms[i].selected = true;
			else
				m_structure.m_arms[i].selected = false;
		}
		else
		{
			m_structure.m_arms[i].selected = true;
		}
	}

	//if(m_colorArmBy == colorArmBy::stiffnessSF)
	//	std::cout << "Limit: " << limit << std::endl;


}

//Visualisation and UI
void StructuralComponent::CopyPositions()
{

	float ix, iy, iz, dx, dy, dz;
	float sf = m_scaleFactor;

	if (m_drawParticles || m_drawBonds)
	{
		for (int i = 0; i < m_nParticles; i++)
		{
			ix = m_structure.m_particles[i].iPosition.x;
			iy = m_structure.m_particles[i].iPosition.y;
			iz = m_structure.m_particles[i].iPosition.z;

			dx = m_structure.m_particles[i].cPosition.x - ix;
			dy = m_structure.m_particles[i].cPosition.y - iy;
			dz = m_structure.m_particles[i].cPosition.z - iz;

			//Scaling the position
			m_vertices[i].Position.x = ix + sf * dx;
			m_vertices[i].Position.y = iy + sf * dy;
			m_vertices[i].Position.z = iz + sf * dz;
		}
	}

	if (m_drawBonds2)
	{
		int armIndexCounter = 0;
		int armSubsetIndexCounter = 0;

		for (int i = 0; i < m_structure.m_arms.size(); i++)
		{
			int i1 = m_structure.m_arms[i].p1;
			int i2 = m_structure.m_arms[i].p2;

			ix = m_structure.m_particles[i1].iPosition.x;
			iy = m_structure.m_particles[i1].iPosition.y;
			iz = m_structure.m_particles[i1].iPosition.z;

			dx = m_structure.m_particles[i1].cPosition.x - ix;
			dy = m_structure.m_particles[i1].cPosition.y - iy;
			dz = m_structure.m_particles[i1].cPosition.z - iz;

			//Scaling the position
			m_vertices2[armIndexCounter].Position.x = ix + sf * dx;
			m_vertices2[armIndexCounter].Position.y = iy + sf * dy;
			m_vertices2[armIndexCounter].Position.z = iz + sf * dz;

			armIndexCounter++;

			bool selection = false;
			//Testing subset of lines
			if (m_structure.m_arms[i].selected)
			{
				m_vertices3[armSubsetIndexCounter].Position.x = ix + sf * dx;
				m_vertices3[armSubsetIndexCounter].Position.y = iy + sf * dy;
				m_vertices3[armSubsetIndexCounter].Position.z = iz + sf * dz;
				armSubsetIndexCounter++;
			}

			ix = m_structure.m_particles[i2].iPosition.x;
			iy = m_structure.m_particles[i2].iPosition.y;
			iz = m_structure.m_particles[i2].iPosition.z;

			dx = m_structure.m_particles[i2].cPosition.x - ix;
			dy = m_structure.m_particles[i2].cPosition.y - iy;
			dz = m_structure.m_particles[i2].cPosition.z - iz;

			//Scaling the position
			m_vertices2[armIndexCounter].Position.x = ix + sf * dx;
			m_vertices2[armIndexCounter].Position.y = iy + sf * dy;
			m_vertices2[armIndexCounter].Position.z = iz + sf * dz;

			armIndexCounter++;

			//Save subset of indices
			if (m_structure.m_arms[i].selected)
			{
				m_vertices3[armSubsetIndexCounter].Position.x = ix + sf * dx;
				m_vertices3[armSubsetIndexCounter].Position.y = iy + sf * dy;
				m_vertices3[armSubsetIndexCounter].Position.z = iz + sf * dz;
				armSubsetIndexCounter++;
			}
		}

		m_armSubsetIndexCounter = armSubsetIndexCounter;
	}

}

void StructuralComponent::ColorParticles()
{

	float min = 1e20, max = -1e20;
	float* values = new float[m_nParticles];

	for (int i = 0; i < m_nParticles; i++)
	{
		float value = 0.0f;

		if (m_colorParticleBy == colorParticleBy::velocity)
			value = GetVectorLength(m_structure.m_particles[i].Veloc);
		else if (m_colorParticleBy == colorParticleBy::acceleration)
			value = GetVectorLength(m_structure.m_particles[i].Accel);
		else if (m_colorParticleBy == colorParticleBy::forceArm)
			value = GetVectorLength(m_structure.m_particles[i].ArmForce);
		else if (m_colorParticleBy == colorParticleBy::forceExt)
			value = GetVectorLength(m_structure.m_particles[i].ExtForce);
		else if (m_colorParticleBy == colorParticleBy::meanStrain)
			value = m_structure.m_particles[i].meanStrain;
		else if (m_colorParticleBy == colorParticleBy::forceTot)
			value = GetAddedVectorLength(m_structure.m_particles[i].ArmForce, m_structure.m_particles[i].ExtForce);
		else if (m_colorParticleBy == colorParticleBy::displacement)
			value = Distance(m_structure.m_particles[i].cPosition, m_structure.m_particles[i].iPosition);
		else if (m_colorParticleBy == colorParticleBy::damage)
			value = m_structure.m_particles[i].damage;
		else if (m_colorParticleBy == colorParticleBy::isBC)
			value = (float)m_structure.m_particles[i].isBC;
		else if (m_colorParticleBy == colorParticleBy::life)
			value = (float)m_structure.m_particles[i].life;
		else if (m_colorParticleBy == colorParticleBy::strainSquared)
			value = m_structure.m_particles[i].strainSquared;
		else if (m_colorParticleBy == colorParticleBy::strain)
			value = m_structure.m_particles[i].strain;


		if (value <= min)
			min = value;

		if (value >= max)
			max = value;

		if (m_colorParticleBy == colorParticleBy::damage) 
		{
			min = 0.0f;
			max = 0.5f;
		}

		values[i] = value;
	}

	float perc;

	for (int i = 0; i < m_nParticles; i++)
	{
		if (m_colorParticleBy == colorParticleBy::strainSquared || m_colorParticleBy == colorParticleBy::strain) 
		{
			Vec4 color = GetBlendedColorCap(min, max, m_colorCap, values[i]);
			m_vertices[i].Color = color;
		}
		else if (m_colorParticleBy == colorParticleBy::meanStrain)
		{
			Vec4 color = GetBlendedColorCap(min, max, m_colorCap, values[i]);
			m_vertices[i].Color = color;
		}
		else if (m_colorParticleBy == colorParticleBy::isBC)
		{
			Vec4 color = { 0.0, 0.0, 1.0, 1.0 };
			if (values[i] == 1.0f)
				color = { 1.0, 0.0, 0.0, 1.0 };
			 
			m_vertices[i].Color = color;
		}
		else
		{
			Vec4 color = GetBlendedColor(min, max, values[i]);
			m_vertices[i].Color = color; // color;
		}
	}

	delete[] values;
}

void StructuralComponent::ColorArms()
{

	float min = 1e20, max = -1e20, colorMax;
	float value = 0.0f;
	float* values = new float[m_structure.m_arms.size()];

	for (int i = 0; i < m_structure.m_arms.size(); i++)
	{
		if (m_colorArmBy == colorArmBy::force || m_colorArmBy == colorArmBy::forceComTen || m_colorArmBy == colorArmBy::tension || m_colorArmBy == colorArmBy::compression)
			value = m_structure.m_arms[i].fMag;
		else if (m_colorArmBy == colorArmBy::strain)
			value = m_structure.m_arms[i].strain;
		else if (m_colorArmBy == colorArmBy::plasticStrain)
			value = m_structure.m_arms[i].plasticStrain;
		else if (m_colorArmBy == colorArmBy::broken)
			value = (float)m_structure.m_arms[i].broken;
		else if (m_colorArmBy == colorArmBy::elongationToFracture)
			value = (float)m_structure.m_arms[i].elongation;
		else if (m_colorArmBy == colorArmBy::stiffnessSF)
			value = (float)m_structure.m_arms[i].sf;
		else if (m_colorArmBy == colorArmBy::anisotropy)
			value = (float)m_structure.m_arms[i].dir_sf;
		
		if (value < min)
			min = value;

		if (value > max)
			max = value;

		values[i] = value;
	}

	//std::cout<<"Min: " << min << " Max: " << max << std::endl;

	//Limits for strain
	if (m_colorArmBy == colorArmBy::plasticStrain || m_colorArmBy == colorArmBy::strain)
	{
		min = -m_structure.m_material.yieldStrain;
		max = m_structure.m_material.yieldStrain;
	}
	else if (m_colorArmBy == colorArmBy::elongationToFracture)
	{
		min = 0;
		max = m_structure.m_material.elongationLimit;
	}

	int armIndexCounter = 0;
	int armSubsetIndexCounter = 0;

	for (int i = 0; i < m_structure.m_arms.size(); i++)
	{
		float num = values[i];
		Vec4 color;
		if (m_colorArmBy == colorArmBy::forceComTen)
		{
			color = GetBlendedColorCapedBlueBlackRed(min, m_colorCap, max, m_colorCap, num); 
		}
		else if (m_colorArmBy == colorArmBy::stiffnessSF)
		{
			color = GetBlendedColorCapedBlackWhite(min, m_colorCap, max, m_colorCap, num);
		}
		else if (m_colorArmBy == colorArmBy::anisotropy)
		{
			color = GetBlendedColor(min, max, num);
		}
		else if (m_colorArmBy == colorArmBy::tension) 
		{
			color = GetBlendedColorCapedBlueBlackRed(min, m_colorCap, max, m_colorCap, num);
		}
		else if (m_colorArmBy == colorArmBy::compression)
		{
			color = GetBlendedColorCapedBlueBlackRed(min, m_colorCap, max, m_colorCap, num);
		}
		else if (m_colorArmBy == colorArmBy::strain)
		{
			color = GetBlendedColorGreenZero(min, max, num);
		}
		else if (m_colorArmBy == colorArmBy::broken)
		{
			if (m_structure.m_arms[i].broken)
				color = { 1.0f, 1.0f, 1.0f, 1.0f };
			else
				color = { 1.0f, 1.0f, 1.0f, 0.0f };
		}
		else if (m_colorArmBy == colorArmBy::elongationToFracture)
		{
			if (num < 0.0f)
				num = 0.0f;		//Ignore compressed elements

			color = GetBlendedColor(min, max, num);
		}
		else
			color = GetBlendedColor(min, max, num);

		m_vertices2[armIndexCounter].Color = color;
		armIndexCounter++;
		m_vertices2[armIndexCounter].Color = color;
		armIndexCounter++;

		if (m_structure.m_arms[i].selected)
		{
			m_vertices3[armSubsetIndexCounter].Color = color;
			armSubsetIndexCounter++;
			m_vertices3[armSubsetIndexCounter].Color = color;
			armSubsetIndexCounter++;
		}
	}

	delete[] values;

}

void StructuralComponent::GenDiscs()
{
	int counter = 0;
	int nextIndex = 0;
	int vertexIndexCounter = 0;
	float smallestHa = 1e10;
	float largestHa = -1e10;

	for (int i = 0; i < m_nParticles; i++)
	{
		if (m_pMassSize[i] > largestHa)
			largestHa = m_pMassSize[i];

		if (m_pMassSize[i] < smallestHa)
			smallestHa = m_pMassSize[i];
	}

	float haMaxForColor = largestHa - smallestHa;

	for (int i = 0; i < m_nParticles; i++)
	{
		float haForColor = m_pMassSize[i] - smallestHa; //Make sure the size goes from 0 -> upwards for percentage calc.
		float perc = 100 * (haForColor / haMaxForColor);

		Vec4 color = GetBlendedColor(perc);

		Vertex v = m_vertices[i];
		vector<Vertex> discVertices = CreateDisc(v.Position.x, v.Position.y, v.Position.z, m_pMassSize[i], m_nDiscSides, color);
		vector<int> discIndices = GetDiscIndices(nextIndex, m_nDiscSides);

		nextIndex += m_nDiscSides + 1;

		//Copy vertices to global array
		for (int j = 0; j < discVertices.size(); j++)
		{
			m_discVertices[counter] = discVertices[j];
			counter++;
		}

		//Copy indices to global array
		for (int j = 0; j < discIndices.size(); j++)
		{
			m_discIndices[vertexIndexCounter] = discIndices[j];
			vertexIndexCounter++;
		}
	}
}

void StructuralComponent::GenIcosas()
{
	int counter = 0;
	int nextIndex = 0;
	int vertexIndexCounter = 0;

	for (int i = 0; i < m_nParticles; i++)
	{
		Vertex v = m_vertices[i];
		vector<Vertex> icosaVertices = CreateIcoSphere(v.Position.x, v.Position.y, v.Position.z, m_pMassSize[i], m_vertices[i].Color);
		vector<int> icosaIndices = GetIcoSphereIndices(nextIndex);

		nextIndex += 12;

		Vertex vCenter;
		vCenter.Position = m_vertices[i].Position;
		vCenter.Color = m_vertices[i].Color;
		m_icosaCenter[i] = vCenter;

		//Copy vertices to global array
		for (int j = 0; j < icosaVertices.size(); j++)
		{
			m_icosaVertices[counter] = icosaVertices[j];
			counter++;
		}

		//Copy indices to global array
		for (int j = 0; j < icosaIndices.size(); j++)
		{
			m_icosaIndices[vertexIndexCounter] = icosaIndices[j];
			vertexIndexCounter++;
		}
	}

}

//Simulation run
void StructuralComponent::RunAnalysis()
{	
	if (m_runAnalysis)
	{
		if (m_simType == SimulationType::Bone) 
		{
			m_structure.RunAnalysis_BoneRemodelling();
		}
		else if (m_simType == SimulationType::SteelComponent) 
		{
			m_structure.RunAnalysis_SteelComponent();
		}

		m_runCounter++;

		//if(m_runCounter > 300 && m_optimise)
			//m_structure.ScaleStiffness();

		//Get last saved strain value
		//m_strain = m_structure.m_incGaugeStrain.back();

		//m_stress = m_structure.m_incStress.back();

		if (m_runCounter % 100 == 0)
		{
			m_tenCounter++;
			//float percentageComplete = 100 * (m_strain / m_strainLimit);
			//std::cout << "Completed " << m_tenCounter * 100 << " analysis iterations. Resulting strain: " << m_strain << ". Resulting stress : " << m_stress /1000000.0f <<" MPa. Percentage completed: " << percentageComplete << " Time elapsed: " << m_min << ":" << m_sec << endl;
		}
	}
}

void StructuralComponent::RunShaking()
{
	if (m_runShaking)
	{
		ShakeParticles(m_shakeCounter);

		m_shakeCounter++;
		std::cout << "Shaking iteration:" << m_shakeCounter << " done" << std::endl;

		if (m_shakeCounter == 100)
		{
			m_runShaking = false;
			m_shakeCounter = 0;
		}
	}
}

void StructuralComponent::RunOutput()
{
	if (m_btnRunOutput)
	{
		m_structure.OutputResults();
	}
}

void StructuralComponent::BtnNonLinear()
{
	if (m_btnScaleStiffness)
	{	
		m_scaleStiffness = !m_scaleStiffness;
		std::cout<< "Btn Non linear pressed. Value passed to structure:" << m_scaleStiffness << std::endl;
		m_structure.SetAnalysisType(m_scaleStiffness);
	}
}


void StructuralComponent::Output() 
{
	m_structure.OutputResults();
}

void StructuralComponent::SaveResults()
{
	m_structure.SaveResults();
}

void StructuralComponent::RunExport()
{
	if (m_btnRunExport)
	{
		ExportModel();
	}
}

void StructuralComponent::RunXray()
{
	if (m_XrayYZ)
	{
		float step = m_gs.bbSize.y / 100;

		if (m_clipDistance1 < - m_gs.bbSize.y / 2.0f)
			m_XrayDir = 1;
		else if (m_clipDistance1 > m_gs.bbSize.y / 2.0f)
			m_XrayDir = -1;

		m_clipDistance1 += m_XrayDir * step;
	}

	if (m_XrayXY)
	{
		float step = m_gs.bbSize.z / 250;

		if (m_clipDistance3 < -m_gs.bbSize.z / 2.0f)
			m_XrayDir2 = 1;
		else if (m_clipDistance3 > m_gs.bbSize.z / 2.0f)
			m_XrayDir2 = -1;

		m_clipDistance3 += m_XrayDir2 * step;
	}
}

void StructuralComponent::RunDefAni()
{
	if (m_AniDeformation)
	{
		float step = 100.0f / 75.0f;

		if (m_scaleFactor < 0.0f)
			m_AniDefDir = 1;
		else if (m_scaleFactor > 100.0f)
			m_AniDefDir = -1;

		m_scaleFactor += m_AniDefDir * step;
	}
}

float StructuralComponent::GetStrain() 
{
	return m_strain;
}

float StructuralComponent::GetStress()
{
	return m_stress;
}

void StructuralComponent::ComputeStats(MaterialSettings ms)
{
	int pCount = 0;

	for (int i = 0; i < m_nZones; i++)
	{
		if (m_zpTop[i].size() > 0)
		{
			m_stats.nUsedZones++;
			pCount += m_zpTop[i].size();
		}
	}

	m_stats.nParticles = m_nParticles;
	m_stats.nParticlesPerZone = pCount / m_stats.nUsedZones;
	m_stats.nZones = m_nZones;

	pCount = 0;

	for (int i = 0; i < m_ppTop.size(); i++)
	{
		pCount += m_ppTop[i].size();
	}

	float largestHa = 0;
	for (int i = 0; i < m_nParticles; i++)
	{
		if (m_pHaSize[i] > largestHa)
			largestHa = m_pHaSize[i];
	}

	m_stats.nBonds = pCount;
	m_stats.nBondsPerParticle = pCount / m_ppTop.size();

	float smallestZoneDim = 1e10;
	if (m_gs.zoneStep.x < smallestZoneDim) smallestZoneDim = m_gs.zoneStep.x;
	if (m_gs.zoneStep.y < smallestZoneDim) smallestZoneDim = m_gs.zoneStep.y;
	if (m_gs.zoneStep.z < smallestZoneDim) smallestZoneDim = m_gs.zoneStep.z;

	m_stats.zoneSizeParticleSizeRatio = smallestZoneDim / largestHa;

	float elongationOverArmlength = ms.elongationLimit / m_avrgArmLength;

	m_stats.averageArmLength = m_avrgArmLength;

	cout << "particle count:" << m_stats.nParticles << endl;
	cout << "bond count:" << m_stats.nBonds << endl;
	cout << "zone count:" << m_stats.nZones << endl;
	cout << "used zone count:" << m_stats.nUsedZones << endl;
	cout << "average number of neighbours per particle:" << m_stats.nBondsPerParticle << endl;
	cout << "average number of particles per zone:" << m_stats.nParticlesPerZone << endl;
	cout << "zone size (x,y,z):" << m_gs.zoneStep.x << ", " << m_gs.zoneStep.y << ", " << m_gs.zoneStep.z << endl;
	cout << "zone over horizon (should be greater than 1!): " << m_stats.zoneSizeParticleSizeRatio << endl;
	cout << "elongation limit over average arm length = " << elongationOverArmlength << endl;

}


//----------------------------Analysing---------------------------//

void StructuralComponent::OnUpdateAnalysis()
{	
	RunShaking();

	RunAnalysis();
}

void StructuralComponent::OnUpdateTime() 
{
	//Time keeping
	m_nowTimeAnalysis = std::chrono::high_resolution_clock::now();
	m_durationAnalysis = m_nowTimeAnalysis - m_startTimeAnalysis;
	m_min = floor(m_durationAnalysis.count() / 60.0f);
	m_sec = (float)(((int)m_durationAnalysis.count()) % 60);
}


//----------------------------Drawing-----------------------------//

void StructuralComponent::BtnEvents() 
{

	if (m_btnRunAnalysis)
	{
		m_startTimeAnalysis = std::chrono::high_resolution_clock::now();
		m_runAnalysis = !m_runAnalysis;
	}

	if (m_btnRunShaking)
		m_runShaking = !m_runShaking;

	if (m_btnRunOutput)
		RunOutput();

	if (m_btnRunExport)
		RunExport();

	if (m_btnXrayYZ)
		m_XrayYZ = !m_XrayYZ;

	if (m_btnXrayXY)
		m_XrayXY = !m_XrayXY;

	if (m_btnAniDeformation)
		m_AniDeformation = !m_AniDeformation;

	if (m_btnScaleStiffness)
		BtnNonLinear();

	if (m_btnViewSpinning)
		m_viewSpinning = !m_viewSpinning;


	//Select which arms to draw
	if (m_drawBonds2)
		SelectArms2();

	//Copy prositions from structures class to opengl arrays
	CopyPositions();

	if (m_drawParticles)
		ColorParticles();

	if (m_drawBonds2)
		ColorArms();

	if (m_btnSwapViewStyle)
	{
		if (m_viewType == viewType::perspective)
			m_viewType = viewType::orthographic;
		else
			m_viewType = viewType::perspective;
	}

}

void StructuralComponent::OnUpdateOpenGL() 
{	
	BtnEvents();

	RunDefAni();

	RunXray();

	//OpenGL 
	{
		if (m_drawParticles)
		{
			GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_pIndexVB));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nParticles, m_vertices));
		}
		if (m_drawZones)
		{
			GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_zLineIndexVB));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_zVertexCounter, m_zoneVertices1D));
		}
		if (m_drawBonds)
		{
			GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_bLineIndexVB));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nParticles, m_vertices));
		}
		if (m_drawBonds2)
		{
			GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_bLineIndexVB2));
			//GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_armIndexCounter, m_vertices2));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_armSubsetIndexCounter, m_vertices3));
		}
		if (m_drawDisc)
		{
			UpdateDiscPosition();
			GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_discIndexVB));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nParticles * (m_nDiscSides + 1), m_discVertices));
		}
		if (m_drawIcosa)
		{
			UpdateIcosaPosition();
			GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_icosaIndexVB));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nParticles * 12, m_icosaVertices));
		}

	}

	GLCall(glClear(GL_COLOR_BUFFER_BIT));
	GLCall(glClear(GL_DEPTH_BUFFER_BIT));

	m_Shader->Bind();

	float oW = (16.0f / 9.0f) * m_orthoSF;
	float oH = 1.0f * m_orthoSF;


	if (m_viewType == viewType::perspective)
		m_Proj = glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f / 9.0f, 0.01f, 100.0f);
	else
		m_Proj = glm::ortho(-1.0f * oW, 1.0f * oW, -1.0f * oH, 1.0f * oH, 0.01f, 100.0f);

	
	if(m_viewSpinning)
	{
		m_spinAngle += m_spinStep;
		m_Rot = glm::rotate(m_Rot, glm::radians(m_spinStep), glm::vec3(0.0f, 0.0f, 1.0f));
	}
	else
		m_Rot = glm::mat4(1.0f);

	m_View = glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up);
	m_Trans = glm::mat4(1.0f);
	m_Model = m_Trans * m_Rot;
	glm::mat4 mvp = m_Proj * m_View * m_Model;

	m_Shader->SetUniformMat4f("u_Model", m_Model);
	m_Shader->SetUniformMat4f("u_View", m_View);
	m_Shader->SetUniformMat4f("u_Proj", m_Proj);
	m_Shader->SetUniform1f("u_clipDistance1", m_clipDistance1);
	m_Shader->SetUniform1f("u_clipDistance2", m_clipDistance2);
	m_Shader->SetUniform1f("u_clipDistance3", m_clipDistance3);
	m_Shader->SetUniform1f("u_clipDistance4", m_clipDistance4);

	//Enable and disable clipping plane
	if (m_enableClipping)
	{
		glEnable(GL_CLIP_DISTANCE0);
		glEnable(GL_CLIP_DISTANCE1);
		glEnable(GL_CLIP_DISTANCE2);
		glEnable(GL_CLIP_DISTANCE3);
	}
	else
	{
		glDisable(GL_CLIP_DISTANCE0);
		glDisable(GL_CLIP_DISTANCE1);
		glDisable(GL_CLIP_DISTANCE2);
		glDisable(GL_CLIP_DISTANCE3);
	}
}

void StructuralComponent::OnRender()
{
	//Draw lines
	if (m_drawZones)
	{
		GLCall(glBindVertexArray(m_zLineIndexVA));
		GLCall(glDrawElements(GL_LINES, m_zLineIndexCounter, GL_UNSIGNED_INT, nullptr));
	}

	//Draw lines
	if (m_drawBonds)
	{
		GLCall(glBindVertexArray(m_bLineIndexVA));
		GLCall(glDrawElements(GL_LINES, m_armIndexCounter, GL_UNSIGNED_INT, nullptr));
	}

	//Draw lines 2
	if (m_drawBonds2)
	{
		GLCall(glBindVertexArray(m_bLineIndexVA2));
		//GLCall(glDrawElements(GL_LINES, m_armIndexCounter, GL_UNSIGNED_INT, nullptr));
		GLCall(glDrawElements(GL_LINES, m_armSubsetIndexCounter, GL_UNSIGNED_INT, nullptr));
	}

	//Draw disc
	if (m_drawDisc)
	{
		GLCall(glBindVertexArray(m_discIndexVA));
		GLCall(glDrawElements(GL_TRIANGLES, m_nParticles * m_nDiscSides * 3, GL_UNSIGNED_INT, nullptr));
	}

	//Draw points
	if (m_drawParticles)
	{
		GLCall(glBindVertexArray(m_pIndexVA));
		GLCall(glDrawElements(GL_POINTS, m_nParticles, GL_UNSIGNED_INT, nullptr));
	}

	//Draw icosahedron
	if (m_drawIcosa)
	{
		GLCall(glBindVertexArray(m_icosaIndexVA));
		GLCall(glDrawElements(GL_TRIANGLES, m_nParticles * 20 * 3, GL_UNSIGNED_INT, nullptr));
	}
}

//---------------------UI and dynamic moving----------------------//

void StructuralComponent::OnImGuiRenderer()
{
	ImGui::TextColored(ImVec4(1, 1, 0, 1), "CLIPPING PLANES:");
	ImGui::Checkbox("Enable clipping", &m_enableClipping);
	m_btnXrayYZ = ImGui::Button("X-Ray Loop YZ-plane");
	m_btnXrayXY = ImGui::Button("X-Ray Loop XY-plane");
	ImGui::SliderFloat("YZ front", &m_clipDistance1, -0.08, 0.08);
	ImGui::SliderFloat("YZ back", &m_clipDistance2, -0.08, 0.08);
	ImGui::SliderFloat("XY top", &m_clipDistance3, -0.08, 0.08);
	ImGui::SliderFloat("XY bot", &m_clipDistance4, -0.08, 0.08);

	ImGui::TextColored(ImVec4(1, 1, 0, 1), "PERFORMANCE:");
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::Text("Iteration counter %.1f", (float)m_structure.m_disp.stepCounter);
	ImGui::Text("Time elapsed: %.0f:%.0f", m_min, m_sec);

	ImGui::TextColored(ImVec4(1, 1, 0, 1), "PERSP or ORTHO:");
	m_btnSwapViewStyle = ImGui::Button("Swap view");

	ImGui::TextColored(ImVec4(1, 1, 0, 1), "SPINNING VIEW:");
	m_btnViewSpinning = ImGui::Button("Spinning view");

	ImGui::TextColored(ImVec4(1, 1, 0, 1), "DRAWING OPTIONS:");
	ImGui::Checkbox("Particles", &m_drawParticles);
	ImGui::Checkbox("Zones", &m_drawZones);
	ImGui::Checkbox("Arms P", &m_drawBonds);
	ImGui::Checkbox("Arms A", &m_drawBonds2);
	ImGui::Checkbox("Disc", &m_drawDisc);
	ImGui::Checkbox("Solid", &m_drawIcosa);

	ImGui::TextColored(ImVec4(1, 1, 0, 1), "DEFORMATION SCALE:");
	m_btnAniDeformation = ImGui::Button("Animate Deformation");
	ImGui::SliderFloat("", &m_scaleFactor, 0.0f, 100.0f);

	ImGui::TextColored(ImVec4(1, 1, 0, 1), "COLOR CAP:");
	ImGui::SliderFloat("colorCap", &m_colorCap, 0.0f, 1.0f);

	ImGui::TextColored(ImVec4(1, 1, 0, 1), "SELECTION CAP:");
	ImGui::SliderFloat("selectionCap", &m_selectionCap, 0.0f, 1.0f);

	ImGui::TextColored(ImVec4(1, 1, 0, 1), "COLOR PARTICLES BY:");
	const char* itemsP[] = {  "displacement", "velocity", "acceleration", "meanStrain", "forceArm", "forceExt", "forceTot", "damage", "isBC", "life", "strainSquared", "strain" };
	ImGui::Combo(" ", &m_selectedItemParticleColorBy, itemsP, IM_ARRAYSIZE(itemsP));
	m_colorParticleBy = (colorParticleBy)m_selectedItemParticleColorBy;

	ImGui::TextColored(ImVec4(1, 1, 0, 1), "COLOR ARMS BY:");
	const char* itemsA[] = { "elongationToFracture", "strain","plasticStrain","force","forceComTen","broken", "stiffnessSF", "anisotropy", "tension", "compression"};
	ImGui::Combo("  ", &m_selectedItemArmColorBy, itemsA, IM_ARRAYSIZE(itemsA));
	m_colorArmBy = (colorArmBy)m_selectedItemArmColorBy;

	ImGui::TextColored(ImVec4(1, 1, 0, 1), "Analysis type:");
	m_btnScaleStiffness = ImGui::Button("Scale Stiffness");

	ImGui::TextColored(ImVec4(1, 1, 0, 1), "Action:");
	m_btnRunShaking = ImGui::Button("Run Shaking");
	m_btnRunAnalysis = ImGui::Button("Run Analysis");
	m_btnRunOutput = ImGui::Button("Output results");
	m_btnRunExport = ImGui::Button("Export Model");

}

void StructuralComponent::UpdatePositions()
{
	int counter = 0;
	int quadCounter = 0;
	float pSpeed = 0.001;

	for (int i = 0; i < m_nParticles * 4; i += 4)
	{
		float xRnd = pSpeed * (((float)rand() / (RAND_MAX)) - 0.5);
		float yRnd = pSpeed * (((float)rand() / (RAND_MAX)) - 0.5);
		float zRnd = 0; //m_pSpeed * (((float)rand() / (RAND_MAX)) - 0.5);

		m_vertices[quadCounter].Position.x += xRnd;
		m_vertices[quadCounter].Position.y += yRnd;
		m_vertices[quadCounter].Position.z += zRnd;

		quadCounter++;
	}
}

void StructuralComponent::UpdateDiscPosition()
{
	int pIndex = 0;

	for (int i = 0; i < m_nParticles; i++)
	{
		float diffX = m_vertices[i].Position.x - m_discVertices[pIndex].Position.x;
		float diffY = m_vertices[i].Position.y - m_discVertices[pIndex].Position.y;
		float diffZ = m_vertices[i].Position.z - m_discVertices[pIndex].Position.z;

		for (int j = 0; j <= m_nDiscSides; j++)
		{
			m_discVertices[pIndex + j].Position.x += diffX;
			m_discVertices[pIndex + j].Position.y += diffY;
			m_discVertices[pIndex + j].Position.z += diffZ;
		}

		pIndex += m_nDiscSides + 1;
	}
}

void StructuralComponent::UpdateDiscSize()
{
	int pIndex = 0;
	float smallestHa = 1e10;
	float largestHa = -1e10;

	for (int i = 0; i < m_nParticles; i++)
	{
		if (m_pMassSize[i] > largestHa)
			largestHa = m_pMassSize[i];

		if (m_pMassSize[i] < smallestHa)
			smallestHa = m_pMassSize[i];
	}

	float haMaxForColor = largestHa - smallestHa;

	for (int i = 0; i < m_nParticles; i++)
	{
		float haForColor = m_pMassSize[i] - smallestHa; //Make sure the size goes from 0 -> upwards for percentage calc.
		float perc = 100 * (haForColor / haMaxForColor);
		Vec4 color = GetBlendedColor(perc);

		float newR = m_pMassSize[i];
		m_discVertices[pIndex].Position.x = m_vertices[i].Position.x;
		m_discVertices[pIndex].Position.y = m_vertices[i].Position.y;
		m_discVertices[pIndex].Position.z = m_vertices[i].Position.z;

		float pX = m_discVertices[pIndex].Position.x;
		float pY = m_discVertices[pIndex].Position.y;

		float oldR = Distance(m_discVertices[pIndex], m_discVertices[pIndex + 1]);

		float sf = newR / oldR;

		for (int j = 1; j <= m_nDiscSides; j++)
		{
			m_discVertices[pIndex + j].Position.x = pX + sf * (m_discVertices[pIndex + j].Position.x - pX);
			m_discVertices[pIndex + j].Position.y = pY + sf * (m_discVertices[pIndex + j].Position.y - pY);
			m_discVertices[pIndex + j].Color = color;
		}

		m_discVertices[pIndex].Color = color;

		pIndex += m_nDiscSides + 1;
	}
}

void StructuralComponent::UpdateIcosaPosition()
{
	int pIndex = 0;

	for (int i = 0; i < m_nParticles; i++)
	{
		float diffX = m_vertices[i].Position.x - m_icosaCenter[i].Position.x;
		float diffY = m_vertices[i].Position.y - m_icosaCenter[i].Position.y;
		float diffZ = m_vertices[i].Position.z - m_icosaCenter[i].Position.z;

		for (int j = 0; j < 12; j++)
		{
			m_icosaVertices[pIndex + j].Position.x += diffX;
			m_icosaVertices[pIndex + j].Position.y += diffY;
			m_icosaVertices[pIndex + j].Position.z += diffZ;
		}

		m_icosaCenter[i].Position.x = m_vertices[i].Position.x;
		m_icosaCenter[i].Position.y = m_vertices[i].Position.y;
		m_icosaCenter[i].Position.z = m_vertices[i].Position.z;

		pIndex += 12;
	}
}

void StructuralComponent::UpdateIcosaSize()
{
	int pIndex = 0;

	for (int i = 0; i < m_nParticles; i++)
	{
		float newR = m_pMassSize[i];
		float pX = m_vertices[i].Position.x;
		float pY = m_vertices[i].Position.y;
		float pZ = m_vertices[i].Position.z;

		float oldR = Distance(m_vertices[i], m_icosaVertices[pIndex]);

		float sf = newR / oldR;

		for (int j = 0; j < 12; j++)
		{
			m_icosaVertices[pIndex + j].Position.x = pX + sf * (m_icosaVertices[pIndex + j].Position.x - pX);
			m_icosaVertices[pIndex + j].Position.y = pY + sf * (m_icosaVertices[pIndex + j].Position.y - pY);
			m_icosaVertices[pIndex + j].Position.z = pZ + sf * (m_icosaVertices[pIndex + j].Position.z - pZ);
		}

		pIndex += 12;
	}
}

float StructuralComponent::GenRandom(float min, float max)
{
	float rnd = (float)rand() / (RAND_MAX);
	float diff = max - min;
	float val = min + rnd * diff;
	return val;
}

void StructuralComponent::ExportModel()
{
	m_structure.ExportModel();

	/*
	std::ofstream fileP("output/coord.txt");

	for (int i = 0; i < m_nParticles; i++)
	{
		float x = m_vertices[i].Position.x;
		float y = m_vertices[i].Position.y;
		float z = m_vertices[i].Position.z;
		float r = m_pMassSize[i];

		fileP << x << ',' << y << ',' << z << ',' << r << '\n';
	}
	fileP.close();

	std::ofstream fileC("output/color.txt");

	for (int i = 0; i < m_nParticles; i++)
	{
		float x = m_vertices[i].Color.x;
		float y = m_vertices[i].Color.y;
		float z = m_vertices[i].Color.z;
		float r = m_vertices[i].Color.w;

		fileC << x << ',' << y << ',' << z << ',' << r << '\n';
	}
	fileC.close();
	*/
}

//Supporting mesh functions
vector<Vertex> StructuralComponent::CreateDisc(float x, float y, float z, float radius, int n, Vec4 color)
{
	vector<Vertex> vs;

	float a, aStep, cX, cY, cZ;
	float pi = 3.1415;

	a = 0;
	aStep = (pi * 2) / (n - 1);

	Vertex v;
	v.Position = { x, y, z };
	v.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

	vs.push_back(v);

	for (int i = 0; i < n; i++)
	{
		cX = x + radius * std::cos(a);
		cY = y + radius * std::sin(a);
		cZ = z;

		Vertex vNext;
		vNext.Position = { cX, cY, cZ };
		vNext.Color = color; //{ 0.18f, 0.6f, 0.96f, 1.0f };

		vs.push_back(vNext);

		a += aStep;
	}

	return vs;
}

vector<int> StructuralComponent::GetDiscIndices(int startIndex, int n)
{
	vector<int> indices;

	for (int i = 1; i <= n; i++)
	{
		indices.push_back(startIndex);
		indices.push_back(startIndex + i);

		if (i == n)
			indices.push_back(startIndex + 1);
		else
			indices.push_back(startIndex + 1 + i);
	}

	return indices;
}

vector<Vertex> StructuralComponent::CreateIcoSphere(float x, float y, float z, float radius, Vec4 color)
{
	//Predefined coordinates for the 12 vertices of an icosahedron with 1 in radius
	float xIco[12] = { 0, 0.894427, 0.276393, -0.723607, -0.723607, 0.276393, 0.723607, -0.276393, -0.894427, -0.276393, 0.723607, 0.0 };
	float yIco[12] = { 0, 0, 0.850651, 0.525731, -0.525731, -0.850651, 0.525731, 0.850651, 0.0, -0.850651, -0.525731, 0.0 };
	float zIco[12] = { 1, 0.447214, 0.447214, 0.447214, 0.447214, 0.447214, -0.447214, -0.447214, -0.447214, -0.447214, -0.447214, -1.0 };

	vector<Vertex> vs;
	float a, aStep, cX, cY, cZ;

	for (int i = 0; i < 12; i++)
	{
		cX = x + radius * xIco[i];
		cY = y + radius * yIco[i];
		cZ = z + radius * zIco[i];

		Vertex vNext;
		vNext.Position = { cX, cY, cZ };
		vNext.Color = color;
		vs.push_back(vNext);
	}
	return vs;
}

vector<int> StructuralComponent::GetIcoSphereIndices(int startIndex)
{
	//Indices for 20 triangles for the predifined icosahedron
	vector<int> indices =
	{ 0,1,2,
		0,2,3,
		0,3,4,
		0,4,5,
		0,5,1,
		1,5,10,
		1,6,2,
		1,10,6,
		2,6,7,
		2,7,3,
		3,7,8,
		3,8,4,
		4,8,9,
		4,9,5,
		5,9,10,
		6,10,11,
		6,11,7,
		7,11,8,
		8,11,9,
		9,11,10
	};

	for (int i = 0; i < indices.size(); i++)
		indices[i] += startIndex;

	return indices;
}

//Camera functions
void StructuralComponent::OnUpdateCameraKey(keyAcion ka)
{
	m_Camera.ProcessKeyInput(ka);
}

void StructuralComponent::OnUpdateCameraMouseRotate(float xPos, float yPos)
{
	m_Camera.ProcessMouseInputRotate(xPos, yPos);
}

void StructuralComponent::OnUpdateCameraMousePan(float xPos, float yPos)
{
	m_Camera.ProcessMouseInputPan(xPos, yPos);
}

void StructuralComponent::OnUpdateCameraScroll(float fov)
{
	m_Camera.ProcessScrollInput(fov);
}

void StructuralComponent::OnUpdateCameraReset()
{
	m_Camera.m_FirstMouseRotate = true;
}

void StructuralComponent::OnUpdateCameraResetPan()
{
	m_Camera.m_FirstMousePan = true;
}

//Old functions
void StructuralComponent::IncrementSizeGPU()
{
	/*
	//GPU execusion
	if (m_IncrementSizeGPU)
		m_IncrementSizeCounterGPU = 0;

	if (m_IncrementSizeCounterGPU < 1)
	{
		int n = 10;
		m_ffCuda.CopyFromHostToDevice_Ha(m_pBaseSize);
		m_ffCuda.CopyFromHostToDevice_FriendsCount(m_pCurrentFriendsCount);

		for (int i = 0; i < n; i++)
		{
			m_ffCuda.IncrementSize();
			m_ffCuda.SwapArrays_Ha();
			m_ffCuda.SwapArrays_Friends();
			m_ffCuda.CopyFromDeviceToHost_Ha(m_pBaseSize);
			m_ffCuda.CopyFromDeviceToHost_FriendsCount(m_pCurrentFriendsCount);

			float averageSize = 0;
			int allFriends = 0;

			for (int j = 0; j < m_nParticles; j++)
			{
				averageSize += m_pBaseSize[j] / m_nParticles;
				allFriends += m_pCurrentFriendsCount[i];
			}

			std::cout << "Average size GPU: " << averageSize << std::endl;
			std::cout << "All particle friends GPU: " << allFriends << std::endl;
			std::cout << "Average particle friends GPU: " << allFriends / m_nParticles << std::endl;
		}

		UpdateDiscSize();
		UpdateIcosaSize();
		m_IncrementSizeCounterGPU = 1;
	}
	*/
}

void StructuralComponent::LaplacianCPU()
{
	for (int i = 0; i < m_nParticles; i++)
	{
		int startIndex = m_ppStartIndex[i];
		int rowLength = m_ppRowLength[i];
		int n = rowLength;

		//Get position from current vertex
		float xavrg = m_vertices[i].Position.x;
		float yavrg = m_vertices[i].Position.y;
		float zavrg = m_vertices[i].Position.z;

		float sf = 0.001;

		for (int j = 0; j < rowLength; j++)
		{
			int flatIndex = startIndex + j;
			int pGlobalIndex = m_ppTopFlat[flatIndex];
			xavrg += sf * (m_vertices[pGlobalIndex].Position.x / n);
			yavrg += sf * (m_vertices[pGlobalIndex].Position.y / n);
			zavrg += sf * (m_vertices[pGlobalIndex].Position.z / n);
		}

		//Update position for the copy of the current vertex
		m_vertices[i].Position.x = xavrg;
		m_vertices[i].Position.y = yavrg;
		m_vertices[i].Position.z = zavrg;
	}
}

void StructuralComponent::LaplacianGPU()
{
	/*
	int n = 10;

	m_ffCuda.CopyFromHostToDevice_Particles(m_vertices);

	for (int i = 0; i < n; i++)
	{
		m_ffCuda.ExecuteLaplacian();
		m_ffCuda.SwapArrays_Vertices();
	}

	m_ffCuda.CopyFromDeviceToHost_Particles(m_vertices);
	*/
}
