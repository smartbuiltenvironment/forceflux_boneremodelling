#pragma once

#include "Common.h"
#include "Structure.h"
#include "Camera.h"
#include "Shader.h"
#include <glew.h>
#include "imgui/imgui.h"
#include "glm/glm.hpp" 
#include "glm/gtc/matrix_transform.hpp"

using namespace std;

class StructuralComponent
{
public:
	StructuralComponent(SimulationType simType, ImportType impType, SolverSettings solverSettings, MaterialSettings materialSettings, KernelSettings kernelSettings, DisplacementSettings dispSettings, float strainLimit, string suffix);
	~StructuralComponent();

	void OnUpdateAnalysis(); //override;
	void OnUpdateTime();
	void OnUpdateOpenGL();
	void OnRender(); //override;
	void OnImGuiRenderer(); //override;

	float GetStrain();
	float GetStress();

	void OnUpdateCameraKey(keyAcion ka); // override;
	void OnUpdateCameraMouseRotate(float xPos, float yPos); // override;
	void OnUpdateCameraMousePan(float xPos, float yPos); // override;
	void OnUpdateCameraScroll(float fov); // override;
	void OnUpdateCameraReset(); //override;
	void OnUpdateCameraResetPan(); // override;

	void InitialiseParameters();
	void InitialiseArrays();
	void InitialiseOpenGL();
	void UpdatePositions();
	void UpdateDiscSize();
	void UpdateIcosaSize();
	void UpdateDiscPosition();
	void UpdateIcosaPosition();

	vector<Vertex> CreateDisc(float x, float y, float z, float radius, int n, Vec4 color);
	vector<int> GetDiscIndices(int startIndex, int n);
	vector<Vertex> CreateIcoSphere(float x, float y, float z, float radius, Vec4 color);
	vector<int> GetIcoSphereIndices(int startIndex);
	
	void CreateParticlesRegularInPolygon();
	void CreateParticlesRegularTetras();
	
	void CreateContainer2D(string fileName);
	void CreateContainer3D(string fileName);
	void SetupBB2D(Arr3 zoneCount);
	void SetupBB3D(Arr3 zoneCount);

	void TetraZoning(Arr3 zoneCount);

	void GenDiscs();
	void GenIcosas();
	
	void UpdateBcImportFemur2D();
	void UpdateBcImportFrame2D();
	void UpdateBcImportTwaColumn();
	void UpdateBcImportConcreteFrame();

	void UpdateBcImportGerberette();
	void UpdateBcImportTetraNode();
	void UpdateBcImportNodeRotSym(float rLimit);
	void UpdateBcImportPipe(float rLimit);

	void UpdateBcImport3D();
	void UpdateBcImportFemur3D();

	bool InsideTetrahedron(Vec3 pt);
	bool InsidePolygon(float x, float y);

	void BtnNonLinear();

	void CopyPositions();

	void CalcTargetSize();
	void CalcBaseSize(int nIterations, int nNeighbours);
	void CalcMassSize();
	void CalcHaSize(float sf);
	void IncrementSizeGPU();
	void IncrementParticleSize();
	void IncrementParticleSize1D(int requiredFriendsCount);

	void ShakeParticles(int rndSeed);
	void ShakeParticlesIteration(int n);
	void RunShaking();
	void RunXray();
	void RunDefAni();

	void CreateArms();
	void CreateZones();
	void CreateZone(float x, float y, float z);
	void CreateZoneTopology(float xMp, float yMp, float zMp);

	void ComputeStats(MaterialSettings ms);
	void GenFlatTopology1();
	void GenFlatTopology2();

	bool DoesBondExist(int vIndex, int vNextIndex);
	int FindNextParticleIndex(int vIndex, int vNextIndex);
	int FindExistingArmFlatIndex(int vIndex, int vNextIndex);
	float GenRandom(float min, float max);
		
	//Simulation functions
	void LaplacianCPU();
	void LaplacianGPU();

	//Main run function
	void RunAnalysis();

	//UI based interaction
	void RunOutput();
	void RunExport();

	//Non-UI interaction
	void Output();
	void ExportModel();
	void SaveResults();

	void ColorParticles();
	void ColorArms();

	void SelectArms();
	void SelectArms2();

	void BtnEvents();
		

private:

	unique_ptr<Shader> m_Shader;				//Smart pointers, deleted automatically etc..
	glm::mat4 m_Proj, m_View, m_Model, m_Trans, m_Rot;
	glm::vec3 m_TranslationA, m_TranslationB;

	//Camer parameters
	float m_Fov;
	float m_pSize;

	Camera m_Camera;


	//******************** OpenGL parameters *********************//

	unsigned int m_pIndexVA;			//Vertex array buffer for particles
	unsigned int m_pIndexVB;
	unsigned int m_pIndexIB;

	unsigned int m_zLineIndexVA;
	unsigned int m_zLineIndexVB;
	unsigned int m_zLineIndexIB;

	unsigned int m_bLineIndexVA;
	unsigned int m_bLineIndexVB;
	unsigned int m_bLineIndexIB;

	unsigned int m_discIndexVA;
	unsigned int m_discIndexVB;
	unsigned int m_discIndexIB;

	unsigned int m_icosaIndexVA;
	unsigned int m_icosaIndexVB;
	unsigned int m_icosaIndexIB;

	unsigned int m_bLineIndexVA2;
	unsigned int m_bLineIndexVB2;
	unsigned int m_bLineIndexIB2;

	//******************** OpenGL parameters *********************//


	int m_nParticles;
	int m_nDiscSides;
	int m_shakeCounter;

	int m_armIndexCounter;
	int m_armCounter;
	int m_armVertexCounter;

	int m_armSubsetIndexCounter;

	int m_zLineIndexCounter;
	int m_zLineCounter;
	int m_zVertexCounter;
		
	unsigned int* m_Indices;
	unsigned int* m_arms;
	unsigned int* m_arms2;
	unsigned int* m_zoneLineIndices;
	unsigned int* m_discIndices;
	unsigned int* m_icosaIndices;
		

	float* m_pBaseSize;			//Radius for a based size related to number of neighbours
	float* m_pMassSize;			//Radius for the material size (true volume)
	float* m_pHaSize;			//Radius for the horizon, ie particle the kernel-reach	
	float* m_pMass;

	float m_baseSizeEstimate;
	float m_massSizeEstimate;

	Vertex* m_vertices;			//Vertices for the n-particles	
	Vertex* m_vertices2;		//Vertices for arms with unique colors
	Vertex* m_vertices3;		//Subset of vertices for arms with unique colors

	Vertex* m_zoneVertices1D;	//Flat list of zone corners
	Vertex* m_discVertices;		
	Vertex* m_icosaVertices;
	Vertex* m_icosaCenter;

	vector<int> m_arms3V;


	vector<vector<Vertex>> m_zoneVertices2D;

	int* m_pzTopFlat;					//Particel zone topology

	vector<vector<int>> m_zpTop;		//Zone Particle topology
	vector<int> m_zpTopFlatV;
	int* m_zpTopFlat;
	int* m_zpStartIndex;
	int* m_zpRowLength;
	int m_zpCounter;

	vector<vector<int>> m_zzTop;		//Zone zone topology
	vector<int> m_zzTopFlatV;
	int* m_zzTopFlat;					//Zone zone topology as 1D array
	int* m_zzStartIndex;
	int* m_zzRowLength;
	int m_zzCounter;

	vector<vector<int>> m_ppTop;		//Particle particles topology
	int* m_ppTopFlat;
	int* m_ppStartIndex; 
	int* m_ppRowLength;
	int m_ppCounter;
		
	vector<vector<int>> m_paTop;		//Particle arm topology
	int* m_paTopFlat;
	int* m_paStartIndex;
	int* m_paRowLength;
	int m_paCounter;

	ArmIndices* m_armi;

	GeometrySettings m_gs;

	vector<Vec3> m_outline;							//Outline for a 2D shape
	vector<vector<Vec3>> m_tetras;					//Vertices that define the tetrhedrons that are used to define the volume of a 3D object.
	vector<vector<Vec3>> m_tetrasExtraVertices;		//Extra vertices for the tetrahedrons.

	vector<Vec3> m_tetraVertices;
	vector<vector<int>> m_ztTop;					//Zone tetrahedron topology
	TetraZoneSettings m_tzs;						//Tetrahedron zone settings (tzs)
	ImportType m_importType;

	bool m_scaleStiffness;
	bool m_btnScaleStiffness;
	float m_colorCap;
	float m_selectionCap;
	BB m_containerBB;

	//View parameters
	bool m_btnViewSpinning;
	bool m_viewSpinning;
	float m_spinAngle;
	float m_spinStep;
	bool m_btnSwapViewStyle;
	bool m_btnViewTop;

	Structure m_structure;
	viewType m_viewType;

	//Orthographic view scale factor
	float m_orthoSF;

	//Numbers of zones
	int m_nZones;

	//Simulation parameters
	float m_Horizon;
	Vec3* m_loadDir;					//Load on the particles
	bool* m_isLoaded;				//Array with booleans to track loaded particels
	bool* m_isLocked;				//Array with booleans for locked particels
	bool* m_isSlider;				
	float m_loadMag;				//Loading magnitude
	float m_alpha;					
		
	bool m_runShaking;
	bool m_runAnalysis;
	bool m_runOutput;
	bool m_runExport;
		
	bool m_btnRunAnalysis;
	bool m_btnRunShaking;
	bool m_btnRunOutput;
	bool m_btnRunExport;

	//Paramters for view 
	bool m_btnXrayYZ;
	bool m_XrayYZ;
	int m_XrayDir;
	bool m_btnXrayXY;
	bool m_XrayXY;
	int m_XrayDir2;
	bool m_btnAniDeformation;
	bool m_AniDeformation;
	int m_AniDefDir;

	float m_strain;
	float m_stress;
	float m_strainLimit;

	float m_volTarget;

		
	int m_runCounter;
	int m_tenCounter;
	colorParticleBy m_colorParticleBy;
	colorArmBy m_colorArmBy;


	//UI parameters
	bool m_drawDisc;
	bool m_drawIcosa;
	bool m_drawZones;
	bool m_drawBonds;
	bool m_drawBonds2;
	bool m_drawBroken;
	bool m_drawParticles;
	int m_selectedItemArmColorBy;
	int m_selectedItemParticleColorBy;
	float m_scaleFactor;
	bool m_enableClipping;

	int m_brokenArmsCounter;
	float m_tol;

	int* m_pCurrentFriendsCount;
	int m_pRequiredFriendsCount;
		
	float m_clipDistance1;
	float m_clipDistance2;
	float m_clipDistance3;
	float m_clipDistance4;

	bool m_aColorByStrain;
	bool m_aColorByBroken;

	float m_zMin;
	float m_zMax;
	float m_zPart;
	float m_zPartST;
	float m_waistSF; //For direct tension test setup.

	int m_rndSeed;
		
	Stats m_stats;
	SimulationType m_simType;
		
	std::chrono::steady_clock::time_point m_startTimeAnalysis;
	std::chrono::steady_clock::time_point m_nowTimeAnalysis;
	std::chrono::duration<float> m_durationAnalysis;

	float m_min;
	float m_sec;

	float m_avrgArmLength;

};
