#shader vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Normal;
layout(location = 2) in vec4 a_Color;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_proj;

out vec4 v_Color;
out vec3 v_Normal;
out vec3 v_FragPos;

void main()
{
	gl_Position = u_proj * u_view * u_model * vec4(a_Position, 1);
	v_Color = a_Color;
	v_Normal = a_Normal;
	v_FragPos = vec3(u_model * vec4(a_Position, 1.0));
};

#shader fragment
#version 450 core
out vec4 FragColor;

in vec4 v_Color;
in vec3 v_Normal;
in vec3 v_FragPos;

uniform vec3 objectColor;
uniform vec3 lightColor;
uniform vec3 lightPosition;
uniform vec3 viewPosition;

void main()
{
	float ambientStrength = 0.2;
	vec3 ambient = ambientStrength * lightColor;

	vec3 norm = normalize(v_Normal);
	vec3 lightDir = normalize(lightPosition - v_FragPos);

	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * lightColor;

	float specularStrength = 0.7;
	vec3 viewDir = normalize(viewPosition - v_FragPos);
	vec3 reflectDir = reflect(-lightDir, norm);

	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 16);
	vec3 specular = specularStrength * spec * lightColor;

	vec3 result = (ambient + diffuse + specular) * vec3(v_Color); // objectColor;
	FragColor = vec4(result, 1.0);

};