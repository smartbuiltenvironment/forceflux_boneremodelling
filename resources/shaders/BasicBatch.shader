#shader vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec4 a_Color;

uniform mat4 u_MVP;

vec4 clippingPlane = vec4(0, -1, 0, 0.05);

out vec4 v_Color;

void main()
{
	//gl_ClipDistance[0] = dot(worldPosition, clippingPlane);

	gl_Position = u_MVP * vec4(a_Position, 1);
	v_Color = a_Color;
};

#shader fragment
#version 450 core
layout(location = 0) out vec4 o_Color;

in vec4 v_Color;

void main()
{
	o_Color = v_Color;
};