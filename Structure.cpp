#include "Structure.h"


Structure::Structure()
{
}

Structure::Structure(
	int nParticles,
	int nArms,
	int nZones,
	int ppCount,
	int paCount,
	Vertex* vertices,
	ArmIndices* arms,
	float* massSize,
	Vec3* loadVecs,
	bool* isLoaded,
	bool* isLocked,
	bool* isSlider,
	int* ppTop,
	int* ppStartIndex,
	int* ppRowLength,
	int* paTop,
	int* paStartIndex,
	int* paRowLength,
	SimulationType simType,
	ImportType impType,
	GeometrySettings gs,
	SolverSettings solverSettings,
	MaterialSettings materialSettings,
	KernelSettings kernelSettings,
	DisplacementSettings dispSettings,
	Stats stats,
	string suffix)
{
	m_fileNameSuffix = suffix;
	
	m_nArms = nArms;
	m_nParticles = nParticles;
	m_nZones = nZones;

	m_scaleStiffness = false;
	m_plasticityOn = false; 
	m_CheckFracture = false;
	m_isAnisotrop = true;
	m_isCracked = true;
	m_simType = simType;
	m_importType = impType;
	m_gs = gs;
	m_stats = stats;

	m_crackDirection = crackDirection::tangential;

	m_arms = std::vector<Arm>(m_nArms);
	m_particles = std::vector<Particle>(m_nParticles);
	m_pInitPos = std::vector<Vec3>(m_nParticles);
	m_loadDir = std::vector<Vec3>(m_nParticles);
	m_isLoaded = std::vector<bool>(m_nParticles);
	m_isLocked = std::vector<bool>(m_nParticles);
	m_isSlider = std::vector<bool>(m_nParticles);

	m_ppCount = ppCount;

	m_ppTopV = std::vector<int>(m_ppCount);
	m_ppStartIndexV = std::vector<int>(m_nParticles);
	m_ppRowLengthV = std::vector<int>(m_nParticles);

	m_paCount = paCount;

	m_paTopV = std::vector<int>(m_ppCount);
	m_paStartIndexV = std::vector<int>(m_nParticles);
	m_paRowLengthV = std::vector<int>(m_nParticles);

	m_bc = BC::springLoad;

	m_nBrokenArms = 0;

	m_kernel = kernelSettings;
	m_solver = solverSettings;
	m_material = materialSettings;
	m_disp = dispSettings;

	m_anisotropy = Anisotropy::Cylindrical;
	
	SetupLoad();
	SetupSpring();
	SetupPoissonsRatio();

	CreateStructure(vertices, arms, massSize, loadVecs, isLoaded, isLocked, isSlider);
	CopyTopology(ppTop, ppStartIndex, ppRowLength, paTop, paStartIndex, paRowLength);

	SetupAnisotropy();
	SetupCracks();

	//CreateGaugePlanes();

	std::cout << "Structure created" << std::endl;
}

Structure::~Structure()
{

}

void Structure::SetupKernel(float alpha)
{

}

void Structure::SetupLoad()
{
	m_load.magnitude = 1e9;		//N/m3
	m_load.nLoadSteps = 100;
	m_load.stepCounter = 1;
}

void Structure::SetupSpring()
{
	m_spring.k = 1.0e6; //3.0e6;
	m_spring.iLength = 0.01;
}

void Structure::SetupPoissonsRatio()
{
	m_prs.hLimit = 0.95 * m_gs.height / 2.0f;
	m_prs.waistHeight = 0.5f * m_gs.height;
	m_prs.rLimit = 0.95f * m_gs.radius / 2.0f;
}

void Structure::CreateStructure(Vertex* vertices, ArmIndices* arms, float* massSize, Vec3* loadVecs, bool* isLoaded, bool* isLocked, bool* isSlider)
{
	float pi = 3.1415;
	double dOriginMin = 1e30;

	for (int i = 0; i < m_nParticles; i++)
	{
		float mass = ((4.0f / 3.0f) * pi * massSize[i] * massSize[i] * massSize[i]) * m_material.rho;

		Particle p;
		p.iPosition = vertices[i].Position;
		p.cPosition = vertices[i].Position;
		p.Color = vertices[i].Color;
		p.rMass = mass;
		p.h = massSize[i]; //m_kernel.alpha * massSize[i];
		p.Veloc = { 0, 0, 0 };
		p.Accel = { 0, 0, 0 };
		p.ExtForce = { 0, 0, 0 };
		p.TotForce = { 0, 0, 0 };
		p.fMass = mass;
		p.fMasses = { mass, mass, mass };
		p.meanStrain = 0;
		p.rSize = massSize[i];
		p.damage = 0.0f;
		p.isBC = false;
		p.strainSquared = 0.0f;
		p.strain = 0.0f;
		p.life = pLife::alive;

		Vec3 v;
		v.x = p.cPosition.x;
		v.y = p.cPosition.y;
		v.z = p.cPosition.z;
		m_pInitPos[i] = v;

		m_isLoaded[i] = isLoaded[i];
		m_isLocked[i] = isLocked[i];
		m_isSlider[i] = isSlider[i];
		m_loadDir[i] = loadVecs[i];

		if (m_isLoaded[i] || m_isLocked[i] || m_isSlider[i])
		{
			p.isBC = true;
			p.life = pLife::immortal;
		}
		
		m_particles[i] = p;

		m_kernelConcistency.push_back(0.0f);

		float r = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);

		if (r < dOriginMin) 
		{
			dOriginMin = r;
			m_pClosestToOrigin = i;
		}
	}

	float length = 0;
	float lengthSquared;

	for (int i = 0; i < m_nArms; i++)
	{
		Vec4 color = { 0.5f, 0.5f, 0.5f, 1.0f };
		Arm a;
		a.p1 = arms[i].p1;
		a.p2 = arms[i].p2; 

		length = Distance(m_particles[a.p1], m_particles[a.p2]);
		lengthSquared = length * length;
		a.iLength = length;
		a.cLength = 0.0f;
		a.iLengthSquared = lengthSquared;
		a.plasticStrain = 0.0f;
		a.strain = 0.0f;
		a.color = color;
		a.fMag = 0.0f;
		a.broken = false;
		a.elongation = 0.0f;
		a.selected = false;
		a.stiffness = 0.0f;
		a.sf = 1.0f;
		a.dir_sf = 1.0f;
		m_arms[i] = a;
	}
}

void Structure::CopyTopology(int* ppTop, int* ppStartIndex, int* ppRowLength, int* paTop, int* paStartIndex, int* paRowLength)
{
	for (int i = 0; i < m_ppCount; i++)
		m_ppTopV[i] = ppTop[i];

	for (int i = 0; i < m_paCount; i++)
		m_paTopV[i] = paTop[i];

	for (int i = 0; i < m_nParticles; i++)
	{
		m_ppStartIndexV[i] = ppStartIndex[i];
		m_ppRowLengthV[i] = ppRowLength[i];
		m_paStartIndexV[i] = paStartIndex[i];
		m_paRowLengthV[i] = paRowLength[i];
	}
}

void Structure::SetupAnisotropy()
{
	Vec3 avrgVec;
	Vec3 vecAB;
	Vec3 vecBA;
	float pi = 3.1415;

	float sf_lower = 0.7f;
	float sf_upper = 1.0f;

	if (m_anisotropy == Anisotropy::Cylindrical) 
	{
		for (int i = 0; i < m_nArms; i++)
		{
			Particle pa = m_particles[m_arms[i].p1];
			Particle pb = m_particles[m_arms[i].p2];
			
			//Vector to the average position between particle A and B
			avrgVec.x = (pb.cPosition.x + pa.cPosition.x)/2.0f;
			avrgVec.y = (pb.cPosition.y + pa.cPosition.y)/2.0f;
			avrgVec.z = 0.0f;
			avrgVec = UnitizeVector(avrgVec);

			//Vector from A to B
			vecAB.x = pb.cPosition.x - pa.cPosition.x;
			vecAB.y = pb.cPosition.y - pa.cPosition.y;
			vecAB.z = 0.0f;
			vecAB = UnitizeVector(vecAB);

			//Vector from B to A
			vecBA.x = pa.cPosition.x - pb.cPosition.x;
			vecBA.y = pa.cPosition.y - pb.cPosition.y;
			vecBA.z = 0.0f;
			vecBA = UnitizeVector(vecBA);

			float angle1 = VectorAngle(vecAB, avrgVec);
			float angle2 = VectorAngle(vecBA, avrgVec);

			float angle = angle1;
			if (angle2 < angle1)
				angle = angle2;

			float frac = angle / (pi / 2.0f);

			if (!std::isnan(frac)) 
			{
				float sf = Remap(sf_lower, sf_upper, 0.0f, 1.0f, frac);
				m_arms[i].dir_sf = sf;
			}
		}
	}
	else if (m_anisotropy == Anisotropy::BuildDirX || m_anisotropy == Anisotropy::BuildDirY || m_anisotropy == Anisotropy::BuildDirZ)
	{	
		Vec3 dirVec = { 1, 0, 0 };
		if (m_anisotropy == Anisotropy::BuildDirY)
			dirVec = { 0, 1, 0 };
		else if (m_anisotropy == Anisotropy::BuildDirZ)
			dirVec = { 0, 0, 1 };

		for (int i = 0; i < m_nArms; i++)
		{
			Particle pa = m_particles[m_arms[i].p1];
			Particle pb = m_particles[m_arms[i].p2];

			//Vector from A to B
			vecAB.x = pb.cPosition.x - pa.cPosition.x;
			vecAB.y = pb.cPosition.y - pa.cPosition.y;
			vecAB.z = pb.cPosition.z - pa.cPosition.z;
			vecAB = UnitizeVector(vecAB);

			//Vector from B to A
			vecBA.x = pa.cPosition.x - pb.cPosition.x;
			vecBA.y = pa.cPosition.y - pb.cPosition.y;
			vecBA.z = pa.cPosition.z - pb.cPosition.z;
			vecBA = UnitizeVector(vecBA);

			float angle1 = VectorAngle(vecAB, dirVec);
			float angle2 = VectorAngle(vecBA, dirVec);

			float angle = angle1;
			if (angle2 < angle1)
				angle = angle2;

			float frac = angle / (pi / 2.0f);

			if (!std::isnan(frac))
			{
				float sf = Remap(sf_lower, sf_upper, 0.0f, 1.0f, frac);
				m_arms[i].dir_sf = sf;
			}
		}
	
	}
}

void Structure::SetupCracks() 
{
	if (m_isCracked)
	{
		m_cracks = std::vector<Crack>();

		CreateRandomCracks();
		
		ImplementCracks();
	}
}


void Structure::CreateRandomCracks()
{
	float minXY = 0.02;
	float maxXY = 0.06;
	float rangeZ = 0.012;

	m_nCracks = 10;

	float crack_radius = 0.01;
	int crack_counter = 0;
	int attem_counter = 0;
	int whilebreak = 1000;

	//#define RAND_MAX = m_nParticles;

	std::cout << "m_nParticles: " << m_nParticles << std::endl;

	while(crack_counter <= m_nCracks && attem_counter <= whilebreak)
	{
		//int rndIndex = GenRandomInt(0, m_nParticles);     //std::rand() * m_nParticles; //std::rand() % m_nParticles + 1; //std::rand() * 2;

		int rndIndex = std::rand() * 2;

		std::cout << "rndIndex: " << rndIndex << std::endl;

		if (rndIndex < m_nParticles) 
		{
			Vec3 pos = m_particles[rndIndex].iPosition;

			float d = sqrt(pos.x * pos.x + pos.y * pos.y + +pos.z * pos.z);

			if (d > minXY && d < maxXY) 
			{
				//std::cout << "Crack origin:" << pos.x << ", " << pos.y << ", " << pos.z << std::endl;
				Crack c = CreateCrack(pos, crack_radius, m_crackDirection);
				m_cracks.push_back(c);
			}
		
			crack_counter++;	
		}
		attem_counter++;
	}

	std::cout << "Crack counter: " << crack_counter << std::endl;
}

Crack Structure::CreateCrack(Vec3 crackPosition, float crack_radius, crackDirection crackDir)
{
	Vec3 z = { 0, 0, 1 };
	Vec3 y = { 0, 1, 0 };

	Crack c;
	Plane p;
	Vec3 xAxis;
	Vec3 yAxis;

	p.Origin = crackPosition;
	Vec3 normal = crackPosition;
	normal = UnitizeVector(normal);

	std::cout << "Crack origin:" << crackPosition.x << ", " << crackPosition.y << ", " << crackPosition.z << std::endl;

	//Flip the normal 90 deg if the crack is radial. Otherwise its tangential.
	if (crackDir == crackDirection::radial) 
	{
		normal = CrossProduct(normal, z);
	}


	if (!IsSameVector(normal, z)) 
	{
		xAxis = CrossProduct(normal, z);
		yAxis = CrossProduct(normal, xAxis);
	}
	else
	{
		xAxis = CrossProduct(normal, y);
		yAxis = CrossProduct(normal, xAxis);
	}

	p.Normal = normal;
	p.xAxis = xAxis;
	p.yAxis = yAxis;

	std::vector<float> abcd = GetPlaneEquation(p);
		
	p.a = abcd[0];
	p.b = abcd[1];
	p.c = abcd[2];
	p.d = abcd[3];

	c.plane = p;
	c.radius = crack_radius;

	return c;
}

void Structure::ImplementCracks() 
{
	int counter = 0;

	for (int i = 0; i < m_cracks.size(); i++)
	{
		for (int j = 0; j < m_nArms; j++) 
		{
			Particle pa = m_particles[m_arms[j].p1];
			Particle pb = m_particles[m_arms[j].p2];
			if (IsArmCrossingCrack(pa, pb, m_cracks[i]))
			{
				m_arms[j].broken = true;
				m_arms[j].fMag = 0.0f;
				m_arms[j].elongation = m_material.elongationLimit;
				m_arms[j].color = { 1.0f, 1.0f, 1.0f, 1.0f };
				counter++;
			}
		}
	}

	std::cout << "Broken arms from cracks: " << counter << std::endl;
}

//Simulation to generate porous structures
void Structure::RunAnalysis_BoneRemodelling()
{
	m_plasticityOn = false;
	m_CheckFracture = false;

	CalcParticleFictiousMass();

	ZeroParticleForce();			//P-loop

	CalcFirstPartialVelocity();		//P-loop

	CalcParticleMeanStrain();		//P-A-loop

	CalcArmForceSimpleEnergy();

	ApplyExternalSpringDisp_New();	//P-loop
	
	CalcAcceleration();				//P-loop

	CalcSecondPartialVelocity();	//P-loop

	ScaleStiffnessArms();

	SumVelocity();

	m_disp.stepCounter++;

	//Increment time steps
	m_solver.t += m_solver.dt;
	m_solver.t2 = m_solver.t + m_solver.dt;
	m_solver.t1 = 0.5 * (m_solver.t + m_solver.t2);
}

//Simulation to run steel material calibration
void Structure::RunAnalysis_SteelTest()
{
	m_plasticityOn = true;
	m_CheckFracture = true;

	CalcParticleFictiousMass();

	ZeroParticleForce();			//P-loop

	CalcFirstPartialVelocity();		//P-loop

	CalcParticleMeanStrain();		//P-A-loop

	CalcArmForce();					//A-loop

	ApplyExternalSpringDisp_New();	//P-loop

	CalcAcceleration();				//P-loop

	CalcSecondPartialVelocity();	//P-loop

	SumVelocity();

	m_disp.stepCounter++;

	//Increment time steps
	m_solver.t += m_solver.dt;
	m_solver.t2 = m_solver.t + m_solver.dt;
	m_solver.t1 = 0.5 * (m_solver.t + m_solver.t2);
}

//Simulation to analyse imported steel component
void Structure::RunAnalysis_SteelComponent()
{
	m_plasticityOn = true;
	m_CheckFracture = true;

	//CalcParticleFictiousMass();

	ZeroParticleForce();			//P-loop

	CalcFirstPartialVelocity();		//P-loop

	CalcParticleMeanStrain();		//P-A-loop

	CalcArmForce();					//A-loop

	ApplyExternalSpringDisp_New();	//P-loop

	CalcAcceleration();				//P-loop

	CalcSecondPartialVelocity();	//P-loop

	SumVelocity();

	m_disp.stepCounter++;

	//Increment time steps
	m_solver.t += m_solver.dt;
	m_solver.t2 = m_solver.t + m_solver.dt;
	m_solver.t1 = 0.5 * (m_solver.t + m_solver.t2);
}


//Steel component analysis functions
void Structure::CalcParticleFictiousMass()
{
	float dtSquared = m_solver.dt * m_solver.dt;
	float minMass = 0.0000001;

	for (int i = 0; i < m_nParticles; i++)
	{
		//m_particles[i].fMasses.x = minMass + (1.0f / 4.0f) * dtSquared * m_solver.sScale * m_particles[i].stiffness.x;
		//m_particles[i].fMasses.y = minMass + (1.0f / 4.0f) * dtSquared * m_solver.sScale * m_particles[i].stiffness.y;
		//m_particles[i].fMasses.z = minMass + (1.0f / 4.0f) * dtSquared * m_solver.sScale * m_particles[i].stiffness.z;

		m_particles[i].fMasses.x = 1.0e-6;
		m_particles[i].fMasses.y = 1.0e-6;
		m_particles[i].fMasses.z = 1.0e-6;

		m_particles[i].strainSquared = 0.0f;
		m_particles[i].strain = 0.0f;
	}
}

void Structure::ZeroParticleForce()
{
	for (int i = 0; i < m_nParticles; i++)
	{
		m_particles[i].ArmForce.x = 0;
		m_particles[i].ArmForce.y = 0;
		m_particles[i].ArmForce.z = 0;
	}
}

void Structure::CalcFirstPartialVelocity()
{
	float co = m_solver.co;
	float t = m_solver.t;
	float t1 = m_solver.t1;
	float dt = m_solver.dt;

	bool is3D = true;
	if (m_importType == ImportType::Femur2D || m_importType == ImportType::Frame2D)
		is3D = false;

	for (int i = 0; i < m_nParticles; i++)
	{
		m_particles[i].Veloc.x = m_solver.co * m_particles[i].Veloc.x + (t1 - t) * m_particles[i].Accel.x;
		m_particles[i].Veloc.y = m_solver.co * m_particles[i].Veloc.y + (t1 - t) * m_particles[i].Accel.y;

		if(is3D)
			m_particles[i].Veloc.z = m_solver.co * m_particles[i].Veloc.z + (t1 - t) * m_particles[i].Accel.z;

		if(!m_isLocked[i] && !m_isSlider[i])
		{
			if (!m_isLoaded[i])
			{
				//Allow movement in all directions for unloaded particles
				m_particles[i].cPosition.x = m_particles[i].cPosition.x + m_particles[i].Veloc.x * dt;
				m_particles[i].cPosition.y = m_particles[i].cPosition.y + m_particles[i].Veloc.y * dt;
				
				if (is3D)
					m_particles[i].cPosition.z = m_particles[i].cPosition.z + m_particles[i].Veloc.z * dt;
			}
			else if (m_isLoaded[i] && m_bc == BC::springLoad)
			{
				//Allow movement in all directions for loaded particles
				m_particles[i].cPosition.x = m_particles[i].cPosition.x + m_particles[i].Veloc.x * dt;
				m_particles[i].cPosition.y = m_particles[i].cPosition.y + m_particles[i].Veloc.y * dt;
				
				if (is3D)
					m_particles[i].cPosition.z = m_particles[i].cPosition.z + m_particles[i].Veloc.z * dt;
			}
		}
		else if(m_isSlider[i])
		{	
			//Allow movement in x and y-dir
			m_particles[i].cPosition.x = m_particles[i].cPosition.x + m_particles[i].Veloc.x * dt;
			m_particles[i].cPosition.y = m_particles[i].cPosition.y + m_particles[i].Veloc.y * dt;
		}

	}
}

void Structure::CalcParticleMeanStrain()
{
	float mb, ma, ha, hb, rab, Wab, Wba, strain_ab, hbCube;
	float rho = m_material.rho;
	Arm arm;
	Particle p, pNext;
	float damage = 0.0;
	int brokenBondsCounter = 0;
	m_avrgParticleMeanStrain = 0.0f;
	
	for (int i = 0; i < m_nParticles; i++)
	{
		int startIndex = m_paStartIndexV[i];
		int rowLength = m_paRowLengthV[i];
		p = m_particles[i];
		ma = p.rMass;

		brokenBondsCounter = 0;

		float numerator = 0.0f;
		float denominator = 0.0f;

		//Loop around the connecting arms
		for (int j = 0; j < rowLength; j++)
		{
			int paFlatIndex = startIndex + j;
			int aNextIndex = m_paTopV[paFlatIndex];

			arm = m_arms[aNextIndex];
			rab = arm.cLength;

			pNext = m_particles[arm.p2];
			mb = pNext.rMass;
			hb = pNext.h;

			hbCube = hb * hb * hb;

			Wba = Evaluate(rab, hb, m_kernel);			//TODO: Precalculate values and store in an array instead?
			strain_ab = arm.strain;

			//Eq.(65) in paper version F. (Omitted in the final paper)
			numerator += (mb * strain_ab * Wba) / (rho * hbCube);
			denominator += (mb * Wba) / (rho * hbCube);

			if (arm.broken)
				brokenBondsCounter++;
		}

		damage = ((float)brokenBondsCounter) / ((float)rowLength);
		m_particles[i].damage = damage;

		if (denominator > 0)
		{
			m_particles[i].meanStrain = numerator / denominator;
			m_avrgParticleMeanStrain += m_particles[i].meanStrain;
		}

		m_avrgParticleMeanStrain = m_avrgParticleMeanStrain / m_nParticles;
	}
}

void Structure::CalcArmForce()
{
	float ma, mb, Sa, Sb, rab, elongation, strain, plasticStrain;
	float rho = m_material.rho;
	float rhoSquared = rho * rho;
	Vec3 dirVec;

	float min_sf = 1000000000;
	float max_sf = -1000000000;


	for (int i = 0; i < m_nArms; i++)
	{
		if (!m_arms[i].broken)
		{
			Particle pa = m_particles[m_arms[i].p1];
			Particle pb = m_particles[m_arms[i].p2];

			m_arms[i].cLength = Distance(pa, pb);
			m_arms[i].elongation = m_arms[i].cLength - m_arms[i].iLength;
			m_arms[i].strain = m_arms[i].elongation / m_arms[i].iLength;

			float dir_sf = 1.0f;
			if (m_isAnisotrop)
				dir_sf = m_arms[i].dir_sf;

			if (!m_plasticityOn)
			{
				//Eq.(77)
				Sa = CalcForceFlux77(m_arms[i].strain, m_material, pa.meanStrain, dir_sf);
				Sb = CalcForceFlux77(m_arms[i].strain, m_material, pb.meanStrain, dir_sf);
			}
			else
			{
				strain = m_arms[i].strain;
				plasticStrain = m_arms[i].plasticStrain;

				//Eq. (111)
				if (strain - plasticStrain > m_material.yieldStrain)
					plasticStrain = strain - m_material.yieldStrain;
				else if (strain - plasticStrain < -m_material.yieldStrain)
					plasticStrain = strain + m_material.yieldStrain;

				m_arms[i].plasticStrain = plasticStrain;

				//Eq.(110)
				Sa = CalcForceFlux110(strain, m_arms[i].plasticStrain, m_material, pa.meanStrain, dir_sf);
				Sb = CalcForceFlux110(strain, m_arms[i].plasticStrain, m_material, pb.meanStrain, dir_sf);
			}

			float Wpa = EvaluateDerivative(m_arms[i].cLength, pa.h, m_kernel);
			float Wpb = EvaluateDerivative(m_arms[i].cLength, pb.h, m_kernel);
			bool isBroken = false;

			if (m_CheckFracture && (m_arms[i].elongation > m_material.elongationLimit))
				isBroken = true;

			float Tab = 0.0f;


			if (isBroken)
			{
				m_arms[i].fMag = 0.0f;
				m_arms[i].broken = true;
				m_arms[i].color = { 1.0f, 1.0f, 1.0f, 1.0f };
				m_nBrokenArms++;
			}
			else
			{
				ma = pa.rMass;
				mb = pb.rMass;

				//From eq.(70)
				Tab = -(ma * mb) * (((Sa * Wpa) / rhoSquared) + ((Sb * Wpb) / rhoSquared));
				m_arms[i].fMag = Tab;
				dirVec = GetUnitVector(pa, pb, m_arms[i].cLength);

				//Positive to pa
				m_particles[m_arms[i].p1].ArmForce.x += dirVec.x * Tab;
				m_particles[m_arms[i].p1].ArmForce.y += dirVec.y * Tab;
				m_particles[m_arms[i].p1].ArmForce.z += dirVec.z * Tab;

				//Negative to pb
				m_particles[m_arms[i].p2].ArmForce.x += -dirVec.x * Tab;
				m_particles[m_arms[i].p2].ArmForce.y += -dirVec.y * Tab;
				m_particles[m_arms[i].p2].ArmForce.z += -dirVec.z * Tab;
			}
		}
	}

}

void Structure::CalcAcceleration()
{
	//.9 Compute acceleration _accel = _rMass * f. Where f = f_int + f_ext + f_cont.
	//Also multiply with the volume to get the units right.

	for (int i = 0; i < m_nParticles; i++)
	{
		if (m_simType == SimulationType::Bone) 
		{
			m_particles[i].Accel.x = (m_particles[i].ArmForce.x + m_particles[i].ExtForce.x) / m_particles[i].fMasses.x; 
			m_particles[i].Accel.y = (m_particles[i].ArmForce.y + m_particles[i].ExtForce.y) / m_particles[i].fMasses.y; 
			m_particles[i].Accel.z = (m_particles[i].ArmForce.z + m_particles[i].ExtForce.z) / m_particles[i].fMasses.z; 		}
		else
		{
			m_particles[i].Accel.x = (m_particles[i].ArmForce.x + m_particles[i].ExtForce.x) / m_particles[i].fMass;
			m_particles[i].Accel.y = (m_particles[i].ArmForce.y + m_particles[i].ExtForce.y) / m_particles[i].fMass;
			m_particles[i].Accel.z = (m_particles[i].ArmForce.z + m_particles[i].ExtForce.z) / m_particles[i].fMass;
		}
	}
}

void Structure::CalcSecondPartialVelocity()
{
	float t1 = m_solver.t1;
	float t2 = m_solver.t2;

	//10.Second partial velocity update
	for (int i = 0; i < m_nParticles; i++)
	{
		m_particles[i].Veloc.x = m_particles[i].Veloc.x + (t2 - t1) * m_particles[i].Accel.x;
		m_particles[i].Veloc.y = m_particles[i].Veloc.y + (t2 - t1) * m_particles[i].Accel.y;
		m_particles[i].Veloc.z = m_particles[i].Veloc.z + (t2 - t1) * m_particles[i].Accel.z;
	}
}


void Structure::CountBrokenArms()
{
	int aCounter = 0;

	for (int i = 0; i < m_nArms; i++)
	{
		if (m_arms[i].broken)
			aCounter++;
	}

	std::cout << "Broken arms counter: " << aCounter << std::endl;
}

void Structure::ApplyExternalSpringDisp_New()
{
	if (m_disp.stepCounter == 1)
	{
		m_incDisp.push_back(0);
		m_incSpringForceTop.push_back(0);
		m_incSpringForceBot.push_back(0);
	}

	//Displace the top of the spring and resulting force to the particle

	float disp = 0;
	float dispStep = m_disp.stepSize; // = m_disp.magnitude / (float)m_disp.nDispSteps;
	float accumulatedDisp = m_incDisp[m_disp.stepCounter - 1];

	float totSpringForceTop = 0;
	float totSpringForceBot = 0;
	float topStress = 0;
	float dSz, dPz, f, sign;

	for (int i = 0; i < m_nParticles; i++)
	{
		if (m_isLoaded[i])
		{
			Vec3 dirVec = m_loadDir[i];
			
			//Length change of the load spring
			float dL = accumulatedDisp;

			//Length change of the spring is assumed to be positive.
			if (dL > 0)
			{
				f = dL * m_spring.k;
				m_particles[i].ExtForce.x = dirVec.x * f;
				m_particles[i].ExtForce.y = dirVec.y * f;
				m_particles[i].ExtForce.z = dirVec.z * f;
				totSpringForceTop += f;
			}
		}
	}

	//Only increment until the nDispSteps counter is reached.
	if(m_disp.stepCounter < m_disp.nDispSteps)
		accumulatedDisp += dispStep;

	m_incDisp.push_back(accumulatedDisp);
	m_incSpringForceTop.push_back(totSpringForceTop);
	m_incSpringForceBot.push_back(totSpringForceBot);
}


//Bone remodelling specific functions
void Structure::ScaleStiffnessArms()
{
	float sfTot = 0.0f;
	float sfMax = 1.0f;
	float sfMin = 1.0f;
	float sfContribution = 0.0f;

	int countScaledArms = 0;

	//Abort unless the scale stiffness button has been click to flip the boolean.
	if (!m_scaleStiffness)
		return;

	int countNegScaling = 0;

	for (int i = 0; i < m_nArms; i++)
	{
		sfContribution = 0.0f;

		float frac = std::abs(m_arms[i].strain / m_avrgArmStrain);

		sfContribution = std::pow((frac - 1), 3);

		//if (frac < 1.0f)
		//	countStrainLessThenOne++;
		float scaleDamping = 0.0001; //0.0005;

		if (sfContribution < 0)
			scaleDamping = 0.001;

		float newSfArm = m_arms[i].sf + scaleDamping * sfContribution;

		if (newSfArm > 0.01 && newSfArm < 5) //25)//25)//300)
		{
			m_arms[i].sf = newSfArm;
			countScaledArms++;
		}

		if (m_arms[i].sf < sfMin)
			sfMin = m_arms[i].sf;

		if (m_arms[i].sf > sfMax)
			sfMax = m_arms[i].sf;

		sfTot += m_arms[i].sf;
	}

	float sfAvrg = sfTot / m_nArms;

	m_sfMax = sfMax;

	if (m_disp.stepCounter % 10 == 0)
	{
		std::cout << " Min, Average, Max stiffness scale factor: " << sfMin << ", " << sfAvrg << ", " << sfMax << "Scaled arms count: " << countScaledArms << ", " << std::endl;
	}
}

void Structure::CalcArmForceSimpleEnergy()
{
	Vec3 dirVec;
	float strain_ab;
	Vec3 dr;
	Vec3 dv;
	float dr_dot_dv;
	float strainRate;
	float avrgTC = 0.0f;
	float maxTC = 0.0f;
	float minTC = 0.0f;
	float tensionCoefficient;
	float thisStiffness;

	float strainEnergy = 0.0f;
	float totStrainEnergy = 0.0f;

	m_avrgArmStrain = 0.0f;

	for (int i = 0; i < m_nArms; i++)
	{
		Particle pa = m_particles[m_arms[i].p1];
		Particle pb = m_particles[m_arms[i].p2];

		dr.x = pb.cPosition.x - pa.cPosition.x;
		dr.y = pb.cPosition.y - pa.cPosition.y;
		dr.z = pb.cPosition.z - pa.cPosition.z;

		dv.x = pb.Veloc.x - pa.Veloc.x;
		dv.y = pb.Veloc.y - pa.Veloc.y;
		dv.z = pb.Veloc.z - pa.Veloc.z;

		dr_dot_dv = dr.x * dv.x + dr.y * dv.y + dr.z * dv.z;
		strainRate = dr_dot_dv / m_arms[i].iLengthSquared;

		m_arms[i].cLength = Distance(pa, pb);
		m_arms[i].elongation = m_arms[i].cLength - m_arms[i].iLength;
		m_arms[i].strain = m_arms[i].elongation / m_arms[i].iLength;
		m_arms[i].strainRate = strainRate;

		strain_ab = m_arms[i].strain;
		float L0 = m_arms[i].iLength;

		tensionCoefficient = m_arms[i].sf * (strain_ab + 0.1 * strainRate * m_solver.dt) * m_material.basicFibreStiffnessConstant / L0;

		m_arms[i].fMag = tensionCoefficient;
		dirVec = GetUnitVector(pa, pb, m_arms[i].cLength);

		strainEnergy = tensionCoefficient * strain_ab / 2.0f;

		//Positive to pa
		m_particles[m_arms[i].p1].ArmForce.x += dirVec.x * tensionCoefficient;
		m_particles[m_arms[i].p1].ArmForce.y += dirVec.y * tensionCoefficient;
		m_particles[m_arms[i].p1].ArmForce.z += dirVec.z * tensionCoefficient;

		//Negative to pb
		m_particles[m_arms[i].p2].ArmForce.x += -dirVec.x * tensionCoefficient;
		m_particles[m_arms[i].p2].ArmForce.y += -dirVec.y * tensionCoefficient;
		m_particles[m_arms[i].p2].ArmForce.z += -dirVec.z * tensionCoefficient;


		//Increment masses
		thisStiffness = m_arms[i].sf * m_material.basicFibreStiffnessConstant / L0;

		m_particles[m_arms[i].p1].fMasses.x += thisStiffness;
		m_particles[m_arms[i].p1].fMasses.y += thisStiffness;
		m_particles[m_arms[i].p1].fMasses.z += thisStiffness;

		m_particles[m_arms[i].p2].fMasses.x += thisStiffness;
		m_particles[m_arms[i].p2].fMasses.y += thisStiffness;
		m_particles[m_arms[i].p2].fMasses.z += thisStiffness;


		//Particle strain contribution
		float strainSquaredContribution = strain_ab * strain_ab;
		m_particles[m_arms[i].p1].strainSquared += strainSquaredContribution;
		m_particles[m_arms[i].p2].strainSquared += strainSquaredContribution;

		float strainContribution = std::abs(strain_ab);
		m_particles[m_arms[i].p1].strain += strainContribution;
		m_particles[m_arms[i].p2].strain += strainContribution;


		//Debug values
		if (tensionCoefficient > maxTC)
			maxTC = tensionCoefficient;

		if (tensionCoefficient < minTC)
			minTC = tensionCoefficient;

		avrgTC += std::abs(tensionCoefficient);

		m_avrgArmStrain += std::abs(strain_ab);
	}

	m_avrgArmStrain = m_avrgArmStrain / m_nArms;

	if (m_disp.stepCounter % 500 == 0)
	{
		avrgTC = avrgTC / m_nArms;
		float avrgMass = 0.0f;
		for (int i = 0; i < m_nParticles; i++)
			avrgMass += GetVectorLength(m_particles[i].fMasses);

		avrgMass = avrgMass / m_nParticles;
		std::cout << "Average particle mass: " << avrgMass << " Min, Average, Max tension coefficient: " << minTC << ", " << avrgTC << ", " << maxTC << std::endl;
	}
}

//Steel test specific functions
void Structure::CalcDisplacementEnds()
{
	int topCounter = 0;
	int botCounter = 0;
	double avrgTopDisp = 0.0f;
	double avrgBotDisp = 0.0f;

	Vec3 topDisp = { 0, 0, 0 };
	Vec3 botDisp = { 0, 0, 0 };

	if (m_disp.stepCounter == 1)
	{
		m_avrgDispTop.push_back(0);
		m_avrgDispBot.push_back(0);
		m_avrgDispTotVer.push_back(0);
	}

	for (int i = 0; i < m_nParticles; i++)
	{
		//If true we'r at a particle in the top of the cylinder
		if (m_isLoaded[i])
		{
			float xDiff = m_particles[i].cPosition.x - m_particles[i].iPosition.x;
			float yDiff = m_particles[i].cPosition.y - m_particles[i].iPosition.y;
			float zDiff = m_particles[i].cPosition.z - m_particles[i].iPosition.z;

			topDisp.x += xDiff;
			topDisp.y += yDiff;
			topDisp.z += zDiff;
			topCounter++;
		}
		else if (m_isLocked[i])
		{
			float xDiff = m_particles[i].cPosition.x - m_particles[i].iPosition.x;
			float yDiff = m_particles[i].cPosition.y - m_particles[i].iPosition.y;
			float zDiff = m_particles[i].cPosition.z - m_particles[i].iPosition.z;

			botDisp.x += xDiff;
			botDisp.y += yDiff;
			botDisp.z += zDiff;
			botCounter++;
		}
	}

	float topDispMag = GetVectorLength(topDisp) / (float)topCounter;
	float botDispMag = GetVectorLength(botDisp) / (float)botCounter;
	float totDispMag = topDispMag + botDispMag;

	m_avrgDispTop.push_back(topDispMag);
	m_avrgDispBot.push_back(botDispMag);
	m_avrgDispTotVer.push_back(totDispMag);

	m_incStrain.push_back(totDispMag / m_gs.height);
}

void Structure::CalcStressEnds()
{

	Vec3 reactionForceVecTop = { 0, 0, 0 };
	Vec3 reactionForceVecBot = { 0, 0, 0 };

	if (m_disp.stepCounter == 1)
	{
		m_reactionForceTop.push_back(0);
		m_reactionForceBot.push_back(0);
		m_incStress.push_back(0);
	}

	for (int i = 0; i < m_nParticles; i++)
	{
		if (m_isLoaded[i])
		{
			reactionForceVecTop.x += m_particles[i].ArmForce.x;
			reactionForceVecTop.y += m_particles[i].ArmForce.y;
			reactionForceVecTop.z += m_particles[i].ArmForce.z;
		}
		else if (m_isLocked[i])
		{
			reactionForceVecBot.x += m_particles[i].ArmForce.x;
			reactionForceVecBot.y += m_particles[i].ArmForce.y;
			reactionForceVecBot.z += m_particles[i].ArmForce.z;
		}
	}

	float reactionForceMagTop = GetVectorLength(reactionForceVecTop);
	float reactionForceMagBot = GetVectorLength(reactionForceVecBot);

	m_reactionForceTop.push_back(reactionForceMagTop);
	m_reactionForceBot.push_back(reactionForceMagBot);

	m_incStress.push_back(reactionForceMagTop / m_gs.AreaSmall);
}

//Other utility functions
void Structure::SaveResults()
{
	CalcStressEnds();

	CalcStrain();
}

void Structure::SumVelocity() 
{
	float v = 0.0f;
	float totV = 0.0f;

	for (int i = 0; i < m_nParticles; i++) 
	{
		v = GetVectorLength(m_particles[i].Veloc);
		totV += v;
	}

	m_incVelocity.push_back(totV);
}

void Structure::CalcConvergenceForce()
{
	/*
	Vec3 obfVec = { 0, 0, 0 };

	if (m_disp.stepCounter == 1)
		m_incObfAvrg.push_back(0);

	int counter = 0;
	float avrgObf;

	for (int i = 0; i < m_nParticles; i++)
	{
		if (!m_loaded[i] && !m_locked[i])
		{
			obfVec.x += m_particles[i].ArmForce.x;
			obfVec.y += m_particles[i].ArmForce.y;
			obfVec.z += m_particles[i].ArmForce.z;
			counter++;
		}
	}

	avrgObf = GetVectorLength(obfVec) / ((float)counter);
	m_incObfAvrg.push_back(avrgObf);

	*/
}

void Structure::UpdateParticleLife()
{
	int nDyingParticles = 0;
	int nRecoveredParticles = 0;

	for (int i = 0; i < m_nParticles; i++)
	{
		if (m_particles[i].life != pLife::immortal)
		{
			if (m_particles[i].strainSquared < 0.00001)
			{
				m_particles[i].life = pLife::dead;
				nDyingParticles++;
			}

			if (m_particles[i].strainSquared > 0.00001)
			{
				m_particles[i].life = pLife::alive;
				nRecoveredParticles++;
			}
		}
	}
}

void Structure::CalcStrain()
{
	/*
	int topCounter = 0;
	int botCounter = 0;
	double avrgTopDisp = 0.0f;
	double avrgBotDisp = 0.0f;

	Vec3 topDisp = { 0, 0, 0 };
	Vec3 botDisp = { 0, 0, 0 };

	if (m_disp.stepCounter == 1)
	{
		m_avrgDispGaugeTop.push_back(0);
		m_avrgDispGaugeBot.push_back(0);
		m_avrgDispGaugeTot.push_back(0);
		m_incGaugeStrain.push_back(0);
	}

	for (int i = 0; i < m_nParticles; i++)
	{
		//If true we'r at a particle in the top of the cylinder
		if (m_particles[i].gauge == gaugePlane::top)
		{
			float xDiff = m_particles[i].cPosition.x - m_particles[i].iPosition.x;
			float yDiff = m_particles[i].cPosition.y - m_particles[i].iPosition.y;
			float zDiff = m_particles[i].cPosition.z - m_particles[i].iPosition.z;

			topDisp.x += xDiff;
			topDisp.y += yDiff;
			topDisp.z += zDiff;
			topCounter++;
		}
		else if (m_particles[i].gauge == gaugePlane::bot)
		{
			float xDiff = m_particles[i].cPosition.x - m_particles[i].iPosition.x;
			float yDiff = m_particles[i].cPosition.y - m_particles[i].iPosition.y;
			float zDiff = m_particles[i].cPosition.z - m_particles[i].iPosition.z;

			botDisp.x += xDiff;
			botDisp.y += yDiff;
			botDisp.z += zDiff;
			botCounter++;
		}
	}

	float topDispMag = GetVectorLength(topDisp) / (float)topCounter;
	float botDispMag = GetVectorLength(botDisp) / (float)botCounter;
	float totDispMag = topDispMag + botDispMag;

	m_avrgDispGaugeTop.push_back(topDispMag);
	m_avrgDispGaugeBot.push_back(botDispMag);
	m_avrgDispGaugeTot.push_back(totDispMag);

	m_incGaugeStrain.push_back(totDispMag / m_gaugeSpacing);

	*/

}

void Structure::CalcPoissonsRatio()
{
}

void Structure::CalcPoissonsRatioVertical() 
{
	float avrgDi = 0;
	float avrgDc = 0;
	float avrgHi = 0;
	float avrgHc = 0;

	int hCounter = 0;
	int rCounter = 0;

	for (int i = 0; i < m_nParticles; i++) 
	{
		//Waist condition. Particles here are used to calculate the change in diameter.
		if (m_particles[i].iPosition.z < m_prs.waistHeight / 2.0f && m_particles[i].iPosition.z > -1 * (m_prs.waistHeight / 2.0f))
		{
			float xi = m_particles[i].iPosition.x;
			float yi = m_particles[i].iPosition.y;
			float ri = sqrt(xi * xi + yi * yi);
			float xc = m_particles[i].cPosition.x;
			float yc = m_particles[i].cPosition.y;
			float rc = sqrt(xc * xc + yc * yc);

			if (ri > m_prs.rLimit)
			{
				avrgDi += ri * 2;
				avrgDc += rc * 2;
				rCounter++;
			}
		}

		//Height condition. Particles here are used to calculate the change in hight for the cylinder.
		if (m_particles[i].iPosition.z < -1 * m_prs.hLimit || m_particles[i].iPosition.z > m_prs.hLimit)
		{
			float zi = std::abs(m_particles[i].iPosition.z);
			float zc = std::abs(m_particles[i].cPosition.z);

			avrgHi += zi * 2;
			avrgHc += zc * 2;
			hCounter++;
		}
	}
	
	//Make values average.
	avrgDi = avrgDi / rCounter;
	avrgDc = avrgDc / rCounter;

	avrgHi = avrgHi / hCounter;
	avrgHc = avrgHc / hCounter;

	float numerator = (avrgDc - avrgDi) / avrgDi;
	float denominator = (avrgHc - avrgHi) / avrgHi;

	if (denominator != 0) 
	{
		float poisson = -1.0f * numerator / denominator;
		m_incPossionsRatio.push_back(poisson);
	}
}

void Structure::CalcSectionStress() 
{
	if (m_disp.stepCounter == 1) 
	{
		m_incSectionStress.push_back(0);
		m_incSectionStressTen.push_back(0);
		m_incSectionStressCom.push_back(0);
	}

	float fTot = 0.0f;
	float tenTot = 0.0f;
	float comTot = 0.0f;

	float area = 0.0f;
	float areaCom = 0.0f;
	float areaTen = 0.0f;
	int counter = 0;
	int tenCounter = 0;
	int comCounter = 0;
	int brokenCounter = 0;

	for (int i = 0; i < m_arms.size(); i++)
	{	
		//Looking for stress i z-dir
		float z1 = m_particles[m_arms[i].p1].iPosition.z;
		float z2 = m_particles[m_arms[i].p2].iPosition.z;

		if (std::signbit(z1) != std::signbit(z2))
		{
			Vec3 dirVec = GetUnitVector(m_particles[m_arms[i].p1], m_particles[m_arms[i].p2], m_arms[i].cLength);
			float fMag = m_arms[i].fMag;
			float fz = std::abs(dirVec.z) * fMag;
			fTot += fz;
		}
	}
	area = m_gs.AreaSmall;

	float stress = fTot / area;
	float stressTen = tenTot / areaTen;
	float stressCom = comTot / areaCom;

	m_incSectionStress.push_back(stress);
	m_incSectionStressTen.push_back(stressTen);
	m_incSectionStressCom.push_back(stressCom);
}

void Structure::CalcMass() 
{
	float mTot = 0;

	for (int i = 0; i < m_nParticles; i++) 
	{
		mTot += m_particles[i].rMass;
	}

	std::cout << "Total Mass: " << mTot << std::endl;
}

void Structure::TestConsitencyCondition()
{
	float num; //Should be close to 1.
	Arm arm;
	float avrgNum = 0;
	int counter = 0;
	float rab, mb;
	Particle pNext;

	float totVol = 0;


	for (int i = 0; i < m_nParticles; i++) 
	{
		int startIndex = m_paStartIndexV[i];
		int rowLength = m_paRowLengthV[i];
		Particle p = m_particles[i];
		
		float W, Wprime, dV;

		num = 0;

		float pdV = p.rMass / m_material.rho;

		W = Evaluate(0.0f, p.h, m_kernel);

		//Adding the particle iself
		num += W * pdV;

		totVol += pdV;

		if (i == m_pClosestToOrigin) 
		{
			int a = 10;
		}

		
		//Loop around the connecting arms
		for (int j = 0; j < rowLength; j++)
		{
			int paFlatIndex = startIndex + j;
			int aNextIndex = m_paTopV[paFlatIndex];

			arm = m_arms[aNextIndex];
			rab = arm.cLength;

			pNext = m_particles[arm.p2];
			
			W = Evaluate(rab, pNext.h, m_kernel);

			Wprime = abs(EvaluateDerivative(rab, pNext.h, m_kernel));

			dV = pNext.rMass / m_material.rho;

			num += W * dV;

			counter++;
		}

		avrgNum += num;

		m_kernelConcistency[i] = num;
	}

	avrgNum = avrgNum / m_nParticles;

	std::cout << "Kernel concistency number average: " << avrgNum << std::endl;

	std::cout << "Total volume: " << totVol << std::endl;

		
}

void Structure::SetAnalysisType(bool scaleStiffness) 
{
	m_scaleStiffness = scaleStiffness;
}

void Structure::CreateGaugePlanes()
{
	/*
	m_gaugeSpacing = 0.02;	//Resulting in 0.02 m gauge distance
	float gaugePlaneThickness = 0.001;

	for (int i = 0; i < m_particles.size(); i++)
	{
		float topLowerLim = (m_gaugeSpacing / 2.0f) - (gaugePlaneThickness / 2.0f);
		float topUpperLim = (m_gaugeSpacing / 2.0f) + (gaugePlaneThickness / 2.0f);
		float botLowerLim = -(m_gaugeSpacing / 2.0f) - (gaugePlaneThickness / 2.0f);
		float botUpperLim = -(m_gaugeSpacing / 2.0f) + (gaugePlaneThickness / 2.0f);

		float pZ = m_particles[i].iPosition.z;

		if (pZ > topLowerLim && pZ < topUpperLim)
		{
			m_particles[i].gauge = gaugePlane::top;
		}
		else if (pZ > botLowerLim && pZ < botUpperLim)
		{
			m_particles[i].gauge = gaugePlane::bot;
		}
	}
	*/
}


//---------------- Results and Exports -----------------//

void Structure::OutputResults()
{
	CollectData();
	float area = 0.0f;
	float height = 0.0f;
	float diameter = 0.0f;
	string name = "";

	if (m_importType == ImportType::Femur2D || m_importType == ImportType::Frame2D)
	{
		name = "output/strain" + m_fileNameSuffix + ".txt";

		std::ofstream fileStrain(name);
		for (int i = 0; i < m_incGaugeStrain.size(); i++)
		{
			float strain = m_incGaugeStrain[i];
			fileStrain << strain << '\n';
		}
		fileStrain.close();

		name = "output/stress" + m_fileNameSuffix + ".txt";

		std::ofstream fileStress(name);
		for (int i = 0; i < m_incStress.size(); i++)
		{
			float stress = m_incStress[i];
			fileStress << stress << '\n';
		}
		fileStress.close();

		
	}
	else //3D simulation
	{
		name = "output/veloc" + m_fileNameSuffix + ".txt";

		std::ofstream fileVeloc(name);
		for (int i = 0; i < m_incVelocity.size(); i++)
		{
			float veloc = m_incVelocity[i];
			fileVeloc << veloc << '\n';
		}
		fileVeloc.close();
	}

}

void Structure::CollectData() 
{
	std::string filename = "";

	filename = "output/simulationData" + m_fileNameSuffix + ".txt";

	std::ofstream file(filename);

	if (m_importType == ImportType::Femur2D || m_importType == ImportType::Frame2D)
	{
		file << "Femur Simulation Settings" << '\n';
	}
	else //3D Simulation
	{
		file << "Tetra based Simulation Settings" << '\n';
	}

	file << "Random seed for shaking: " << m_gs.rndSeed << '\n';

	file << "BC thickness: " << m_gs.bcThickness << '\n';

	file << "Cross section area small: " << m_gs.AreaSmall << '\n';
	file << "Cross section area large: " << m_gs.AreaLarge << '\n';
	
	file << "nP: " << m_nParticles << '\n';
	file << "nA: " << m_nArms << '\n';
	file << "nZ: " << m_nZones << '\n';

	file << "Average arm count per particle: " << m_stats.nBondsPerParticle << '\n';
	file << "Average particle count per zone: " << m_stats.nParticlesPerZone << '\n';
	file << "Smallest zone over horizon (should be greater than 1!): " << m_stats.zoneSizeParticleSizeRatio << '\n';

	file << "Average arme length: " << m_stats.averageArmLength << '\n';
	file << "Elongation limit over average arme length: " << m_material.elongationLimit / m_stats.averageArmLength << '\n';

	file << "Elongation limit: " << m_material.elongationLimit << '\n';
	file << "Yield strain: " << m_material.yieldStrain << '\n';
	file << "Youngs Modulus: " << m_material.E << '\n';
	file << "Shear Modulus: " << m_material.G << '\n';
	file << "Bulk Modulus: " << m_material.K << '\n';
	file << "Poissons number: " << m_material.v << '\n';

	file << "Time step: " << m_solver.dt << '\n';
	file << "Carry over factor: " << m_solver.co << '\n';

	file << "Displacement magnitude: " << m_disp.magnitude << '\n';
	file << "Load step size: " << m_disp.stepSize << '\n';
	file << "Used load steps: " << m_disp.stepCounter << '\n';
	
	file << "Kernel k: " << m_kernel.k << '\n';
	file << "Kernel C: " << m_kernel.C << '\n';
	file << "Kernel alpha: " << m_kernel.alpha << '\n';

	file << "Plasticity on: " << m_plasticityOn << '\n';
	file << "Check fracture on: " << m_CheckFracture << '\n';

	file << "Load spring initial length: " << m_spring.iLength << '\n';
	file << "Load spring stiffness: " << m_spring.k << '\n';

	file.close();

}

void Structure::ExportModel() 
{
	std::ofstream fileA("output/armsA.txt");
	std::ofstream fileB("output/armsB.txt");

	for (int i = 0; i < m_arms.size(); i++)
	{
		float x1 = m_particles[m_arms[i].p1].iPosition.x;
		float y1 = m_particles[m_arms[i].p1].iPosition.y;
		float z1 = m_particles[m_arms[i].p1].iPosition.z;

		float x2 = m_particles[m_arms[i].p2].iPosition.x;
		float y2 = m_particles[m_arms[i].p2].iPosition.y;
		float z2 = m_particles[m_arms[i].p2].iPosition.z;

		fileA << x1 << ',' << y1 << ',' << z1 << '\n';
		fileB << x2 << ',' << y2 << ',' << z2 << '\n';
	}

	fileA.close();
	fileB.close();

	std::ofstream fileP("output/coord.txt");

	for (int i = 0; i < m_particles.size(); i++)
	{
		float x = m_particles[i].iPosition.x;
		float y = m_particles[i].iPosition.y;
		float z = m_particles[i].iPosition.z;
		float r = m_particles[i].rSize;

		fileP << x << ',' << y << ',' << z << ',' << r << '\n';
	}
	fileP.close();

	std::ofstream fileC("output/color.txt");

	for (int i = 0; i < m_particles.size(); i++)
	{
		float x = m_particles[i].Color.x;
		float y = m_particles[i].Color.y;
		float z = m_particles[i].Color.z;
		float r = m_particles[i].Color.w;

		fileC << x << ',' << y << ',' << z << ',' << r << '\n';
	}
	fileC.close();



}




